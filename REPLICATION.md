# [AERI-2021-0161.R2 CA Data Review Request Rev.0] [Wage Cyclicality and Labor Market Sorting] Validation and Replication results

> Some useful links:
> - [Official Data and Code Availability Policy](https://www.aeaweb.org/journals/policies/data-code)
> - [Step by step guidance](https://aeadataeditor.github.io/aea-de-guidance/) 
> - [Template README](https://social-science-data-editors.github.io/template_README/)

SUMMARY
-------

Thank you for your replication archive. All tables and figure were successfully reproduced. Please add/update data citations for data sources. Also, please add access conditions to the README for each data source. In assessing compliance with our [Data and Code Availability Policy](https://www.aeaweb.org/journals/policies/data-code), we have identified the following issues, which we ask you to address.


**Conditional on making the requested changes to the openICPSR deposit prior to publication, the replication package is accepted.**

### Action Items (manuscript)

- [REQUIRED] Please add (corrected/complete) data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

### Action Items (openICPSR)

- [REQUIRED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 
- [REQUIRED] Please amend README to contain complete requirements.
- [REQUIRED] Please provide debugged code, addressing the issues identified in this report.


- [SUGGESTED] We suggest you update the openICPSR metadata fields marked as (highly recommended), in order to improve findability of your data and code supplement. 
- [SUGGESTED] We suggest you update the openICPSR metadata fields marked as (suggested), in order to improve findability of your data and code supplement. 

General
-------

> [SUGGESTED] A recommended README template for replication packages in economics can be found on the [Social Science Data Editor Github site](https://social-science-data-editors.github.io/guidance/template-README.html).

Data description
----------------

### Data Sources

#### NLSY79 Dataset

- Data are provided.
- The data are publicly available.
- Access conditions are not described. Only a link to the homepage of the BLS is provided.
- The data are cited in the paper.

> Bureau of Labor Statistics, U.S. Department of Labor. 2019. “National Longitudinal Survey of Youth 1979 cohort, 1979-2016 (rounds 1-27).” Produced and distributed by the Center for Human Resource Research (CHRR), The Ohio State University. Columbus

> [REQUIRED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 


#### Unemployment rate Data

- Data are provided.
- The data are publicly available.
- Access conditions are not described. Only a link to the homepage of the BLS is provided.
- All data is for the US. Source: Bureau of Labor Statistics, U.S. Department of Labor
- Data is not cited.

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

> [REQUIRED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 


#### Crosswalk Data (Autor and Dorn (2013))

- Data are provided.
- The data are publicly available.
- Access conditions are not described. Only a link to the homepage of the Census Bureau is provided.
- Autor and Dorn paper is cited in README and paper, but data is not.

> Autor, David H., and David Dorn. 2013. “The Growth of Low-Skill Service Jobs and the
Polarization of the US Labor Market.” American Economic Review, 103(5): 1553–97.

> [REQUIRED] Please add *data* citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html).

You should cite David Dorn's website (if that's what you used), or the replication package for the 2013 AER article (if that's what you used).

> [REQUIRED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 

#### O\*NET (National Center for O*NET Development (2016))

- Data are provided. 
- The data are publicly available.
- Access conditions are not described.
- Data is cited in paper, but citation is not complete. There is no information for location of data (ie. link) in citation.

> National Center for O*NET Development. 2016. “O*NET OnLine.”

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html). **In particular, please add location of data (ie. link) to the citation, as well as the version of the O\*NET data, as there are multiple versions.**

> [REQUIRED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 


Data deposit
------------

- [x] README is in TXT, MD, PDF format
- [x] openICPSR deposit has no ZIP files
- [x] Title conforms to guidance (starts with "Data and Code for:" or "Code for:", is properly capitalized)
- [x] Authors (with affiliations) are listed in the same order as on the paper


### Deposit Metadata


- [x] JEL Classification (required)
- [x] Manuscript Number (required)
- [x] Subject Terms (highly recommended)
- [ ] Geographic coverage (highly recommended)
- [ ] Time period(s) (highly recommended)
- [ ] Collection date(s) (suggested)
- [ ] Universe (suggested)
- [x] Data Type(s) (suggested)
- [x] Data Source (suggested)
- [ ] Units of Observation (suggested)

- [NOTE] openICPSR metadata is sufficient.

- [SUGGESTED] We suggest you update the openICPSR metadata fields marked as (highly recommended), in order to improve findability of your data and code supplement. 
- [SUGGESTED] We suggest you update the openICPSR metadata fields marked as (suggested), in order to improve findability of your data and code supplement. 

For additional guidance, see [https://aeadataeditor.github.io/aea-de-guidance/data-deposit-aea-guidance.html](https://aeadataeditor.github.io/aea-de-guidance/data-deposit-aea-guidance.html).

Data checks
-----------
-Data sets are provided in .dta format.

Code description
----------------

There are 19 provided Stata do files, including a master do file.

- There are two subfolders: (i) Main, which includes Stata do-files that prepare the data to reproduce tables and figures in the manuscript; and (ii) Appendix, which includes Stata do-files that prepare the data to replicate figure D.1 in the online appendix
- All tables and figures map to code. Mapping is commented in code.


Stated Requirements
---------------------

- [ ] No requirements specified
- [x] Software Requirements specified as follows:
  - Stata (last run in version 16)
    - reghdfe, estout and outreg
- [ ] Computational Requirements specified as follows:
  - Cluster size, etc.
- [ ] Time Requirements specified as follows:
  - Length of necessary computation (hours, weeks, etc.)

- [ ] Requirements are complete.

Missing Requirements
--------------------

- [x] Software Requirements 
  - [x] Stata
    - [x]Packages: egen
- [x] Computational Requirements specified as follows:
  - Cluster size, disk size, memory size, etc.
- [x] Time Requirements 
  - Length of necessary computation (hours, weeks, etc.)

> [REQUIRED] Please amend README to contain complete requirements. 

Computing Environment of the Replicator
---------------------

- CISER Shared Windows Server 2019, 256GB, Intel Xeon E5-4669 v3 @ 2.10Ghz (2 processors, 36 cores)
- Stata/MP 17

Replication steps
-----------------

1. Downloaded code from URL provided and data from OpenICPSR
2. Ran master.do
3. Ran into error on do file figures_tables_app.do
`rename(unempl_region unempl 1.dummy1#c.unempl_region 1.dummy1#c.unempl 1.dummy2#c.unempl_region 1.dummy2#c.unempl  mismatch1w mismatch1w_w c.mismatch1w#c.unempl_region c.mismatch1w_w#c.unempl 1.dummy1#c.unempl_region#c.mismatch1w 1.dummy1#c.unempl#c.mismatch1w_w 1.dummy2#c.unempl_region#c.mismatch1w 1.dummy2#c.unempl#c.mismatch1w_w 1.dummy2#c.unempl_region#c.mismatch1w_pos 1.dummy2#c.unempl#c.mismatch1w_pos_w 1.dummy2#c.unempl_region#c.mismatch1w_neg  1.dummy2#c.unempl#c.mismatch1w_neg_w 1.dummy1#c.unempl_region#c.mismatch1w_pos 1.dummy1#c.unempl#c.mismatch1w_pos_w 1.dummy1#c.unempl_region#c.mismatch1w_neg 1.dummy1#c.unempl#c.mismatch1w_neg_w c.mismatch1w_neg#c.unempl_region c.mismatch1w_neg_w#c.unempl c.mismatch1w_pos#c.unempl_region c.mismatch1w_pos_w#c.unempl) interaction(" $\cdot$ ")`
  - Fixed the problem by corresponding correct names with correct variables.
4. Re-run do file figures_tables_app.do 

> [REQUIRED] Please provide debugged code, addressing the issues identified in this report.


Findings
--------

| Data Preparation Program        | Dataset created                      | Reproduced? |
| ------------------------------- | ------------------------------------ | ----------- |
| 1\_ind\_info.do                 | \_individualfile.dta                 | Yes         |
| 2\_job\_info.do                 | $data/\_jobfile.dta                  | Yes         |
| 3\_monthly\_panel               | $data/data\_month.dta                | Yes         |
| 4\_construct\_data\_analysis.do | $data/data\_analysis                 | Yes         |
| cpi.do                          | $data/Macro indicators/cpi           | Yes         |
| job2to5.do                      | $data/weekly\_j2to5\_long.dta        | Yes         |
| occ\_input.do                   |                                      | Yes         | 
| rename\_weekly\_lstatus.do      |                                      | Yes         |
| rename\_weekly\_lstatus2.do     |                                      | Yes         |
| 1\_sample\_criteria\_mil1.do    | $data/appendix/data\_analysis\_mil1  | Yes         |
| 2\_sample\_criteria\_mil4.do    | $data/appendix/data\_analysis\_mil4  | Yes         |
| 3\_sample\_criteria\_olf15.do   | $data/appendix/data\_analysis\_olf15 | Yes         |
| 4\_sample\_criteria\_olf20.do   | $data/appendix/data\_analysis\_olf20 | Yes         |
| 5\_sample\_criteria\_full.do    | $data/appendix/data\_analysis\_full  | Yes         |


### Tables/Figures
| Figure/Table # | Program                 | Line Number | Reproduced? |
| -------------- | ----------------------- | ----------- | ----------- |
| Table 1        | figures\_tables.do      | 9           | yes         |
| Table 2        | figures\_tables.do      | 58          | yes         |
| Figure 1       | figures\_tables.do      | 96          | yes         |
| Figure 2       | figures\_tables.do      | 154         | yes         |
| Figure 3       | figures\_tables.do      | 289         | yes         |
| Table A.1      | figures\_tables\_app.do | 9           | yes         |
| Table A.2      | figures\_tables\_app.do | 34          | yes         |
| Tables C.1     | figures\_tables\_app.do | 57          | yes         |
| Tables C.2     | figures\_tables\_app.do | 91          | yes         |
| Tables C.3     | figures\_tables\_app.do | 131         | yes         |
| Figure B.1     | figures\_tables\_app.do | 170         | yes         |
| Figure E.1     | figures\_tables\_app.do | 266         | yes         |
| Figure D.1     | figures\_tables\_app.do | 334         | yes         |

### In text numbers
| In-text numbers | Program            | Line Number | Reproduced? |
| --------------- | ------------------ | ----------- | ----------- |
| 1.24            | figures\_tables.do | 988         | yes         |
| 1.7             | figures\_tables.do | 1000        | yes         |
| 1.6             | figures\_tables.do | 1005        | yes         |
| 3.3             | figures\_tables.do | 1014        | yes         |
| 1.6             | figures\_tables.do | 1019        | yes         |
| 2               | figures\_tables.do | 1033        | yes         |
| 2               | figures\_tables.do | 1042        | yes         |
| 3               | figures\_tables.do | 1047        | yes         |


Classification
--------------


- [ ] full reproduction
- [x] full reproduction with minor issues
- [ ] partial reproduction (see above)
- [ ] not able to reproduce most or all of the results (reasons see above)

### Reason for incomplete reproducibility


- [ ] `Discrepancy in output` (either figures or numbers in tables or text differ)
- [x] `Bugs in code`  that  were fixable by the replicator (but should be fixed in the final deposit)
- [ ] `Code missing`, in particular if it  prevented the replicator from completing the reproducibility check
  - [ ] `Data preparation code missing` should be checked if the code missing seems to be data preparation code
- [ ] `Code not functional` is more severe than a simple bug: it  prevented the replicator from completing the reproducibility check
- [ ] `Software not available to replicator`  may happen for a variety of reasons, but in particular (a) when the software is commercial, and the replicator does not have access to a licensed copy, or (b) the software is open-source, but a specific version required to conduct the reproducibility check is not available.
- [ ] `Insufficient time available to replicator` is applicable when (a) running the code would take weeks or more (b) running the code might take less time if sufficient compute resources were to be brought to bear, but no such resources can be accessed in a timely fashion (c) the replication package is very complex, and following all (manual and scripted) steps would take too long.
- [ ] `Data missing` is marked when data *should* be available, but was erroneously not provided, or is not accessible via the procedures described in the replication package
- [ ] `Data not available` is marked when data requires additional access steps, for instance purchase or application procedure. 
