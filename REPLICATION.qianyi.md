# [AERI-2021-0161.R2 CA Data Review Request Rev.0] [Wage Cyclicality and Labor Market Sorting] Validation and Replication results

> INSTRUCTIONS: Once you've read these instructions, DELETE THESE AND SIMILAR LINES.
> In the above title, replace [Manuscript Title] with the actual title of the paper, and [MC number] with the Manuscript Central number (e.g., AEJPol-2017-0097)
> Go through the steps to download and attempt a replication. Document your steps here, the errors generated, and the steps you took to alleviate those errors.

> Some useful links:
> - [Official Data and Code Availability Policy](https://www.aeaweb.org/journals/policies/data-code)
> - [Step by step guidance](https://aeadataeditor.github.io/aea-de-guidance/) 
> - [Template README](https://social-science-data-editors.github.io/template_README/)

SUMMARY
-------
Action Items (Manuscript)

[REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the AEA Sample References and in additional guidance.

General
-------

> INSTRUCTIONS: Leave this in, when any of the sections is lacking. Remove the entire section only if the README has all the pieces necessary (up to minor imprecisions).

> [SUGGESTED] A recommended README template for replication packages in economics can be found on the [Social Science Data Editor Github site](https://social-science-data-editors.github.io/guidance/template-README.html).

Data description
----------------

### Data Sources

NLSY79 Dataset
- Data are provided. Link is provided.
- The data are publicly available.
- The data are cited in the README but not in the article.
[REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the AEA Sample References and in additional guidance.

Monthly panel at the worker level Data
- Data are provided. Link is provided.
- The data are publicly available.
- The data are cited in the README and in the article.
  - The author build a monthly panel at the worker level as in Baley, Figueiredo and Ulbricht (2020).

National unemployment rate Data from the BLS
- Data are provided. Link is provided.
- The data are publicly available.
- The data are cited in the README and in the article.

O*NET
- Data are provided. Link is provided.
- The data are publicly available.
- The data are cited in the README but not in the article.
- with the importance score for the selected descriptors (listed in the online appendix) and SOC
occupation codes. Source: National Center for O*NET Development (2016).
[REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the AEA Sample References and in additional guidance.

Unemployment rate Data
- Data are provided. Link is provided.
- The data are publicly available.
- The data are cited in the README but not in the article.
- All data is for the US. Source: Bureau of Labor Statistics, U.S. Department of Labor
[REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the AEA Sample References and in additional guidance.

Crosswalk Data
- Data are provided. Link is provided.
- The data are publicly available.
- The data are cited in the README but not in the article.
[REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the AEA Sample References and in additional guidance.


Data deposit
------------


- [x] README is in TXT, MD, PDF format
- [x] openICPSR deposit has no ZIP files
- [x] Title conforms to guidance (starts with "Data and Code for:" or "Code for:", is properly capitalized)
- [x] Authors (with affiliations) are listed in the same order as on the paper


### Deposit Metadata


- [x] JEL Classification (required)
- [x] Manuscript Number (required)
- [x] Subject Terms (highly recommended)
- [ ] Geographic coverage (highly recommended)
- [ ] Time period(s) (highly recommended)
- [ ] Collection date(s) (suggested)
- [ ] Universe (suggested)
- [x] Data Type(s) (suggested)
- [x] Data Source (suggested)
- [ ] Units of Observation (suggested)


For additional guidance, see [https://aeadataeditor.github.io/aea-de-guidance/data-deposit-aea-guidance.html](https://aeadataeditor.github.io/aea-de-guidance/data-deposit-aea-guidance.html).

Data checks
-----------
-Data sets are provided in .dta format.


Code description
----------------


There are 19 provided Stata do files.


Stated Requirements
---------------------

> INSTRUCTIONS: The authors may have specified specific requirements in terms of software, computer hardware, etc. Please list them here. This is **different** from the Computing Environment of the Replicator. You have the option to amend these with unstated requirements later. If all requirements are listed, check the box "Requirements are complete".

- [ ] No requirements specified
- [x] Software Requirements specified as follows:
  - Software 1
- [ ] Computational Requirements specified as follows:
  - Cluster size, etc.
- [ ] Time Requirements specified as follows:
  - Length of necessary computation (hours, weeks, etc.)

- [ ] Requirements are complete.

> If easier, simply copy-and-paste the authors' stated requirements here:


Missing Requirements
--------------------

- [x] Software Requirements 
  - [x] Stata
    - [x]Packages: egenmore, estout
- [x] Computational Requirements specified as follows:
  - Cluster size, disk size, memory size, etc.
- [x] Time Requirements 
  - Length of necessary computation (hours, weeks, etc.)

> [REQUIRED] Please amend README to contain complete requirements. 


---------------------

Computing Environment of the Replicator
- CISER Shared Windows Server 2019, 256GB, Intel Xeon E5-4669 v3 @ 2.10Ghz (2 processors, 36 cores)
- Stata/MP 17

Replication steps
-----------------

- Downloaded code from URL provided and data from OpenICPSR
- Run master.do
- Run into error on do file figures_tables_app.do
`rename(unempl_region unempl 1.dummy1#c.unempl_region 1.dummy1#c.unempl 1.dummy2#c.unempl_region 1.dummy2#c.unempl  mismatch1w mismatch1w_w c.mismatch1w#c.unempl_region c.mismatch1w_w#c.unempl 1.dummy1#c.unempl_region#c.mismatch1w 1.dummy1#c.unempl#c.mismatch1w_w 1.dummy2#c.unempl_region#c.mismatch1w 1.dummy2#c.unempl#c.mismatch1w_w 1.dummy2#c.unempl_region#c.mismatch1w_pos 1.dummy2#c.unempl#c.mismatch1w_pos_w 1.dummy2#c.unempl_region#c.mismatch1w_neg  1.dummy2#c.unempl#c.mismatch1w_neg_w 1.dummy1#c.unempl_region#c.mismatch1w_pos 1.dummy1#c.unempl#c.mismatch1w_pos_w 1.dummy1#c.unempl_region#c.mismatch1w_neg 1.dummy1#c.unempl#c.mismatch1w_neg_w c.mismatch1w_neg#c.unempl_region c.mismatch1w_neg_w#c.unempl c.mismatch1w_pos#c.unempl_region c.mismatch1w_pos_w#c.unempl) interaction(" $\cdot$ ")`
- Fixed the problem by corresponding correct names with correct variables.
- Re-run do file figures_tables_app.do 

Findings
--------
| Data Preparation Program        | Dataset created                      | Reproduced? |
| ------------------------------- | ------------------------------------ | ----------- |
| 1\_ind\_info.do                 | \_individualfile.dta                 | Yes         |
| 2\_job\_info.do                 | $data/\_jobfile.dta                  | Yes         |
| 3\_monthly\_panel               | $data/data\_month.dta                | Yes         |
| 4\_construct\_data\_analysis.do | $data/data\_analysis                 | Yes         |
| cpi.do                          | $data/Macro indicators/cpi           | Yes         |
| job2to5.do                      | $data/weekly\_j2to5\_long.dta        | Yes         |
| occ\_input.do                   |                                      | Yes         | 
| rename\_weekly\_lstatus.do      |                                      | Yes         |
| rename\_weekly\_lstatus2.do     |                                      | Yes         |
| 1\_sample\_criteria\_mil1.do    | $data/appendix/data\_analysis\_mil1  | Yes         |
| 2\_sample\_criteria\_mil4.do    | $data/appendix/data\_analysis\_mil4  | Yes         |
| 3\_sample\_criteria\_olf15.do   | $data/appendix/data\_analysis\_olf15 | Yes         |
| 4\_sample\_criteria\_olf20.do   | $data/appendix/data\_analysis\_olf20 | Yes         |
| 5\_sample\_criteria\_full.do    | $data/appendix/data\_analysis\_full  | Yes         |


### Tables/Figures
| Figure/Table # | Program                 | Line Number | Reproduced? |
| -------------- | ----------------------- | ----------- | ----------- |
| Table 1        | figures\_tables.do      | 9           | yes         |
| Table 2        | figures\_tables.do      | 58          | yes         |
| Figure 1       | figures\_tables.do      | 96          | yes         |
| Figure 2       | figures\_tables.do      | 154         | yes         |
| Figure 3       | figures\_tables.do      | 289         | yes         |
| Table A.1      | figures\_tables\_app.do | 9           | yes         |
| Table A.2      | figures\_tables\_app.do | 34          | yes         |
| Tables C.1     | figures\_tables\_app.do | 57          | yes         |
| Tables C.2     | figures\_tables\_app.do | 91          | yes         |
| Tables C.3     | figures\_tables\_app.do | 131         | yes         |
| Figure B.1     | figures\_tables\_app.do | 170         | yes         |
| Figure E.1     | figures\_tables\_app.do | 266         | yes         |
| Figure D.1     | figures\_tables\_app.do | 334         | yes         |

### In text numbers
| In-text numbers | Program            | Line Number | Reproduced? |
| --------------- | ------------------ | ----------- | ----------- |
| 1.24            | figures\_tables.do | 988         | yes         |
| 1.7             | figures\_tables.do | 1000        | yes         |
| 1.6             | figures\_tables.do | 1005        | yes         |
| 3.3             | figures\_tables.do | 1014        | yes         |
| 1.6             | figures\_tables.do | 1019        | yes         |
| 2               | figures\_tables.do | 1033        | yes         |
| 2               | figures\_tables.do | 1042        | yes         |
| 3               | figures\_tables.do | 1047        | yes         |


Classification
--------------


- [ ] full reproduction
- [x] full reproduction with minor issues
- [ ] partial reproduction (see above)
- [ ] not able to reproduce most or all of the results (reasons see above)

### Reason for incomplete reproducibility


- [ ] `Discrepancy in output` (either figures or numbers in tables or text differ)
- [ ] `Bugs in code`  that  were fixable by the replicator (but should be fixed in the final deposit)
- [ ] `Code missing`, in particular if it  prevented the replicator from completing the reproducibility check
  - [ ] `Data preparation code missing` should be checked if the code missing seems to be data preparation code
- [ ] `Code not functional` is more severe than a simple bug: it  prevented the replicator from completing the reproducibility check
- [ ] `Software not available to replicator`  may happen for a variety of reasons, but in particular (a) when the software is commercial, and the replicator does not have access to a licensed copy, or (b) the software is open-source, but a specific version required to conduct the reproducibility check is not available.
- [ ] `Insufficient time available to replicator` is applicable when (a) running the code would take weeks or more (b) running the code might take less time if sufficient compute resources were to be brought to bear, but no such resources can be accessed in a timely fashion (c) the replication package is very complex, and following all (manual and scripted) steps would take too long.
- [ ] `Data missing` is marked when data *should* be available, but was erroneously not provided, or is not accessible via the procedures described in the replication package
- [ ] `Data not available` is marked when data requires additional access steps, for instance purchase or application procedure. 
