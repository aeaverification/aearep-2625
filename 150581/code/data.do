
* note: this file constructs the data file "data_analysis.dta" to be used in the main analysis

do "$do/main/1_ind_info.do"
do "$do/main/2_job_info.do"
do "$do/main/3_monthly_panel.do"
do "$do/main/4_construct_data_analysis.do"
