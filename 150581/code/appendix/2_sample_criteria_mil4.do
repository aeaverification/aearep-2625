
clear all

********************************************************************************

*I. selection criteria as in footnote 2 of the manuscript

use "$data/data_month", clear

*1. more than two years in the military
gen aux=1 if JOB==7
bysort ID: egen years_mil=sum(aux)
gen militar=1 if years_mil>48
replace militar=0 if militar==.
drop if militar==1
drop aux

*2. weak labor attachment
gen aux=1 if JOB==5
bysort ID: egen years_out=sum(aux)
gen weak_labor_attach=1 if years_out>120
replace weak_labor_attach=0 if weak_labor_attach==.
drop if weak_labor_attach==1
drop aux

*3.already working in the beginning of the sample.
bysort ID year: egen HOURSY=sum(HOURSM)
gen aux=1 if HOURSY>1200 & year==1979
bysort ID: egen notransition=mean(aux)
drop if notransition==1
drop aux

*4. no ability scores.
drop if test_info==1

duplicates drop ID, force
keep ID
sort ID

save  "$data/appendix/sample_selection", replace

*************************************************************************************************************************
*II. skill requirements

use "$data/data_month", clear
sort ID 
merge ID using "$data/appendix/sample_selection"
keep if _m==3
drop _m

*generate weights for occupations
*#ofjobs in each occupation
rename OCC occ1990dd
gen aux_occ=1 if occ1990dd>0 & occ1990dd~=.
bysort occ1990dd: egen total_occ1990dd=sum(aux_occ)
rename total_occ1990dd occ_weight
duplicates drop occ1990dd, force
keep occ1990dd occ_weight
sort occ1990dd
save "$data/appendix/occ_weights", replace

*************************************************************************************************************

*2. skill requirements

*2.1 math, verbal & technical

use "$data/ONET/onet_categories_asvab", replace

sort soccode
merge soccode using "$data/Crosswalks/occ2000_soc"
drop if _m==2
drop _m
sort occ2000
quietly do "$do/main/other/occ_input"
replace occ1990dd=78 if soc2010=="19-1020"
bysort occ1990dd Element_ID: egen score=mean(Data_Value)
*duplicates drop occ1990dd Element_ID, force
by occ1990dd Element_ID, sort: keep if _n == 1

sort occ1990dd
merge occ1990dd using "$data/appendix/occ_weights"
drop if _merge==2
replace occ_weight=0 if _merge==1
drop _merge

foreach v in AR MK WK PC GS MC EI {
gen aux_`v'=`v'*score
bysort occ1990dd: egen total_`v'=sum(aux_`v')
bysort occ1990dd: egen all_`v'=sum(`v')
gen `v'_score=total_`v'/all_`v'
}

keep occ1990dd censtitle *_score occ_weight*
duplicates drop occ1990dd, force



foreach var in AR MK WK PC GS MC EI{
gen `var'_zscore=.
sum `var'_score
replace `var'_zscore=(`var'_score-r(mean))/r(sd) 
}

keep occ1990dd censtitle *_zscore occ_weight*

pca AR_zscore MK_zscore
predict math

pca WK_zscore PC_zscore
predict verbal

pca GS_zscore MC_zscore EI_zscore
predict technical

foreach var in math verbal technical {
xtile `var'_perc_occ_weighted=`var' [fweight=occ_weight], nq(100)  
xtile `var'_perc_occ=`var', nq(100)  
}
	
keep occ1990dd censtitle math* verbal* technical*
drop if occ1990dd==.
sort occ1990dd 

save "$data/appendix/occ_perc_weighted", replace

*3.2 social

use "$data/ONET/onet_categories_social", replace

sort soccode
merge soccode using "$data/Crosswalks/occ2000_soc"
drop if _m==2
drop _m
sort occ2000
quietly do "$do/main/other/occ_input"
replace occ1990dd=78 if soc2010=="19-1020"
bysort occ1990dd Element_ID: egen score=mean(Data_Value)

replace Element_Name="ServiceOrientation" if Element_Name=="Service Orientation"
replace Element_Name="SocialPerceptiveness" if Element_Name=="Social Perceptiveness"

*add weights
sort occ1990dd
merge occ1990dd using "$data/appendix/occ_weights"
drop if _merge==2
replace occ_weight=0 if _merge==1

drop _merge
 
duplicates drop occ1990dd Element_ID, force

foreach var in Coordination Instructing Negotiation ServiceOrientation SocialPerceptiveness Persuasion {
gen `var'_aux=.
sum score  if Element_Name=="`var'"
replace `var'_aux=(score-r(mean))/r(sd) if Element_Name=="`var'"
bysort occ1990dd: egen `var'_zscore=mean(`var'_aux)
drop *_aux
}

drop if occ1990dd==.
duplicates drop occ1990dd, force
keep occ1990dd *_zscore occ_weight* censtitle

pca Coordination_zscore Instructing_zscore Negotiation_zscore ServiceOrientation_zscore SocialPerceptiveness_zscore Persuasion_zscore
predict social

xtile social_perc_occ_weighted=social [fweight=occ_weight], nq(100) 
xtile social_perc_occ=social, nq(100) 
				
drop if occ1990dd==.
sort occ1990dd 
merge occ1990dd using "$data/appendix/occ_perc_weighted"
drop _m
sort occ1990dd 

keep censtitle occ1990dd *_perc_occ_weighted *_perc_occ
save "$data/appendix/occ_perc_weighted", replace

************************************************************************************************************************* 

*III. build data_analysis

use "$data/data_month", clear

sort ID year
merge ID using "$data/appendix/sample_selection"
keep if _m==3
drop _m

*add occ percentiles
rename OCC occ1990dd
sort occ1990dd 
merge occ1990dd using "$data/appendix/occ_perc_weighted"
drop if _m==2
drop _m


*************************************************************************************************************************

*2. job transitions & controls

*************************************************************************************************************************

*2.1. job transitions

*seprations: emp to unemp flows
sort ID year month
gen emp_unemp=1 if (JOB==4 & JN[_n-1]>0 & JN[_n-1]~=. & ID==ID[_n-1]) |  (JOB==5 & JN[_n-1]~=. & JN[_n-1]>0 & ID==ID[_n-1]) |  (JOB==2 & JN[_n-1]~=. & JN[_n-1]>0 & ID==ID[_n-1]) 

*auxiliar variable: to identify eue transitions and previous employment data.
gen aux_emp_unemp=emp_unemp
replace aux=aux_emp_unemp[_n-1] if (JN==. & JN[_n-1]==. & ID==ID[_n-1])

*emp to emp flows
sort ID year month
gen emp_emp=1 if JN~=JN[_n-1] & JN[_n-1]>0 & JN[_n]>0 & JN[_n-1]~=. & JN~=. & ID==ID[_n-1]

*non-employment to emp flows
sort ID year month
gen unemp_to_emp=1 if (JOB[_n-1]==4 & JN>0 & JN~=. & ID==ID[_n-1]) |  (JOB[_n-1]==5 & JN~=. & JN>0 & ID==ID[_n-1]) |  (JOB[_n-1]==2 & JN~=. & JN>0 & ID==ID[_n-1])
gen eue=unemp_to_emp if aux_emp_unemp[_n-1]==1  

*all transitions
gen all_switch=emp_emp
replace all_switch=unemp_to_emp if unemp_to_emp==1 & emp_emp==.

*new hire dummies
*new hire: regardless of type 
gen dummy=0 if all_switch==. & JN~=.
replace dummy=1 if all_switch==1

*ee transition
gen dummy1=0 if (emp_emp==. & all_switch==1) | (all_switch==.  & JN~=-.)
replace dummy1=1 if emp_emp==1 

*ue transition
gen dummy2=0 if (emp_emp==1) | (all_switch==. & JN~=.)
replace dummy2=1 if all_switch==1 & emp_emp==.

*previous unemployment
sort ID  year month
gen aux_JN=JN
replace aux_JN=aux_JN[_n-1]  if JN==. & ID==ID[_n-1]
gen JN_prev=aux_JN[_n-1] if dummy==1 & ID==ID[_n-1]

*unemployment duration
sort ID year month
gen unempl_duration=1 if JN==. & ID==ID[_n-1]
replace unempl_duration=unempl_duration[_n-1]+1 if JN==.  & ID==ID[_n-1] & JN[_n-1]==.

**recoding*
**stayers + ee transitions
	sort ID year month
	gen dummy_new=dummy
	replace dummy_new=. if year==1979 & month==1
	replace dummy_new=. if dummy_new[_n-1]==. & dummy==0 & dummy[_n-1]==0
	gen dummy1_new=dummy1 if dummy_new~=.
	foreach var in dummy dummy1 {
	rename `var' `var'_old
	rename `var'_new `var'
	}

*************************************************************************************************************************

*2.2. wage: trimming as in Guvenen at al (2018)

*wage: trimming as in Guvenen at al (2018)

winsor lhrp, gen(lhrp_w) p(0.001)
gen lhrp2=lhrp if lhrp==lhrp_w


*************************************************************************************************************************

*2.3. controls

**a. industry fe

gen INDN_aux=INDN
tostring INDN, replace
**transform 4-digit industry codes into 3-digit
gen aux=length(INDN)
gen aux2=substr(INDN,aux,aux) if year>=2002 & INDN~="-4" & INDN~="-5" & INDN~="-3" & INDN~="-2" & INDN~="-1" & INDN~="0"
gen aux4=substr(INDN,1,aux-1) if aux==4
replace INDN=aux4 if aux==4 
destring INDN, replace
drop aux aux2 aux4


**from 3-digit industry codes to one-digit classification

**1970's
gen industry=1 if INDN>=017 & INDN<=029 & year<=2001
replace industry=2 if INDN>=047 & INDN<=058 & year<=2001
replace industry=3 if INDN>=067 & INDN<=078 & year<=2001
replace industry=4 if INDN>=107 & INDN<=398 & year<=2001
replace industry=5 if INDN>=407 & INDN<=499 & year<=2001
replace industry=6 if INDN>=507 & INDN<=699 & year<=2001
replace industry=7 if INDN>= 707 & INDN<=719 & year<=2001 
replace industry=8 if INDN>=727 & INDN<=767 & year<=2001
replace industry=9 if INDN>=769 & INDN<=799 & year<=2001
replace industry=10 if INDN>=807 & INDN<=817 & year<=2001
replace industry=11 if INDN>=828 & INDN<=899 & year<=2001
replace industry=12 if INDN>=907 & INDN<=947 & year<=2001

**2000's
replace industry=1 if INDN>=017 & INDN<=029 & year>=2002
replace industry=2 if INDN>=037 & INDN<=049 & year>=2002
replace industry=3 if INDN==077  & year>=2002
replace industry=4 if INDN>=107 & INDN<=399 & year>=2002
replace industry=4 if INDN>=647 & INDN<=659 & year>=2002
replace industry=4 if INDN>=678 & INDN<=679 & year>=2002
replace industry=5 if INDN>=57 & INDN<=69 & year>=2002
replace industry=5 if INDN>=607 & INDN<=639 & year>=2002
replace industry=5 if INDN>=667 & INDN<=669 & year>=2002
replace industry=6 if INDN>=407 & INDN<=579 & year>=2002
replace industry=6 if INDN>=868 & INDN<=869 & year>=2002
replace industry=7 if INDN>= 687 & INDN<=719 & year>=2002
replace industry=8 if INDN>=877 & INDN<=879 & year>=2002
replace industry=8 if INDN==887 & year>=2002
replace industry=9 if INDN>=866 & INDN<=867 & year>=2002
replace industry=9 if INDN>=829 & INDN<=929 & year>=2002
replace industry=10 if INDN>=856 & INDN<=859 & year>=2002
replace industry=11 if INDN==677 & year>=2002
replace industry=11 if INDN>=727 & INDN<=779 & year>=2002
replace industry=11 if INDN>=786 & INDN<=847 & year>=2002
replace industry=12 if INDN>=937 & INDN<=989 & year>=2002

*quietly do "$do/Data Preparation/Other/do_input_industry"

sort ID year month
quietly replace industry=industry[_n-1] if JN==JN[_n-1] & ID==ID[_n-1] & industry==. & industry[_n-1]>0 & industry[_n-1]~=. & JN~=.
quietly forvalues i = 1/85 {
quietly replace industry=industry[_n+1] if JN==JN[_n+1] & ID==ID[_n+1] & industry==. & industry[_n+1]>0  & industry[_n+1]~=. & JN~=.
}

**b. occupation fe
**major occupation categories as in Autor and Dorn (2013)
gen occupation_agg=1 if (occ1990dd>=3 & occ1990dd<=37) | (occ1990dd>=43 & occ1990dd<=200) | (occ1990dd>=203 & occ1990dd<=235) | (occ1990dd>=243 & occ1990dd<=258) | (occ1990dd>=417 & occ1990dd<=423)
replace occupation_agg=2 if (occ1990dd>=303 & occ1990dd<=389) | (occ1990dd>=274 & occ1990dd<=283)
replace occupation_agg=3 if  (occ1990dd>=405 & occ1990dd<=408) | (occ1990dd==415) | (occ1990dd>=425 & occ1990dd<=427) | (occ1990dd>=433 & occ1990dd<=444) | (occ1990dd>=445 & occ1990dd<=447) | (occ1990dd>=448 & occ1990dd<=455) | (occ1990dd>=457 & occ1990dd<=458) | (occ1990dd>=459 & occ1990dd<=467) | (occ1990dd==468) | (occ1990dd>=469 & occ1990dd<=472)
replace occupation_agg=4  if (occ1990dd>=628 & occ1990dd<=699)
replace occupation_agg=5  if (occ1990dd>=703 & occ1990dd<=799)
replace occupation_agg=6  if (occ1990dd>=803 & occ1990dd<=889) |  (occ1990dd>=558 & occ1990dd<=599) | (occ1990dd>=503 & occ1990dd<=549) |  (occ1990dd>=614 & occ1990dd<=617) | (occ1990dd>=473 & occ1990dd<=475) | (occ1990dd>=479 & occ1990dd<=498)

**c. age
gen age=year-yob-1 if month<mob
replace age=year-yob if month>=mob
gen agesq=age^2
gen agecb=age^3

**d. gender
gen gender=0 if male==1
replace gender=1 if female==1

**e. race
gen race=0 if white==1
replace race=1 if black==1
replace race=2 if hisp==1

**f. region of residence
gen regionfe=region if region>0

**g. education
gen educ_aux=hgraderev[_n-1] if year==year[_n-1]+1 & month==1 & ID==ID[_n-1] & hgraderev[_n-1]>0 & hgraderev<0
gen educ_aux2=hgraderev[_n+1] if year==year[_n+1]-1 & month==12 & ID==ID[_n+1] & hgraderev[_n+1]>0 & hgraderev<0
bysort ID year: egen educ_aux_max=max(educ_aux)
bysort ID year: egen educ_aux_max2=max(educ_aux2)
replace educ_aux_max=hgraderev if (hgraderev~=. & educ_aux_max==.) | (hgraderev>0 & educ_aux_max==.)
replace educ_aux_max2=hgraderev if (hgraderev~=. & educ_aux_max2==.) | (hgraderev>0 & educ_aux_max2==.)
gen hgraderev_alternative=educ_aux_max if educ_aux_max2==educ_aux_max
bysort ID: egen max_educ=max(hgraderev)
bysort ID: egen gradyear_2=max(gradyear)
replace hgraderev_alternative=max_educ if (hgraderev_alternative==. & year>gradyear_2) | (hgraderev_alternative<0 & year>gradyear_2)  
sort ID year month
gen aux_educ3=1 if hgraderev_alternative~=hgraderev_alternative[_n-1] & ID==ID[_n-1] & hgraderev_alternative~=. & hgraderev_alternative[_n-1]~=.
gen aux_grad_year_3=year if aux_educ3==1 & hgraderev_alternative==max_educ
bysort ID: egen gradyear_3=max(aux_grad_year_3)
replace hgraderev_alternative=max_educ if (hgraderev_alternative==. & year>gradyear_3) | (hgraderev_alternative<0 & year>gradyear_3)  
*has a college degree
gen dummy_educ=1 if hgraderev_alternative>15 & hgraderev_alternative~=.
*does not have a college degree
replace dummy_educ=0 if hgraderev_alternative<=15 & hgraderev_alternative>0 & hgraderev_alternative~=.

**h. time trend
sort ID year month
gen time_trend=1 if year==1979 & month==1
replace time_trend=time_trend[_n-1]+1 if ID==ID[_n-1]
label var time_trend "time trend"

********************************************************************************************************

*3. mismatch measure

**3.1 difference between ability and occupational rank in each percentile
foreach var in math verbal technical social {

gen aux4_`var'_weighted=`var'_perc-`var'_perc_occ_weighted

gen aux2_`var'_weighted=abs(`var'_perc-`var'_perc_occ_weighted)

gen aux4_`var'_pos_weighted=aux4_`var'_weighted
replace aux4_`var'_pos_weighted=0 if aux4_`var'_weighted<=0 & aux4_`var'_weighted~=.

gen aux4_`var'_neg_weighted=abs(aux4_`var'_weighted)
replace aux4_`var'_neg_weighted=0 if aux4_`var'_weighted>=0 & aux4_`var'_weighted~=.
}

**3.2 total mismatch
gen mismatch1w=0.25*abs(aux4_math_weighted)+0.25*abs(aux4_verbal_weighted)+0.25*abs(aux4_technical_weighted)+0.25*abs(aux4_social_weighted)
label var mismatch1w "skill mismatch (equally weighted)"

**3.4 positive and negative mismatch
gen mismatch1w_pos=0.25*aux4_math_pos_weighted+0.25*aux4_verbal_pos_weighted+0.25*aux4_technical_pos_weighted+0.25*aux4_social_pos_weighted
gen mismatch1w_neg=0.25*abs(aux4_math_neg_weighted)+0.25*abs(aux4_verbal_neg_weighted)+0.25*abs(aux4_technical_neg_weighted)+0.25*abs(aux4_social_neg_weighted)
label var mismatch1w_pos "overq. (equally weighted)"
label var mismatch1w_neg "underq. (equally weighted)"

********************************************************************************************************

*4. tenure measures

**4.1 employment tenure
sort ID year month
gen time=1 
replace time=time[_n-1]+1 if JN==JN[_n-1] & ID==ID[_n-1] & JN~=. & JN[_n-1]~=.
replace time=. if JN==.
label var time "employment tenure"

**4.2 labormarket experience.
sort ID year month
gen time_aux2=1 
replace time_aux2=0 if JN==.
bysort ID: ge time_cum=sum(time_aux2)
drop time_aux2
label var time_cum "labor market experience"

*************************************************************************************************************************

*5. add business cycle measures 
 
sort year month 
merge year month using "$data/Macro indicators/agg_unemp"
drop if _m==2
drop _m


sort year month regionfe 
merge year month regionfe using "$data/Macro indicators/reg_unemp"
drop if _m==2
drop _m

keep if gender==0

save "$data/appendix/data_analysis_mil4", replace

*************************************************************************************************************************
