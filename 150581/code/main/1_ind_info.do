
*note: this files puts together individual-leve data 

clear all

tempfile social

**********************************************************************************************************+
*1. add date of birth
infile using "$data/NLSY/individual info/datebirth.dct", clear 
rename R0000100 ID
rename R0000300 mob   
rename R0000500 yob  
rename R0173600 SAMP 
replace yob=yob+1900
label var yob "year of birth"
label var mob "month of birth"
sort ID 
save "$data/_individualfile.dta", replace

**********************************************************************************************************+

*2. add region  
infile using "$data/NLSY/individual info/region.dct", clear
rename R0000100 ID 
rename R0216400 REGION_1979 
rename R0405700 REGION_1980 
rename R0602810 REGION_1981 
rename R0897910 REGION_1982 
rename R1144800 REGION_1983 
rename R1520000 REGION_1984 
rename R1890700 REGION_1985 
rename R2257800 REGION_1986 
rename R2445200 REGION_1987 
rename R2870800 REGION_1988 
rename R3074500 REGION_1989 
rename R3401200 REGION_1990 
rename R3656600 REGION_1991 
rename R4007100 REGION_1992 
rename R4418200 REGION_1993 
rename R5081200 REGION_1994 
rename R5166500 REGION_1996 
rename R6479100 REGION_1998 
rename R7006800 REGION_2000 
rename R7704100 REGION_2002 
rename R8496500 REGION_2004 
rename T0988300 REGION_2006 
rename T2210300 REGION_2008 
rename T3108200 REGION_2010 
rename T4112700 REGION_2012 
rename T5023100 REGION_2014 
rename T5771000 REGION_2016 

reshape i ID   
reshape j year   
reshape xij REGION_   
reshape long   
rename REGION_ region
label var region "region"

sort ID year
merge m:1 ID using "$data/_individualfile.dta"
drop _m
sort ID year
save "$data/_individualfile.dta", replace


**********************************************************************************************************+

*2. add education
*2.1 highest degree achieved
infile using "$data/NLSY/individual info/education.dct", clear 
cap drop A0002500  R0214700 R0214800
rename R0000100 ID
rename R0216701 HGCREV1979 
rename R0216601 ENROLLMTREV1979 
rename R0406401 HGCREV1980 
rename R0406501 ENROLLMTREV1980 
rename R0618901 HGCREV1981 
rename R0619001 ENROLLMTREV1981 
rename R0898201 HGCREV1982 
rename R0898301 ENROLLMTREV1982 
rename R1145001 HGCREV1983 
rename R1145101 ENROLLMTREV1983 
rename R1520201 HGCREV1984 
rename R1520301 ENROLLMTREV1984 
rename R1890901 HGCREV1985 
rename R1891001 ENROLLMTREV1985 
rename R2258001 HGCREV1986 
rename R2258101 ENROLLMTREV1986 
rename R2445401 HGCREV1987 
rename R2445501 ENROLLMTREV1987
rename R2871101 HGCREV1988 
rename R2871201 ENROLLMTREV1988 
rename R3074801 HGCREV1989 
rename R3074901 ENROLLMTREV1989 
rename R3401501 HGCREV1990 
rename R3401601 ENROLLMTREV1990 
rename R3656901 HGCREV1991 
rename R3657001 ENROLLMTREV1991 
rename R4007401 HGCREV1992 
rename R4007501 ENROLLMTREV1992 
rename R4418501 HGCREV1993 
rename R4418601 ENROLLMTREV1993 
rename R5103900 HGCREV1994 
rename R5104000 ENROLLMTREV1994 
rename R5166901 HGCREV1996 
rename R5166902 ENROLLMTREV1996 
rename R6479600 HGCREV1998 
rename R6479700 ENROLLMTREV1998 
rename R7007300 HGCREV2000 
rename R7007400 ENROLLMTREV2000 
rename R7704600 HGCREV2002 
rename R7704700 ENROLLMTREV2002 
rename R8497000 HGCREV2004 
rename R8497100 ENROLLMTREV2004 
rename T0988800 HGCREV2006 
rename T0988900 ENROLLMTREV2006 
rename T2210700 HGCREV2008 
rename T3108600 HGCREV2010 
rename T4113100 HGCREV2012 
  
reshape i ID
reshape j year
reshape xij HGCREV  ENROLLMTREV
reshape long
 
rename HGCREV hgraderev
rename ENROLLMTREV enrollrev
label define venrollrev   -5 "missing" -3 "Skip" 1 "NOT ENROLLED, COMPLETED LESS THAN 12TH GRADE"  2 "ENROLLED IN HIGH SCHOOL"  3 "ENROLLED IN COLLEGE"  4 "NOT ENROLLED, HIGH SCHOOL GRADUATE"
label values enrollrev venrollrev
label var enrollrev "enrollment in education"
label define vhgraderev   0 "NONE"  1 "1ST GRADE"  2 "2ND GRADE"  3 "3RD GRADE"  4 "4TH GRADE"  5 "5TH GRADE"  6 "6TH GRADE"  7 "7TH GRADE"  8 "8TH GRADE"  9 "9TH GRADE"  10 "10TH GRADE"  11 "11TH GRADE"  12 "12TH GRADE"  13 "1ST YR COL"  14 "2ND YR COL"  15 "3RD YR COL"  16 "4TH YR COL"  17 "5TH YR COL"  18 "6TH YR COL"  19 "7TH YR COL"  20 "8TH YR COL OR MORE"  95 "UNGRADED"
label values hgraderev vhgraderev
label var hgraderev "highest grade achieved"

sort ID year
merge (ID year) using "$data/_individualfile.dta"
drop _merge
sort ID year

save "$data/_individualfile.dta", replace

*2.2 graduation year

infile using "$data/NLSY/individual info/education_2.DCT", clear 

drop R0214700 R0214800

rename R0000100 ID
rename R0018200  hs1979
rename  R0018300  ged_yn1979 
rename  R0018400  hs_mo1979
rename  R0018500  hs_yr1979
rename  R0139200  coll_1mo1979  
rename  R0139400  coll_2mo1979  
rename  R0139600  coll_3mo1979  
rename  R0139800  coll_4mo1979  
rename R0229900  hs1980
rename  R0230000  ged_yn1980  
rename  R0230100  hs_mo1980  
rename  R0230200  hs_yr1980  
rename  R0297800  coll_1mo1980  
rename  R0298400  coll_2mo1980  
rename  R0297300  coll1980  
rename  R0297400  coll_1t1980  
rename  R0297810  coll_1yr1980  
rename  R0298000  coll_2t1980  
rename  R0298410  coll_2yr1980
rename  R0418100  hs1981  
rename  R0418200  ged_yn1981  
rename  R0418300  hs_mo1981  
rename  R0418400  hs_yr1981  
rename  R0419800  coll1981  
rename  R0419900  coll_1t1981  
rename  R0420100  coll_2t1981  
rename  R0665200  hs1982  
rename  R0665300  ged_yn1982  
rename  R0665400  hs_mo1982  
rename  R0665500  hs_yr1982  
rename  R0667400  coll1982  
rename  R0667500  coll_1t1982  
rename  R0667700  coll_2t1982  
rename  R0906600  hs1983  
rename  R0906700  ged_yn1983  
rename  R0906800  hs_mo1983  
rename  R0906900  hs_yr1983  
rename  R0908100  coll1983
rename  R0908200  coll_1t1983  
rename  R0908400  coll_2t1983
rename  R1206500  hs1984
rename  R1206600  ged_yn1984  
rename  R1206700  hs_mo1984  
rename  R1206800  hs_yr1984  
rename  R1214300  coll1984
rename  R1214400  coll_1t1984  
rename  R1214600  coll_2t1984    
rename  R1605800  hs1985  
rename  R1605900  ged_yn1985  
rename  R1606000  hs_mo1985  
rename  R1606100  hs_yr1985  
rename  R1906000  hs1986  
rename  R1906100  ged_yn1986  
rename  R1906200  hs_mo1986  
rename  R1906300  hs_yr1986  
rename  R2306900  hs1987  
rename  R2307000  ged_yn1987  
rename  R2307100  hs_mo1987  
rename  R2307200  hs_yr1987  
rename  R2509400  hs1988  
rename  R2509500  ged_yn1988  
rename  R2509600  hs_mo1988  
rename  R2509700  hs_yr1988
rename  R2509900  hdeg_mo1988
rename  R2510000  hdeg_yr1988
rename  R2908500  hs1989  
rename  R2908600  ged_yn1989  
rename  R2908700  hs_mo1989  
rename  R2908800  hs_yr1989  
rename  R2909300  hdeg_mo1989
rename  R2909400  hdeg_yr1989
rename  R3110600  hs1990  
rename  R3110700  ged_yn1990  
rename  R3110800  hs_mo1990  
rename  R3110900  hs_yr1990  
rename  R3111300  hdeg_mo1990
rename  R3111400  hdeg_yr1990
rename  R3510600  hs1991  
rename  R3510700  ged_yn1991  
rename  R3510800  hs_mo1991  
rename  R3510900  hs_yr1991
rename  R3511300  hdeg_mo1991
rename  R3511400  hdeg_yr1991
rename  R3710600  hs1992  
rename  R3710700  ged_yn1992  
rename  R3710800  hs_mo1992  
rename  R3710900  hs_yr1992
rename  R3711300  hdeg_mo1992
rename  R3711400  hdeg_yr1992
rename  R4138400  hs1993  
rename  R4138500  ged_yn1993  
rename  R4138600  hs_mo1993  
rename  R4138601  hs_yr1993
rename  R4139000  hdeg_mo1993  
rename  R4139001  hdeg_yr1993  
rename  R4527000  hs1994  
rename  R4527100  ged_yn1994  
rename  R4527200  ged_mo1994  
rename  R4527201  ged_yr1994  
rename  R4527300  hs_mo1994  
rename  R4527301  hs_yr1994
rename  R4527700  hdeg_mo1994  
rename  R4527701  hdeg_yr1994    
rename  R5222300  hs1996  
rename  R5222400  ged_yn1996  
rename  R5222500  ged_mo1996  
rename  R5222501  ged_yr1996  
rename  R5222600  hs_mo1996  
rename  R5222601  hs_yr1996  
rename  R5223000  hdeg_mo1996  
rename  R5223001  hdeg_yr1996
rename  R5822200  hs1998  
rename  R5822300  ged_yn1998  
rename  R5822400  ged_mo1998  
rename  R5822401  ged_yr1998  
rename  R5822500  hs_mo1998  
rename  R5822501  hs_yr1998  
rename  R5822900  hdeg_mo1998  
rename  R5822901  hdeg_yr1998  
rename  R6540800  hs2000  
rename  R6540900  ged_yn2000  
rename  R6541000  ged_mo2000  
rename  R6541001  ged_yr2000  
rename  R6541100  hs_mo2000  
rename  R6541101  hs_yr2000  
rename  R6541500  hdeg_mo2000  
rename  R6541501  hdeg_yr2000  

reshape i ID
reshape j year
reshape xij hs ged_yn ged_mo ged_yr hs_mo hs_yr hdeg_mo hdeg_yr coll_1mo coll_2mo coll_3mo coll_4mo coll_1t coll_1yr coll_2t coll_2yr coll
reshape long

label var coll "RECEIVED ANY TYPE OF COLLEGE DEGREE OR CERTIFICATE SINCE LAST INT?"
label var hdeg_mo "MONTH RECEIVED HIGHEST DEGREE"
label var hdeg_yr "YEAR RECEIVED HIGHEST DEGREE"
label var ged_mo "MONTH RECEIVED HIGH SCHOOL DIPLOMA/GED"
label var ged_yr "YEAR RECEIVED HIGH SCHOOL DIPLOMA/GED"

sort ID year
merge (ID year) using "$data/_individualfile.dta"
drop _merge
sort ID year

save "$data/_individualfile.dta", replace

infile using "$data/NLSY/individual info/education_3.DCT", clear 
rename R0000100 ID 
rename R7104000 hs2002   
rename R7104100 ged_yn2002  
rename R7104200 ged_mo2002   
rename R7104201 ged_yr2002   
rename R7104300 hs_mo2002   
rename R7104301 hs_yr2002  
rename R7104700 hdeg_mo2002  
rename R7104701 hdeg_yr2002	
rename R7810900 hs2004   
rename R7811000 ged_yn2004  
rename R7811100 ged_mo2004   
rename R7811101 ged_yr2004   
rename R7811200 hs_mo2004   
rename R7811201 hs_yr2004  
rename R7811600 hdeg_mo2004   
rename R7811601 hdeg_yr2004  
rename T0014800 hs2006   
rename T0014900 ged_yn2006  
rename T0015000 ged_mo2006  
rename T0015001 ged_yr2006 
rename T0015100 hs_mo2006  
rename T0015101 hs_yr2006   
rename T0015500 hdeg_mo2006  
rename T0015501 hdeg_yr2006  
rename T1214800 hs2008   
rename T1214900 ged_yn2008   
rename T1215000 ged_mo2008  
rename T1215001 ged_yr2008   
rename T1215100 hs_mo2008   
rename T1215101 hs_yr2008  
rename T1215500 hdeg_mo2008 
rename T1215501 hdeg_yr2008   
rename T1215700 hdeg_mo_2_2008  
rename T1215701 hdeg_yr_2_2008  
rename T2273300 hs2010   
rename T2273400 ged_yn2010  
rename T2273500 ged_mo2010  
rename T2273501 ged_yr2010  
rename T2273600 hs_mo2010   
rename T2273601 hs_yr2010   
rename T2274000 hdeg_mo2010   
rename T2274200 hdeg_mo_2_2010  
rename T3213400 hs2012   
rename T3213500 ged_yn2012   
rename T3213600 ged_mo2012 
rename T3213601 ged_yr2012  
rename T3213700 hs_mo2012   
rename T3213701 hs_yr2012 
rename T3214100 hdeg_mo2012   
rename T3214300 hdeg_mo_2_2012   
  
reshape i ID
reshape j year
reshape xij hs  ged_yn ged_mo ged_yr hs_mo hs_yr hdeg_mo hdeg_yr hdeg_mo_2_ hdeg_yr_2_
reshape long

label var hdeg_mo "MONTH RECEIVED HIGHEST DEGREE"
label var hdeg_yr "YEAR RECEIVED HIGHEST DEGREE"
label var ged_mo "MONTH RECEIVED HIGH SCHOOL DIPLOMA/GED"
label var ged_yr "YEAR RECEIVED HIGH SCHOOL DIPLOMA/GED"


sort ID year
merge (ID year) using "$data/_individualfile.dta"
drop _merge
sort ID year

***max education level
bysort ID: egen max_educ=max(hgraderev)
replace hs_yr=. if hs_yr<0
replace hs_yr=hs_yr+1900 if hs_yr>0 & hs_yr<1000

replace hdeg_yr=. if hdeg_yr<0
replace hdeg_yr=hdeg_yr+1900 if hdeg_yr>0 & hdeg_yr<1000
bysort ID: egen all_aux=min(hdeg_yr)

replace ged_yr=. if ged_yr<0
bysort ID: egen ged_aux=min(ged_yr)

**high school graduation year
bysort ID: egen hs_gradyear=min(hs_yr)
replace hs_gradyear=ged_aux if hs_gradyear==.
***graduation year.
gen college_gradyear=all_aux if all_aux>hs_gradyear & max_educ>12
gen gradyear=college_gradyear
replace gradyear=hs_gradyear  if gradyear==.

drop ged* coll_* max* *_aux  hs hs_mo hs_yr hdeg_mo hdeg_yr coll
sort ID year

save "$data/_individualfile.dta", replace


**********************************************************************************************************+

*6. add weights
infile using "$data/NLSY/individual info/weights.dct", clear
rename R0000100 ID
rename R0216100 SAMPWEIGHT_1979 
rename R0216101 C_SAMPWEIGHT_1979 
rename R0405200 SAMPWEIGHT_1980 
rename R0405201 C_SAMPWEIGHT_1980 
rename R0614600 SAMPWEIGHT_1981 
rename R0614601 C_SAMPWEIGHT_1981 
rename R0614700 SAMPWEIGHT_2_1981 
rename R0896700 SAMPWEIGHT_1982 
rename R0896701 C_SAMPWEIGHT_1982 
rename R1144400 SAMPWEIGHT_1983 
rename R1144401 C_SAMPWEIGHT_1983 
rename R1519600 SAMPWEIGHT_1984 
rename R1519601 C_SAMPWEIGHT_1984 
rename R1890200 SAMPWEIGHT_1985 
rename R1890201 C_SAMPWEIGHT_1985 
rename R2257300 SAMPWEIGHT_1986 
rename R2257301 C_SAMPWEIGHT_1986 
rename R2444500 SAMPWEIGHT_1987 
rename R2444501 C_SAMPWEIGHT_1987 
rename R2870000 SAMPWEIGHT_1988 
rename R2870001 C_SAMPWEIGHT_1988 
rename R3073800 SAMPWEIGHT_1989 
rename R3073801 C_SAMPWEIGHT_1989 
rename R3400200 SAMPWEIGHT_1990 
rename R3400201 C_SAMPWEIGHT_1990 
rename R3655800 SAMPWEIGHT_1991 
rename R3655801 C_SAMPWEIGHT_1991 
rename R4006300 SAMPWEIGHT_1992 
rename R4006301 C_SAMPWEIGHT_1992 
rename R4417400 SAMPWEIGHT_1993 
rename R4417401 C_SAMPWEIGHT_1993 
rename R5080400 SAMPWEIGHT_1994 
rename R5080401 C_SAMPWEIGHT_1994 
rename R5165700 SAMPWEIGHT_1996 
rename R5165701 C_SAMPWEIGHT_1996 
rename R6466300 SAMPWEIGHT_1998 
rename R6466301 C_SAMPWEIGHT_1998 
rename R7006200 SAMPWEIGHT_2000 
rename R7006201 C_SAMPWEIGHT_2000 
rename R7703400 SAMPWEIGHT_2002 
rename R7703401 C_SAMPWEIGHT_2002 
rename R8495700 SAMPWEIGHT_2004 
rename R8495800 C_SAMPWEIGHT_2004 
rename T0987300 SAMPWEIGHT_2006 
rename T0987400 C_SAMPWEIGHT_2006 
rename T2209600 SAMPWEIGHT_2008 
rename T2209700 C_SAMPWEIGHT_2008 
rename T3107400 SAMPWEIGHT_2010 
rename T3107500 C_SAMPWEIGHT_2010 
rename T4111900 SAMPWEIGHT_2012 
rename T4112000 C_SAMPWEIGHT_2012 
rename T5022100 SAMPWEIGHT_2014 
rename T5022200 C_SAMPWEIGHT_2014 
rename T5770400 SAMPWEIGHT_2016 
rename T5770500 C_SAMPWEIGHT_2016 

reshape i ID  
reshape j year 
reshape xij SAMPWEIGHT_  C_SAMPWEIGHT_  
reshape long   
rename SAMPWEIGHT_ sampweight
rename C_SAMPWEIGHT_ cross_sampweight
rename SAMPWEIGHT_2 sampweight_tests
label var sampweight "annual sampling weights"


sort ID year
merge (ID year) using "$data/_individualfile.dta"
drop _merge
sort ID year

save "$data/_individualfile.dta", replace


**********************************************************************************************************+

*7. add tests scores
*7.1 social skill measures
infile using "$data/NLSY/individual info/socialscores.dct", clear
rename R0000100 ID
rename R0153710 rotter_score
rename R0304410 rosenberg_score

sort ID
save `social', replace

*7.2 ASVAB scores
infile using "$data/NLSY/individual info/testscores.DCT", clear
rename R0000100 ID
rename R0173600 SAMP
rename R0615100 AR_score 
rename R0615200 WK_score   
rename R0615300 PC_score 
rename R0615700 MK_score   
rename R0615800 MC_score 
rename R0615900 EI_score 
rename R0615000 GS_score   
label var AR_score "ARITHMETIC REASONING - ASVAB VOCATIONAL TEST"
label var WK_score "WORD KNOWLEDGE - ASVAB VOCATIONAL TEST"
label var PC_score "PARAGRAPH COMP - ASVAB VOCATIONAL TEST"
label var MK_score "MATHEMATICS KNOWLEDGE - ASVAB VOCATIONAL TEST"
label var MC_score "GENERAL SCIENCE - ASVAB VOCATIONAL TEST"
label var EI_score "MECHANICAL COMP - ASVAB VOCATIONAL TEST"
label var GS_score "ELECTRONICS INFO - ASVAB VOCATIONAL TEST"
sort ID
*merges social scores
merge ID using `social'
drop _m
drop if AR_score==-4 | WK_score==-4 | PC_score==-4 | MK_score==-4  | MC_score==-4 | EI_score==-4 | GS_score==-4 | rosenberg<0 | rotter<0
replace  rotter_score=rotter_score*-1
sort ID
merge 1:m (ID) using "$data/_individualfile.dta"
gen test_info=0 if _m==3
replace test_info=1 if _m==2
label variable  test_info "test scores not available"
drop _merge

sort ID year
save "$data/_individualfile.dta", replace

**********************************************************************************************************+

*9. add indicator variable: not interviewed that year 
infile using "$data/NLSY/individual info/nonint.dct", clear 
rename R0000100 ID
rename R0406310 NI1980 
rename R0618810 NI1981 
rename R0898510 NI1982 
rename R1144710 NI1983 
rename R1519910 NI1984 
rename R1890300 NI1985 
rename R2257400 NI1986 
rename R2444600 NI1987 
rename R2870100 NI1988 
rename R3073900 NI1989 
rename R3400500 NI1990 
rename R3655900 NI1991 
rename R4006400 NI1992 
rename R4417500 NI1993 
rename R5080500 NI1994 
rename R5165800 NI1996 
rename R6478500 NI1998 
rename R7006300 NI2000 
rename R7703500 NI2002 
rename R8495900 NI2004 
rename T0987500 NI2006 
rename T2209800 NI2008 
rename T3107600 NI2010 
rename T4112100 NI2012
rename T5022300 NI2014
rename T5770600 NI2016 

reshape i ID 
reshape j year   
reshape xij NI   
reshape long  
label var NI "Not interviewed"
gen died=1 if NI==65
label var died "Died"
replace NI = 1 if NI>=-3
replace NI = 0 if NI~=1

sort ID year
merge (ID year) using "$data/_individualfile.dta"
drop _m

***********************************************************************************************************

*10. keeping only the cross-sectional sample
drop if SAMP>8

***********************************************************************************************************

*11. construct race & gender variables 
gen black = 0
gen hisp = 0 
gen white=0
replace black=1 if SAMP==3|SAMP==7
replace hisp=1 if SAMP==4|SAMP==8
replace white=1 if SAMP==1|SAMP==2|SAMP==5|SAMP==6

gen male=0
gen female=0
replace male=1 if (SAMP>=1&SAMP<=4)
replace female=1 if (SAMP>=5&SAMP<=8)

assert black+hisp+white==1
assert male+female==1

***********************************************************************************************************

sort ID year
save "$data/_individualfile.dta", replace

************************************************************************************************************

*12. build two files, one with fixe characteristics over time

use "$data/_individualfile.dta", clear
keep ID year NI died sampweight cross_sampweight hgraderev enrollrev region hs_gradyear college_gradyear gradyear
sort ID year
save "$data/_individualfile_y.dta", replace

use "$data/_individualfile.dta", clear
keep ID SAMP mob yob black hisp white male female *_score rosenberg rotter test_info sampweight_tests
duplicates drop ID, force
save "$data/_individualfile.dta", replace

************************************************************************************************************

*13. construct ability ranks
use "$data/_individualfile.dta", clear
keep ID yob AR WK PC MK MC EI GS rosenberg rotter test_info
drop if test_info==1

gen age81=1981-yob
tabulate age81, generate(g)

foreach var in AR WK PC MK MC EI GS rosenberg rotter{
gen `var'_zscore=.
forval i=1/8{
sum `var'_score if g`i'==1, detail
replace `var'_zscore=(`var'_score-r(mean))/r(sd) if g`i'==1
}
}

*pca
*math
gen math=.
forval i=1/8{
pca  AR_zscore MK_zscore if g`i'==1
predict math`i' if g`i'==1
replace math=math`i' if g`i'==1
drop math`i'
}

*verbal
gen verbal=.
forval i=1/8{
pca  WK_zscore PC_zscore if g`i'==1
predict verbal`i' if g`i'==1
replace verbal=verbal`i' if g`i'==1
drop verbal`i'
}

*technical
gen technical=.
forval i=1/8{
pca  MC_zscore GS_zscore EI_zscore if g`i'==1
predict technical`i' if g`i'==1
replace technical=technical`i' if g`i'==1
drop technical`i'
}

*social
gen social=.
forval i=1/8{
pca  rosenberg_zscore rotter_zscore if g`i'==1
predict social`i' if g`i'==1
replace social=social`i' if g`i'==1
drop social`i'
}

foreach var in math verbal technical social {
	
gen `var'_perc=.

label variable  `var'_perc "`var' score"
	
forvalues i=1/8{	
egen pct_`var'_`i'= xtile(`var'), by(g`i') nq(100) 
replace pct_`var'_`i'=. if  g`i'==0
replace `var'_perc=pct_`var'_`i' if g`i'==1

}		 
}

keep ID *_perc
sort ID

merge ID using "$data/_individualfile.dta", keep(ID SAMP mob yob black hisp white male female test_info)
drop _m
sort ID
save "$data/_individualfile.dta", replace









