#delimit ;

** renaming the weeks in weekly labour force status variables to give actual numbers **;

qui rename R0000100 ID;
qui rename R0173600 SAMP;

qui cap  rename W0061200 W0;
qui cap  rename W0061300 W1;
qui cap  rename W0061400 W2;
qui cap  rename W0061500 W3;
qui cap  rename W0061600 W4;
qui cap  rename W0061700 W5;
qui cap  rename W0061800 W6;
qui cap  rename W0061900 W7;
qui cap  rename W0062000 W8;
qui cap  rename W0062100 W9;
qui cap  rename W0062200 W10;
qui cap  rename W0062300 W11;
qui cap  rename W0062400 W12;
qui cap  rename W0062500 W13;
qui cap  rename W0062600 W14;
qui cap  rename W0062700 W15;
qui cap  rename W0062800 W16;
qui cap  rename W0062900 W17;
qui cap  rename W0063000 W18;
qui cap  rename W0063100 W19;
qui cap  rename W0063200 W20;
qui cap  rename W0063300 W21;
qui cap  rename W0063400 W22;
qui cap  rename W0063500 W23;
qui cap  rename W0063600 W24;
qui cap  rename W0063700 W25;
qui cap  rename W0063800 W26;
qui cap  rename W0063900 W27;
qui cap  rename W0064000 W28;
qui cap  rename W0064100 W29;
qui cap  rename W0064200 W30;
qui cap  rename W0064300 W31;
qui cap  rename W0064400 W32;
qui cap  rename W0064500 W33;
qui cap  rename W0064600 W34;
qui cap  rename W0064700 W35;
qui cap  rename W0064800 W36;
qui cap  rename W0064900 W37;
qui cap  rename W0065000 W38;
qui cap  rename W0065100 W39;
qui cap  rename W0065200 W40;
qui cap  rename W0065300 W41;
qui cap  rename W0065400 W42;
qui cap  rename W0065500 W43;
qui cap  rename W0065600 W44;
qui cap  rename W0065700 W45;
qui cap  rename W0065800 W46;
qui cap  rename W0065900 W47;
qui cap  rename W0066000 W48;
qui cap  rename W0066100 W49;
qui cap  rename W0066200 W50;
qui cap  rename W0066300 W51;
qui cap  rename W0066400 W52;
qui cap  rename W0066500 W53;
qui cap  rename W0066600 W54;
qui cap  rename W0066700 W55;
qui cap  rename W0066800 W56;
qui cap  rename W0066900 W57;
qui cap  rename W0067000 W58;
qui cap  rename W0067100 W59;
qui cap  rename W0067200 W60;
qui cap  rename W0067300 W61;
qui cap  rename W0067400 W62;
qui cap  rename W0067500 W63;
qui cap  rename W0067600 W64;
qui cap  rename W0067700 W65;
qui cap  rename W0067800 W66;
qui cap  rename W0067900 W67;
qui cap  rename W0068000 W68;
qui cap  rename W0068100 W69;
qui cap  rename W0068200 W70;
qui cap  rename W0068300 W71;
qui cap  rename W0068400 W72;
qui cap  rename W0068500 W73;
qui cap  rename W0068600 W74;
qui cap  rename W0068700 W75;
qui cap  rename W0068800 W76;
qui cap  rename W0068900 W77;
qui cap  rename W0069000 W78;
qui cap  rename W0069100 W79;
qui cap  rename W0069200 W80;
qui cap  rename W0069300 W81;
qui cap  rename W0069400 W82;
qui cap  rename W0069500 W83;
qui cap  rename W0069600 W84;
qui cap  rename W0069700 W85;
qui cap  rename W0069800 W86;
qui cap  rename W0069900 W87;
qui cap  rename W0070000 W88;
qui cap  rename W0070100 W89;
qui cap  rename W0070200 W90;
qui cap  rename W0070300 W91;
qui cap  rename W0070400 W92;
qui cap  rename W0070500 W93;
qui cap  rename W0070600 W94;
qui cap  rename W0070700 W95;
qui cap  rename W0070800 W96;
qui cap  rename W0070900 W97;
qui cap  rename W0071000 W98;
qui cap  rename W0071100 W99;
qui cap  rename W0071200 W100;
qui cap  rename W0071300 W101;
qui cap  rename W0071400 W102;
qui cap  rename W0071500 W103;
qui cap  rename W0071600 W104;
qui cap  rename W0071700 W105;
qui cap  rename W0106500 W106;
qui cap  rename W0106600 W107;
qui cap  rename W0106700 W108;
qui cap  rename W0106800 W109;
qui cap  rename W0106900 W110;
qui cap  rename W0107000 W111;
qui cap  rename W0107100 W112;
qui cap  rename W0107200 W113;
qui cap  rename W0107300 W114;
qui cap  rename W0107400 W115;
qui cap  rename W0107500 W116;
qui cap  rename W0107600 W117;
qui cap  rename W0107700 W118;
qui cap  rename W0107800 W119;
qui cap  rename W0107900 W120;
qui cap  rename W0108000 W121;
qui cap  rename W0108100 W122;
qui cap  rename W0108200 W123;
qui cap  rename W0108300 W124;
qui cap  rename W0108400 W125;
qui cap  rename W0108500 W126;
qui cap  rename W0108600 W127;
qui cap  rename W0108700 W128;
qui cap  rename W0108800 W129;
qui cap  rename W0108900 W130;
qui cap  rename W0109000 W131;
qui cap  rename W0109100 W132;
qui cap  rename W0109200 W133;
qui cap  rename W0109300 W134;
qui cap  rename W0109400 W135;
qui cap  rename W0109500 W136;
qui cap  rename W0109600 W137;
qui cap  rename W0109700 W138;
qui cap  rename W0109800 W139;
qui cap  rename W0109900 W140;
qui cap  rename W0110000 W141;
qui cap  rename W0110100 W142;
qui cap  rename W0110200 W143;
qui cap  rename W0110300 W144;
qui cap  rename W0110400 W145;
qui cap  rename W0110500 W146;
qui cap  rename W0110600 W147;
qui cap  rename W0110700 W148;
qui cap  rename W0110800 W149;
qui cap  rename W0110900 W150;
qui cap  rename W0111000 W151;
qui cap  rename W0111100 W152;
qui cap  rename W0111200 W153;
qui cap  rename W0111300 W154;
qui cap  rename W0111400 W155;
qui cap  rename W0111500 W156;
qui cap  rename W0111600 W157;
qui cap  rename W0146400 W158;
qui cap  rename W0146500 W159;
qui cap  rename W0146600 W160;
qui cap  rename W0146700 W161;
qui cap  rename W0146800 W162;
qui cap  rename W0146900 W163;
qui cap  rename W0147000 W164;
qui cap  rename W0147100 W165;
qui cap  rename W0147200 W166;
qui cap  rename W0147300 W167;
qui cap  rename W0147400 W168;
qui cap  rename W0147500 W169;
qui cap  rename W0147600 W170;
qui cap  rename W0147700 W171;
qui cap  rename W0147800 W172;
qui cap  rename W0147900 W173;
qui cap  rename W0148000 W174;
qui cap  rename W0148100 W175;
qui cap  rename W0148200 W176;
qui cap  rename W0148300 W177;
qui cap  rename W0148400 W178;
qui cap  rename W0148500 W179;
qui cap  rename W0148600 W180;
qui cap  rename W0148700 W181;
qui cap  rename W0148800 W182;
qui cap  rename W0148900 W183;
qui cap  rename W0149000 W184;
qui cap  rename W0149100 W185;
qui cap  rename W0149200 W186;
qui cap  rename W0149300 W187;
qui cap  rename W0149400 W188;
qui cap  rename W0149500 W189;
qui cap  rename W0149600 W190;
qui cap  rename W0149700 W191;
qui cap  rename W0149800 W192;
qui cap  rename W0149900 W193;
qui cap  rename W0150000 W194;
qui cap  rename W0150100 W195;
qui cap  rename W0150200 W196;
qui cap  rename W0150300 W197;
qui cap  rename W0150400 W198;
qui cap  rename W0150500 W199;
qui cap  rename W0150600 W200;
qui cap  rename W0150700 W201;
qui cap  rename W0150800 W202;
qui cap  rename W0150900 W203;
qui cap  rename W0151000 W204;
qui cap  rename W0151100 W205;
qui cap  rename W0151200 W206;
qui cap  rename W0151300 W207;
qui cap  rename W0151400 W208;
qui cap  rename W0151500 W209;
qui cap  rename W0186300 W210;
qui cap  rename W0186400 W211;
qui cap  rename W0186500 W212;
qui cap  rename W0186600 W213;
qui cap  rename W0186700 W214;
qui cap  rename W0186800 W215;
qui cap  rename W0186900 W216;
qui cap  rename W0187000 W217;
qui cap  rename W0187100 W218;
qui cap  rename W0187200 W219;
qui cap  rename W0187300 W220;
qui cap  rename W0187400 W221;
qui cap  rename W0187500 W222;
qui cap  rename W0187600 W223;
qui cap  rename W0187700 W224;
qui cap  rename W0187800 W225;
qui cap  rename W0187900 W226;
qui cap  rename W0188000 W227;
qui cap  rename W0188100 W228;
qui cap  rename W0188200 W229;
qui cap  rename W0188300 W230;
qui cap  rename W0188400 W231;
qui cap  rename W0188500 W232;
qui cap  rename W0188600 W233;
qui cap  rename W0188700 W234;
qui cap  rename W0188800 W235;
qui cap  rename W0188900 W236;
qui cap  rename W0189000 W237;
qui cap  rename W0189100 W238;
qui cap  rename W0189200 W239;
qui cap  rename W0189300 W240;
qui cap  rename W0189400 W241;
qui cap  rename W0189500 W242;
qui cap  rename W0189600 W243;
qui cap  rename W0189700 W244;
qui cap  rename W0189800 W245;
qui cap  rename W0189900 W246;
qui cap  rename W0190000 W247;
qui cap  rename W0190100 W248;
qui cap  rename W0190200 W249;
qui cap  rename W0190300 W250;
qui cap  rename W0190400 W251;
qui cap  rename W0190500 W252;
qui cap  rename W0190600 W253;
qui cap  rename W0190700 W254;
qui cap  rename W0190800 W255;
qui cap  rename W0190900 W256;
qui cap  rename W0191000 W257;
qui cap  rename W0191100 W258;
qui cap  rename W0191200 W259;
qui cap  rename W0191300 W260;
qui cap  rename W0191400 W261;
qui cap  rename W0226200 W262;
qui cap  rename W0226300 W263;
qui cap  rename W0226400 W264;
qui cap  rename W0226500 W265;
qui cap  rename W0226600 W266;
qui cap  rename W0226700 W267;
qui cap  rename W0226800 W268;
qui cap  rename W0226900 W269;
qui cap  rename W0227000 W270;
qui cap  rename W0227100 W271;
qui cap  rename W0227200 W272;
qui cap  rename W0227300 W273;
qui cap  rename W0227400 W274;
qui cap  rename W0227500 W275;
qui cap  rename W0227600 W276;
qui cap  rename W0227700 W277;
qui cap  rename W0227800 W278;
qui cap  rename W0227900 W279;
qui cap  rename W0228000 W280;
qui cap  rename W0228100 W281;
qui cap  rename W0228200 W282;
qui cap  rename W0228300 W283;
qui cap  rename W0228400 W284;
qui cap  rename W0228500 W285;
qui cap  rename W0228600 W286;
qui cap  rename W0228700 W287;
qui cap  rename W0228800 W288;
qui cap  rename W0228900 W289;
qui cap  rename W0229000 W290;
qui cap  rename W0229100 W291;
qui cap  rename W0229200 W292;
qui cap  rename W0229300 W293;
qui cap  rename W0229400 W294;
qui cap  rename W0229500 W295;
qui cap  rename W0229600 W296;
qui cap  rename W0229700 W297;
qui cap  rename W0229800 W298;
qui cap  rename W0229900 W299;
qui cap  rename W0230000 W300;
qui cap  rename W0230100 W301;
qui cap  rename W0230200 W302;
qui cap  rename W0230300 W303;
qui cap  rename W0230400 W304;
qui cap  rename W0230500 W305;
qui cap  rename W0230600 W306;
qui cap  rename W0230700 W307;
qui cap  rename W0230800 W308;
qui cap  rename W0230900 W309;
qui cap  rename W0231000 W310;
qui cap  rename W0231100 W311;
qui cap  rename W0231200 W312;
qui cap  rename W0231300 W313;
qui cap  rename W0266600 W314;
qui cap  rename W0266700 W315;
qui cap  rename W0266800 W316;
qui cap  rename W0266900 W317;
qui cap  rename W0267000 W318;
qui cap  rename W0267100 W319;
qui cap  rename W0267200 W320;
qui cap  rename W0267300 W321;
qui cap  rename W0267400 W322;
qui cap  rename W0267500 W323;
qui cap  rename W0267600 W324;
qui cap  rename W0267700 W325;
qui cap  rename W0267800 W326;
qui cap  rename W0267900 W327;
qui cap  rename W0268000 W328;
qui cap  rename W0268100 W329;
qui cap  rename W0268200 W330;
qui cap  rename W0268300 W331;
qui cap  rename W0268400 W332;
qui cap  rename W0268500 W333;
qui cap  rename W0268600 W334;
qui cap  rename W0268700 W335;
qui cap  rename W0268800 W336;
qui cap  rename W0268900 W337;
qui cap  rename W0269000 W338;
qui cap  rename W0269100 W339;
qui cap  rename W0269200 W340;
qui cap  rename W0269300 W341;
qui cap  rename W0269400 W342;
qui cap  rename W0269500 W343;
qui cap  rename W0269600 W344;
qui cap  rename W0269700 W345;
qui cap  rename W0269800 W346;
qui cap  rename W0269900 W347;
qui cap  rename W0270000 W348;
qui cap  rename W0270100 W349;
qui cap  rename W0270200 W350;
qui cap  rename W0270300 W351;
qui cap  rename W0270400 W352;
qui cap  rename W0270500 W353;
qui cap  rename W0270600 W354;
qui cap  rename W0270700 W355;
qui cap  rename W0270800 W356;
qui cap  rename W0270900 W357;
qui cap  rename W0271000 W358;
qui cap  rename W0271100 W359;
qui cap  rename W0271200 W360;
qui cap  rename W0271300 W361;
qui cap  rename W0271400 W362;
qui cap  rename W0271500 W363;
qui cap  rename W0271600 W364;
qui cap  rename W0271700 W365;
qui cap  rename W0271800 W366;
qui cap  rename W0306600 W367;
qui cap  rename W0306700 W368;
qui cap  rename W0306800 W369;
qui cap  rename W0306900 W370;
qui cap  rename W0307000 W371;
qui cap  rename W0307100 W372;
qui cap  rename W0307200 W373;
qui cap  rename W0307300 W374;
qui cap  rename W0307400 W375;
qui cap  rename W0307500 W376;
qui cap  rename W0307600 W377;
qui cap  rename W0307700 W378;
qui cap  rename W0307800 W379;
qui cap  rename W0307900 W380;
qui cap  rename W0308000 W381;
qui cap  rename W0308100 W382;
qui cap  rename W0308200 W383;
qui cap  rename W0308300 W384;
qui cap  rename W0308400 W385;
qui cap  rename W0308500 W386;
qui cap  rename W0308600 W387;
qui cap  rename W0308700 W388;
qui cap  rename W0308800 W389;
qui cap  rename W0308900 W390;
qui cap  rename W0309000 W391;
qui cap  rename W0309100 W392;
qui cap  rename W0309200 W393;
qui cap  rename W0309300 W394;
qui cap  rename W0309400 W395;
qui cap  rename W0309500 W396;
qui cap  rename W0309600 W397;
qui cap  rename W0309700 W398;
qui cap  rename W0309800 W399;
qui cap  rename W0309900 W400;
qui cap  rename W0310000 W401;
qui cap  rename W0310100 W402;
qui cap  rename W0310200 W403;
qui cap  rename W0310300 W404;
qui cap  rename W0310400 W405;
qui cap  rename W0310500 W406;
qui cap  rename W0310600 W407;
qui cap  rename W0310700 W408;
qui cap  rename W0310800 W409;
qui cap  rename W0310900 W410;
qui cap  rename W0311000 W411;
qui cap  rename W0311100 W412;
qui cap  rename W0311200 W413;
qui cap  rename W0311300 W414;
qui cap  rename W0311400 W415;
qui cap  rename W0311500 W416;
qui cap  rename W0311600 W417;
qui cap  rename W0311700 W418;
qui cap  rename W0346500 W419;
qui cap  rename W0346600 W420;
qui cap  rename W0346700 W421;
qui cap  rename W0346800 W422;
qui cap  rename W0346900 W423;
qui cap  rename W0347000 W424;
qui cap  rename W0347100 W425;
qui cap  rename W0347200 W426;
qui cap  rename W0347300 W427;
qui cap  rename W0347400 W428;
qui cap  rename W0347500 W429;
qui cap  rename W0347600 W430;
qui cap  rename W0347700 W431;
qui cap  rename W0347800 W432;
qui cap  rename W0347900 W433;
qui cap  rename W0348000 W434;
qui cap  rename W0348100 W435;
qui cap  rename W0348200 W436;
qui cap  rename W0348300 W437;
qui cap  rename W0348400 W438;
qui cap  rename W0348500 W439;
qui cap  rename W0348600 W440;
qui cap  rename W0348700 W441;
qui cap  rename W0348800 W442;
qui cap  rename W0348900 W443;
qui cap  rename W0349000 W444;
qui cap  rename W0349100 W445;
qui cap  rename W0349200 W446;
qui cap  rename W0349300 W447;
qui cap  rename W0349400 W448;
qui cap  rename W0349500 W449;
qui cap  rename W0349600 W450;
qui cap  rename W0349700 W451;
qui cap  rename W0349800 W452;
qui cap  rename W0349900 W453;
qui cap  rename W0350000 W454;
qui cap  rename W0350100 W455;
qui cap  rename W0350200 W456;
qui cap  rename W0350300 W457;
qui cap  rename W0350400 W458;
qui cap  rename W0350500 W459;
qui cap  rename W0350600 W460;
qui cap  rename W0350700 W461;
qui cap  rename W0350800 W462;
qui cap  rename W0350900 W463;
qui cap  rename W0351000 W464;
qui cap  rename W0351100 W465;
qui cap  rename W0351200 W466;
qui cap  rename W0351300 W467;
qui cap  rename W0351400 W468;
qui cap  rename W0351500 W469;
qui cap  rename W0351600 W470;
qui cap  rename W0386400 W471;
qui cap  rename W0386500 W472;
qui cap  rename W0386600 W473;
qui cap  rename W0386700 W474;
qui cap  rename W0386800 W475;
qui cap  rename W0386900 W476;
qui cap  rename W0387000 W477;
qui cap  rename W0387100 W478;
qui cap  rename W0387200 W479;
qui cap  rename W0387300 W480;
qui cap  rename W0387400 W481;
qui cap  rename W0387500 W482;
qui cap  rename W0387600 W483;
qui cap  rename W0387700 W484;
qui cap  rename W0387800 W485;
qui cap  rename W0387900 W486;
qui cap  rename W0388000 W487;
qui cap  rename W0388100 W488;
qui cap  rename W0388200 W489;
qui cap  rename W0388300 W490;
qui cap  rename W0388400 W491;
qui cap  rename W0388500 W492;
qui cap  rename W0388600 W493;
qui cap  rename W0388700 W494;
qui cap  rename W0388800 W495;
qui cap  rename W0388900 W496;
qui cap  rename W0389000 W497;
qui cap  rename W0389100 W498;
qui cap  rename W0389200 W499;
qui cap  rename W0389300 W500;
qui cap  rename W0389400 W501;
qui cap  rename W0389500 W502;
qui cap  rename W0389600 W503;
qui cap  rename W0389700 W504;
qui cap  rename W0389800 W505;
qui cap  rename W0389900 W506;
qui cap  rename W0390000 W507;
qui cap  rename W0390100 W508;
qui cap  rename W0390200 W509;
qui cap  rename W0390300 W510;
qui cap  rename W0390400 W511;
qui cap  rename W0390500 W512;
qui cap  rename W0390600 W513;
qui cap  rename W0390700 W514;
qui cap  rename W0390800 W515;
qui cap  rename W0390900 W516;
qui cap  rename W0391000 W517;
qui cap  rename W0391100 W518;
qui cap  rename W0391200 W519;
qui cap  rename W0391300 W520;
qui cap  rename W0391400 W521;
qui cap  rename W0391500 W522;
qui cap  rename W0426300 W523;
qui cap  rename W0426400 W524;
qui cap  rename W0426500 W525;
qui cap  rename W0426600 W526;
qui cap  rename W0426700 W527;
qui cap  rename W0426800 W528;
qui cap  rename W0426900 W529;
qui cap  rename W0427000 W530;
qui cap  rename W0427100 W531;
qui cap  rename W0427200 W532;
qui cap  rename W0427300 W533;
qui cap  rename W0427400 W534;
qui cap  rename W0427500 W535;
qui cap  rename W0427600 W536;
qui cap  rename W0427700 W537;
qui cap  rename W0427800 W538;
qui cap  rename W0427900 W539;
qui cap  rename W0428000 W540;
qui cap  rename W0428100 W541;
qui cap  rename W0428200 W542;
qui cap  rename W0428300 W543;
qui cap  rename W0428400 W544;
qui cap  rename W0428500 W545;
qui cap  rename W0428600 W546;
qui cap  rename W0428700 W547;
qui cap  rename W0428800 W548;
qui cap  rename W0428900 W549;
qui cap  rename W0429000 W550;
qui cap  rename W0429100 W551;
qui cap  rename W0429200 W552;
qui cap  rename W0429300 W553;
qui cap  rename W0429400 W554;
qui cap  rename W0429500 W555;
qui cap  rename W0429600 W556;
qui cap  rename W0429700 W557;
qui cap  rename W0429800 W558;
qui cap  rename W0429900 W559;
qui cap  rename W0430000 W560;
qui cap  rename W0430100 W561;
qui cap  rename W0430200 W562;
qui cap  rename W0430300 W563;
qui cap  rename W0430400 W564;
qui cap  rename W0430500 W565;
qui cap  rename W0430600 W566;
qui cap  rename W0430700 W567;
qui cap  rename W0430800 W568;
qui cap  rename W0430900 W569;
qui cap  rename W0431000 W570;
qui cap  rename W0431100 W571;
qui cap  rename W0431200 W572;
qui cap  rename W0431300 W573;
qui cap  rename W0431400 W574;
qui cap  rename W0466700 W575;
qui cap  rename W0466800 W576;
qui cap  rename W0466900 W577;
qui cap  rename W0467000 W578;
qui cap  rename W0467100 W579;
qui cap  rename W0467200 W580;
qui cap  rename W0467300 W581;
qui cap  rename W0467400 W582;
qui cap  rename W0467500 W583;
qui cap  rename W0467600 W584;
qui cap  rename W0467700 W585;
qui cap  rename W0467800 W586;
qui cap  rename W0467900 W587;
qui cap  rename W0468000 W588;
qui cap  rename W0468100 W589;
qui cap  rename W0468200 W590;
qui cap  rename W0468300 W591;
qui cap  rename W0468400 W592;
qui cap  rename W0468500 W593;
qui cap  rename W0468600 W594;
qui cap  rename W0468700 W595;
qui cap  rename W0468800 W596;
qui cap  rename W0468900 W597;
qui cap  rename W0469000 W598;
qui cap  rename W0469100 W599;
qui cap  rename W0469200 W600;
qui cap  rename W0469300 W601;
qui cap  rename W0469400 W602;
qui cap  rename W0469500 W603;
qui cap  rename W0469600 W604;
qui cap  rename W0469700 W605;
qui cap  rename W0469800 W606;
qui cap  rename W0469900 W607;
qui cap  rename W0470000 W608;
qui cap  rename W0470100 W609;
qui cap  rename W0470200 W610;
qui cap  rename W0470300 W611;
qui cap  rename W0470400 W612;
qui cap  rename W0470500 W613;
qui cap  rename W0470600 W614;
qui cap  rename W0470700 W615;
qui cap  rename W0470800 W616;
qui cap  rename W0470900 W617;
qui cap  rename W0471000 W618;
qui cap  rename W0471100 W619;
qui cap  rename W0471200 W620;
qui cap  rename W0471300 W621;
qui cap  rename W0471400 W622;
qui cap  rename W0471500 W623;
qui cap  rename W0471600 W624;
qui cap  rename W0471700 W625;
qui cap  rename W0471800 W626;
qui cap  rename W0471900 W627;
qui cap  rename W0506700 W628;
qui cap  rename W0506800 W629;
qui cap  rename W0506900 W630;
qui cap  rename W0507000 W631;
qui cap  rename W0507100 W632;
qui cap  rename W0507200 W633;
qui cap  rename W0507300 W634;
qui cap  rename W0507400 W635;
qui cap  rename W0507500 W636;
qui cap  rename W0507600 W637;
qui cap  rename W0507700 W638;
qui cap  rename W0507800 W639;
qui cap  rename W0507900 W640;
qui cap  rename W0508000 W641;
qui cap  rename W0508100 W642;
qui cap  rename W0508200 W643;
qui cap  rename W0508300 W644;
qui cap  rename W0508400 W645;
qui cap  rename W0508500 W646;
qui cap  rename W0508600 W647;
qui cap  rename W0508700 W648;
qui cap  rename W0508800 W649;
qui cap  rename W0508900 W650;
qui cap  rename W0509000 W651;
qui cap  rename W0509100 W652;
qui cap  rename W0509200 W653;
qui cap  rename W0509300 W654;
qui cap  rename W0509400 W655;
qui cap  rename W0509500 W656;
qui cap  rename W0509600 W657;
qui cap  rename W0509700 W658;
qui cap  rename W0509800 W659;
qui cap  rename W0509900 W660;
qui cap  rename W0510000 W661;
qui cap  rename W0510100 W662;
qui cap  rename W0510200 W663;
qui cap  rename W0510300 W664;
qui cap  rename W0510400 W665;
qui cap  rename W0510500 W666;
qui cap  rename W0510600 W667;
qui cap  rename W0510700 W668;
qui cap  rename W0510800 W669;
qui cap  rename W0510900 W670;
qui cap  rename W0511000 W671;
qui cap  rename W0511100 W672;
qui cap  rename W0511200 W673;
qui cap  rename W0511300 W674;
qui cap  rename W0511400 W675;
qui cap  rename W0511500 W676;
qui cap  rename W0511600 W677;
qui cap  rename W0511700 W678;
qui cap  rename W0511800 W679;
qui cap  rename W0546600 W680;
qui cap  rename W0546700 W681;
qui cap  rename W0546800 W682;
qui cap  rename W0546900 W683;
qui cap  rename W0547000 W684;
qui cap  rename W0547100 W685;
qui cap  rename W0547200 W686;
qui cap  rename W0547300 W687;
qui cap  rename W0547400 W688;
qui cap  rename W0547500 W689;
qui cap  rename W0547600 W690;
qui cap  rename W0547700 W691;
qui cap  rename W0547800 W692;
qui cap  rename W0547900 W693;
qui cap  rename W0548000 W694;
qui cap  rename W0548100 W695;
qui cap  rename W0548200 W696;
qui cap  rename W0548300 W697;
qui cap  rename W0548400 W698;
qui cap  rename W0548500 W699;
qui cap  rename W0548600 W700;
qui cap  rename W0548700 W701;
qui cap  rename W0548800 W702;
qui cap  rename W0548900 W703;
qui cap  rename W0549000 W704;
qui cap  rename W0549100 W705;
qui cap  rename W0549200 W706;
qui cap  rename W0549300 W707;
qui cap  rename W0549400 W708;
qui cap  rename W0549500 W709;
qui cap  rename W0549600 W710;
qui cap  rename W0549700 W711;
qui cap  rename W0549800 W712;
qui cap  rename W0549900 W713;
qui cap  rename W0550000 W714;
qui cap  rename W0550100 W715;
qui cap  rename W0550200 W716;
qui cap  rename W0550300 W717;
qui cap  rename W0550400 W718;
qui cap  rename W0550500 W719;
qui cap  rename W0550600 W720;
qui cap  rename W0550700 W721;
qui cap  rename W0550800 W722;
qui cap  rename W0550900 W723;
qui cap  rename W0551000 W724;
qui cap  rename W0551100 W725;
qui cap  rename W0551200 W726;
qui cap  rename W0551300 W727;
qui cap  rename W0551400 W728;
qui cap  rename W0551500 W729;
qui cap  rename W0551600 W730;
qui cap  rename W0551700 W731;
qui cap  rename W0586500 W732;
qui cap  rename W0586600 W733;
qui cap  rename W0586700 W734;
qui cap  rename W0586800 W735;
qui cap  rename W0586900 W736;
qui cap  rename W0587000 W737;
qui cap  rename W0587100 W738;
qui cap  rename W0587200 W739;
qui cap  rename W0587300 W740;
qui cap  rename W0587400 W741;
qui cap  rename W0587500 W742;
qui cap  rename W0587600 W743;
qui cap  rename W0587700 W744;
qui cap  rename W0587800 W745;
qui cap  rename W0587900 W746;
qui cap  rename W0588000 W747;
qui cap  rename W0588100 W748;
qui cap  rename W0588200 W749;
qui cap  rename W0588300 W750;
qui cap  rename W0588400 W751;
qui cap  rename W0588500 W752;
qui cap  rename W0588600 W753;
qui cap  rename W0588700 W754;
qui cap  rename W0588800 W755;
qui cap  rename W0588900 W756;
qui cap  rename W0589000 W757;
qui cap  rename W0589100 W758;
qui cap  rename W0589200 W759;
qui cap  rename W0589300 W760;
qui cap  rename W0589400 W761;
qui cap  rename W0589500 W762;
qui cap  rename W0589600 W763;
qui cap  rename W0589700 W764;
qui cap  rename W0589800 W765;
qui cap  rename W0589900 W766;
qui cap  rename W0590000 W767;
qui cap  rename W0590100 W768;
qui cap  rename W0590200 W769;
qui cap  rename W0590300 W770;
qui cap  rename W0590400 W771;
qui cap  rename W0590500 W772;
qui cap  rename W0590600 W773;
qui cap  rename W0590700 W774;
qui cap  rename W0590800 W775;
qui cap  rename W0590900 W776;
qui cap  rename W0591000 W777;
qui cap  rename W0591100 W778;
qui cap  rename W0591200 W779;
qui cap  rename W0591300 W780;
qui cap  rename W0591400 W781;
qui cap  rename W0591500 W782;
qui cap  rename W0591600 W783;
qui cap  rename W0626400 W784;
qui cap  rename W0626500 W785;
qui cap  rename W0626600 W786;
qui cap  rename W0626700 W787;
qui cap  rename W0626800 W788;
qui cap  rename W0626900 W789;
qui cap  rename W0627000 W790;
qui cap  rename W0627100 W791;
qui cap  rename W0627200 W792;
qui cap  rename W0627300 W793;
qui cap  rename W0627400 W794;
qui cap  rename W0627500 W795;
qui cap  rename W0627600 W796;
qui cap  rename W0627700 W797;
qui cap  rename W0627800 W798;
qui cap  rename W0627900 W799;
qui cap  rename W0628000 W800;
qui cap  rename W0628100 W801;
qui cap  rename W0628200 W802;
qui cap  rename W0628300 W803;
qui cap  rename W0628400 W804;
qui cap  rename W0628500 W805;
qui cap  rename W0628600 W806;
qui cap  rename W0628700 W807;
qui cap  rename W0628800 W808;
qui cap  rename W0628900 W809;
qui cap  rename W0629000 W810;
qui cap  rename W0629100 W811;
qui cap  rename W0629200 W812;
qui cap  rename W0629300 W813;
qui cap  rename W0629400 W814;
qui cap  rename W0629500 W815;
qui cap  rename W0629600 W816;
qui cap  rename W0629700 W817;
qui cap  rename W0629800 W818;
qui cap  rename W0629900 W819;
qui cap  rename W0630000 W820;
qui cap  rename W0630100 W821;
qui cap  rename W0630200 W822;
qui cap  rename W0630300 W823;
qui cap  rename W0630400 W824;
qui cap  rename W0630500 W825;
qui cap  rename W0630600 W826;
qui cap  rename W0630700 W827;
qui cap  rename W0630800 W828;
qui cap  rename W0630900 W829;
qui cap  rename W0631000 W830;
qui cap  rename W0631100 W831;
qui cap  rename W0631200 W832;
qui cap  rename W0631300 W833;
qui cap  rename W0631400 W834;
qui cap  rename W0631500 W835;
qui cap  rename W0666300 W836;
qui cap  rename W0666400 W837;
qui cap  rename W0666500 W838;
qui cap  rename W0666600 W839;
qui cap  rename W0666700 W840;
qui cap  rename W0666800 W841;
qui cap  rename W0666900 W842;
qui cap  rename W0667000 W843;
qui cap  rename W0667100 W844;
qui cap  rename W0667200 W845;
qui cap  rename W0667300 W846;
qui cap  rename W0667400 W847;
qui cap  rename W0667500 W848;
qui cap  rename W0667600 W849;
qui cap  rename W0667700 W850;
qui cap  rename W0667800 W851;
qui cap  rename W0667900 W852;
qui cap  rename W0668000 W853;
qui cap  rename W0668100 W854;
qui cap  rename W0668200 W855;
qui cap  rename W0668300 W856;
qui cap  rename W0668400 W857;
qui cap  rename W0668500 W858;
qui cap  rename W0668600 W859;
qui cap  rename W0668700 W860;
qui cap  rename W0668800 W861;
qui cap  rename W0668900 W862;
qui cap  rename W0669000 W863;
qui cap  rename W0669100 W864;
qui cap  rename W0669200 W865;
qui cap  rename W0669300 W866;
qui cap  rename W0669400 W867;
qui cap  rename W0669500 W868;
qui cap  rename W0669600 W869;
qui cap  rename W0669700 W870;
qui cap  rename W0669800 W871;
qui cap  rename W0669900 W872;
qui cap  rename W0670000 W873;
qui cap  rename W0670100 W874;
qui cap  rename W0670200 W875;
qui cap  rename W0670300 W876;
qui cap  rename W0670400 W877;
qui cap  rename W0670500 W878;
qui cap  rename W0670600 W879;
qui cap  rename W0670700 W880;
qui cap  rename W0670800 W881;
qui cap  rename W0670900 W882;
qui cap  rename W0671000 W883;
qui cap  rename W0671100 W884;
qui cap  rename W0671200 W885;
qui cap  rename W0671300 W886;
qui cap  rename W0671400 W887;
qui cap  rename W0732700 W888;
qui cap  rename W0732800 W889;
qui cap  rename W0732900 W890;
qui cap  rename W0733000 W891;
qui cap  rename W0733100 W892;
qui cap  rename W0733200 W893;
qui cap  rename W0733300 W894;
qui cap  rename W0733400 W895;
qui cap  rename W0733500 W896;
qui cap  rename W0733600 W897;
qui cap  rename W0733700 W898;
qui cap  rename W0733800 W899;
qui cap  rename W0733900 W900;
qui cap  rename W0734000 W901;
qui cap  rename W0734100 W902;
qui cap  rename W0734200 W903;
qui cap  rename W0734300 W904;
qui cap  rename W0734400 W905;
qui cap  rename W0734500 W906;
qui cap  rename W0734600 W907;
qui cap  rename W0734700 W908;
qui cap  rename W0734800 W909;
qui cap  rename W0734900 W910;
qui cap  rename W0735000 W911;
qui cap  rename W0735100 W912;
qui cap  rename W0735200 W913;
qui cap  rename W0735300 W914;
qui cap  rename W0735400 W915;
qui cap  rename W0735500 W916;
qui cap  rename W0735600 W917;
qui cap  rename W0735700 W918;
qui cap  rename W0735800 W919;
qui cap  rename W0735900 W920;
qui cap  rename W0736000 W921;
qui cap  rename W0736100 W922;
qui cap  rename W0736200 W923;
qui cap  rename W0736300 W924;
qui cap  rename W0736400 W925;
qui cap  rename W0736500 W926;
qui cap  rename W0736600 W927;
qui cap  rename W0736700 W928;
qui cap  rename W0736800 W929;
qui cap  rename W0736900 W930;
qui cap  rename W0737000 W931;
qui cap  rename W0737100 W932;
qui cap  rename W0737200 W933;
qui cap  rename W0737300 W934;
qui cap  rename W0737400 W935;
qui cap  rename W0737500 W936;
qui cap  rename W0737600 W937;
qui cap  rename W0737700 W938;
qui cap  rename W0737800 W939;
qui cap  rename W0737900 W940;
qui cap  rename W0738000 W941;
qui cap  rename W0738100 W942;
qui cap  rename W0738200 W943;
qui cap  rename W0738300 W944;
qui cap  rename W0738400 W945;
qui cap  rename W0738500 W946;
qui cap  rename W0738600 W947;
qui cap  rename W0738700 W948;
qui cap  rename W0738800 W949;
qui cap  rename W0738900 W950;
qui cap  rename W0739000 W951;
qui cap  rename W0739100 W952;
qui cap  rename W0739200 W953;
qui cap  rename W0739300 W954;
qui cap  rename W0739400 W955;
qui cap  rename W0739500 W956;
qui cap  rename W0739600 W957;
qui cap  rename W0739700 W958;
qui cap  rename W0739800 W959;
qui cap  rename W0739900 W960;
qui cap  rename W0740000 W961;
qui cap  rename W0740100 W962;
qui cap  rename W0740200 W963;
qui cap  rename W0740300 W964;
qui cap  rename W0740400 W965;
qui cap  rename W0740500 W966;
qui cap  rename W0740600 W967;
qui cap  rename W0740700 W968;
qui cap  rename W0740800 W969;
qui cap  rename W0740900 W970;
qui cap  rename W0741000 W971;
qui cap  rename W0741100 W972;
qui cap  rename W0741200 W973;
qui cap  rename W0741300 W974;
qui cap  rename W0741400 W975;
qui cap  rename W0741500 W976;
qui cap  rename W0741600 W977;
qui cap  rename W0741700 W978;
qui cap  rename W0741800 W979;
qui cap  rename W0741900 W980;
qui cap  rename W0742000 W981;
qui cap  rename W0742100 W982;
qui cap  rename W0742200 W983;
qui cap  rename W0742300 W984;
qui cap  rename W0742400 W985;
qui cap  rename W0742500 W986;
qui cap  rename W0742600 W987;
qui cap  rename W0742700 W988;
qui cap  rename W0742800 W989;
qui cap  rename W0742900 W990;
qui cap  rename W0743000 W991;
qui cap  rename W0743100 W992;
qui cap  rename W0803900 W993;
qui cap  rename W0804000 W994;
qui cap  rename W0804100 W995;
qui cap  rename W0804200 W996;
qui cap  rename W0804300 W997;
qui cap  rename W0804400 W998;
qui cap  rename W0804500 W999;
qui cap  rename W0804600 W1000;
qui cap  rename W0804700 W1001;
qui cap  rename W0804800 W1002;
qui cap  rename W0804900 W1003;
qui cap  rename W0805000 W1004;
qui cap  rename W0805100 W1005;
qui cap  rename W0805200 W1006;
qui cap  rename W0805300 W1007;
qui cap  rename W0805400 W1008;
qui cap  rename W0805500 W1009;
qui cap  rename W0805600 W1010;
qui cap  rename W0805700 W1011;
qui cap  rename W0805800 W1012;
qui cap  rename W0805900 W1013;
qui cap  rename W0806000 W1014;
qui cap  rename W0806100 W1015;
qui cap  rename W0806200 W1016;
qui cap  rename W0806300 W1017;
qui cap  rename W0806400 W1018;
qui cap  rename W0806500 W1019;
qui cap  rename W0806600 W1020;
qui cap  rename W0806700 W1021;
qui cap  rename W0806800 W1022;
qui cap  rename W0806900 W1023;
qui cap  rename W0807000 W1024;
qui cap  rename W0807100 W1025;
qui cap  rename W0807200 W1026;
qui cap  rename W0807300 W1027;
qui cap  rename W0807400 W1028;
qui cap  rename W0807500 W1029;
qui cap  rename W0807600 W1030;
qui cap  rename W0807700 W1031;
qui cap  rename W0807800 W1032;
qui cap  rename W0807900 W1033;
qui cap  rename W0808000 W1034;
qui cap  rename W0808100 W1035;
qui cap  rename W0808200 W1036;
qui cap  rename W0808300 W1037;
qui cap  rename W0808400 W1038;
qui cap  rename W0808500 W1039;
qui cap  rename W0808600 W1040;
qui cap  rename W0808700 W1041;
qui cap  rename W0808800 W1042;
qui cap  rename W0808900 W1043;
qui cap  rename W0809000 W1044;
qui cap  rename W0809100 W1045;
qui cap  rename W0809200 W1046;
qui cap  rename W0809300 W1047;
qui cap  rename W0809400 W1048;
qui cap  rename W0809500 W1049;
qui cap  rename W0809600 W1050;
qui cap  rename W0809700 W1051;
qui cap  rename W0809800 W1052;
qui cap  rename W0809900 W1053;
qui cap  rename W0810000 W1054;
qui cap  rename W0810100 W1055;
qui cap  rename W0810200 W1056;
qui cap  rename W0810300 W1057;
qui cap  rename W0810400 W1058;
qui cap  rename W0810500 W1059;
qui cap  rename W0810600 W1060;
qui cap  rename W0810700 W1061;
qui cap  rename W0810800 W1062;
qui cap  rename W0810900 W1063;
qui cap  rename W0811000 W1064;
qui cap  rename W0811100 W1065;
qui cap  rename W0811200 W1066;
qui cap  rename W0811300 W1067;
qui cap  rename W0811400 W1068;
qui cap  rename W0811500 W1069;
qui cap  rename W0811600 W1070;
qui cap  rename W0811700 W1071;
qui cap  rename W0811800 W1072;
qui cap  rename W0811900 W1073;
qui cap  rename W0812000 W1074;
qui cap  rename W0812100 W1075;
qui cap  rename W0812200 W1076;
qui cap  rename W0812300 W1077;
qui cap  rename W0812400 W1078;
qui cap  rename W0812500 W1079;
qui cap  rename W0812600 W1080;
qui cap  rename W0812700 W1081;
qui cap  rename W0812800 W1082;
qui cap  rename W0812900 W1083;
qui cap  rename W0813000 W1084;
qui cap  rename W0813100 W1085;
qui cap  rename W0813200 W1086;
qui cap  rename W0813300 W1087;
qui cap  rename W0813400 W1088;
qui cap  rename W0813500 W1089;
qui cap  rename W0813600 W1090;
qui cap  rename W0813700 W1091;
qui cap  rename W0813800 W1092;
qui cap  rename W0813900 W1093;
qui cap  rename W0814000 W1094;
qui cap  rename W0814100 W1095;
qui cap  rename W0814200 W1096;
qui cap  rename W0875000 W1097;
qui cap  rename W0875100 W1098;
qui cap  rename W0875200 W1099;
qui cap  rename W0875300 W1100;
qui cap  rename W0875400 W1101;
qui cap  rename W0875500 W1102;
qui cap  rename W0875600 W1103;
qui cap  rename W0875700 W1104;
qui cap  rename W0875800 W1105;
qui cap  rename W0875900 W1106;
qui cap  rename W0876000 W1107;
qui cap  rename W0876100 W1108;
qui cap  rename W0876200 W1109;
qui cap  rename W0876300 W1110;
qui cap  rename W0876400 W1111;
qui cap  rename W0876500 W1112;
qui cap  rename W0876600 W1113;
qui cap  rename W0876700 W1114;
qui cap  rename W0876800 W1115;
qui cap  rename W0876900 W1116;
qui cap  rename W0877000 W1117;
qui cap  rename W0877100 W1118;
qui cap  rename W0877200 W1119;
qui cap  rename W0877300 W1120;
qui cap  rename W0877400 W1121;
qui cap  rename W0877500 W1122;
qui cap  rename W0877600 W1123;
qui cap  rename W0877700 W1124;
qui cap  rename W0877800 W1125;
qui cap  rename W0877900 W1126;
qui cap  rename W0878000 W1127;
qui cap  rename W0878100 W1128;
qui cap  rename W0878200 W1129;
qui cap  rename W0878300 W1130;
qui cap  rename W0878400 W1131;
qui cap  rename W0878500 W1132;
qui cap  rename W0878600 W1133;
qui cap  rename W0878700 W1134;
qui cap  rename W0878800 W1135;
qui cap  rename W0878900 W1136;
qui cap  rename W0879000 W1137;
qui cap  rename W0879100 W1138;
qui cap  rename W0879200 W1139;
qui cap  rename W0879300 W1140;
qui cap  rename W0879400 W1141;
qui cap  rename W0879500 W1142;
qui cap  rename W0879600 W1143;
qui cap  rename W0879700 W1144;
qui cap  rename W0879800 W1145;
qui cap  rename W0879900 W1146;
qui cap  rename W0880000 W1147;
qui cap  rename W0880100 W1148;
qui cap  rename W0880200 W1149;
qui cap  rename W0880300 W1150;
qui cap  rename W0880400 W1151;
qui cap  rename W0880500 W1152;
qui cap  rename W0880600 W1153;
qui cap  rename W0880700 W1154;
qui cap  rename W0880800 W1155;
qui cap  rename W0880900 W1156;
qui cap  rename W0881000 W1157;
qui cap  rename W0881100 W1158;
qui cap  rename W0881200 W1159;
qui cap  rename W0881300 W1160;
qui cap  rename W0881400 W1161;
qui cap  rename W0881500 W1162;
qui cap  rename W0881600 W1163;
qui cap  rename W0881700 W1164;
qui cap  rename W0881800 W1165;
qui cap  rename W0881900 W1166;
qui cap  rename W0882000 W1167;
qui cap  rename W0882100 W1168;
qui cap  rename W0882200 W1169;
qui cap  rename W0882300 W1170;
qui cap  rename W0882400 W1171;
qui cap  rename W0882500 W1172;
qui cap  rename W0882600 W1173;
qui cap  rename W0882700 W1174;
qui cap  rename W0882800 W1175;
qui cap  rename W0882900 W1176;
qui cap  rename W0883000 W1177;
qui cap  rename W0883100 W1178;
qui cap  rename W0883200 W1179;
qui cap  rename W0883300 W1180;
qui cap  rename W0883400 W1181;
qui cap  rename W0883500 W1182;
qui cap  rename W0883600 W1183;
qui cap  rename W0883700 W1184;
qui cap  rename W0883800 W1185;
qui cap  rename W0883900 W1186;
qui cap  rename W0884000 W1187;
qui cap  rename W0884100 W1188;
qui cap  rename W0884200 W1189;
qui cap  rename W0884300 W1190;
qui cap  rename W0884400 W1191;
qui cap  rename W0884500 W1192;
qui cap  rename W0884600 W1193;
qui cap  rename W0884700 W1194;
qui cap  rename W0884800 W1195;
qui cap  rename W0884900 W1196;
qui cap  rename W0885000 W1197;
qui cap  rename W0885100 W1198;
qui cap  rename W0885200 W1199;
qui cap  rename W0885300 W1200;
qui cap  rename W0932100 W1201;



qui reshape i ID;
qui reshape xij W;
qui reshape xi SAMP;
qui reshape j week;
qui reshape long;


qui rename W lstatus;


gen y = 1977 if week == 0;
replace y = 1978 if week >0 & week<54;
replace y = 1979 if week >=54 & week<106;
replace y = 1980 if week >=106 & week<158;
replace y = 1981 if week >=158 & week<210;
replace y = 1982 if week >=210 & week<262;
replace y = 1983 if week >=262 & week<314;
replace y = 1984 if week >=314 & week<367;
replace y = 1985 if week >=367 & week<419;
replace y = 1986 if week >=419 & week<471;
replace y = 1987 if week >=471 & week<523;
replace y = 1988 if week >=523 & week<575;
replace y = 1989 if week >=575 & week<628;
replace y = 1990 if week >=628 & week<680;
replace y = 1991 if week >=680 & week<732;
replace y = 1992 if week >=732 & week<784;
replace y = 1993 if week >=784 & week<836;
replace y = 1994 if week >=836 & week<888;
replace y = 1995 if week >=888 & week<941;
replace y = 1996 if week >=941 & week<993;
replace y = 1997 if week >=993 & week<1045;
replace y = 1998 if week >=1045 & week<1097;
replace y = 1999 if week >=1097 & week<1149;
replace y = 2000 if week >=1149 & week<=1201;

qui sort lstatus;

drop if y < 1978;


