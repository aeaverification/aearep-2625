
clear all

use "$data/_individualfile.dta"
keep ID 

gen cpi1978 = 65.2 
gen cpi1979= 72.6 
gen cpi1980= 82.4 
gen cpi1981= 90.9 
gen cpi1982= 96.5 
gen cpi1983= 99.6 
gen cpi1984= 103.9 
gen cpi1985= 107.6 
gen cpi1986= 109.6 
gen cpi1987= 113.6 
gen cpi1988= 118.3 
gen cpi1989= 124.0 
gen cpi1990= 130.7 
gen cpi1991= 136.2 
gen cpi1992= 140.3 
gen cpi1993= 144.5 
gen cpi1994= 148.2 
gen cpi1995=152.4
gen cpi1996= 156.9 
gen cpi1997=160.5
gen cpi1998= 163.0
gen cpi1999=166.6
gen cpi2000= 172.2
gen cpi2002=179.9
gen cpi2004=188.9
gen cpi2006=201.6
gen cpi2008=215.3
gen cpi2010=218.1
gen cpi2012=229.6
gen cpi2014=236.7
gen cpi2016=240

forval i=1978/2000{
gen conv`i'=cpi2000/cpi`i'
}

forval i=2002(2)2016{
gen conv`i'=cpi2000/cpi`i'
}

 reshape i ID
 reshape j year 
 reshape xij cpi conv
 reshape long
 
 duplicates drop year, force
 drop ID cpi
 sort year
 save "$data/Macro indicators/cpi", replace
