
*note: this file constructs the file "data_analysis"

clear all

*************************************************************************************************************

*I. selection criteria as in footnote 2 of the manuscript

use "$data/data_month", clear

*1. more than two years in the military
gen aux=1 if JOB==7
bysort ID: egen years_mil=sum(aux)
gen militar=1 if years_mil>24
replace militar=0 if militar==.
drop if militar==1
drop aux

*2. weak labor attachment
gen aux=1 if JOB==5
bysort ID: egen years_out=sum(aux)
gen weak_labor_attach=1 if years_out>120
replace weak_labor_attach=0 if weak_labor_attach==.
drop if weak_labor_attach==1
drop aux

*3.already working in the beginning of the sample.
bysort ID year: egen HOURSY=sum(HOURSM)
gen aux=1 if HOURSY>1200 & year==1979
bysort ID: egen notransition=mean(aux)
drop if notransition==1
drop aux

*4. no ability scores.
drop if test_info==1

duplicates drop ID, force
keep ID SAMP
sort ID

save  "$data/sample_selection", replace

*************************************************************************************************************
*II. skill requirements

*1. occ weights

use "$data/data_month", clear
sort ID 
merge ID using "$data/sample_selection"
keep if _m==3
drop _m

*generate weights for occupations
*#ofjobs in each occupation
rename OCC occ1990dd
gen aux_occ=1 if occ1990dd>0 & occ1990dd~=.
bysort occ1990dd: egen total_occ1990dd=sum(aux_occ)
rename total_occ1990dd occ_weight
duplicates drop occ1990dd, force
keep occ1990dd occ_weight
sort occ1990dd
save "$data/occ_weights", replace

*************************************************************************************************************

*2. skill requirements

*2.1 math, verbal & technical

use "$data/ONET/onet_categories", replace

*keep only descriptors related with ASVAB scores
drop if Element_Name=="Coordination" | Element_Name=="Instructing" | Element_Name=="Negotiation" | Element_Name=="Service Orientation" | Element_Name=="Social Perceptiveness" | Element_Name=="Persuasion" 

*relatdness scores to ASVAB test categiries

gen AR=.
gen MK=.
gen WK=.
gen PC=.
gen GS=.
gen MC=.
gen EI=.

replace AR=3.00 if Element_Name=="Oral Comprehension"
replace MK=3.11 if Element_Name=="Oral Comprehension"
replace WK=4.22 if Element_Name=="Oral Comprehension"
replace PC=4.44 if Element_Name=="Oral Comprehension"
replace GS=3.50 if Element_Name=="Oral Comprehension"
replace MC=2.57 if Element_Name=="Oral Comprehension"
replace EI=2.50 if Element_Name=="Oral Comprehension"

replace AR=4.11 if Element_Name=="Written Comprehension"
replace MK=3.44 if Element_Name=="Written Comprehension"
replace WK=6.00 if Element_Name=="Written Comprehension"
replace PC=5.89 if Element_Name=="Written Comprehension"
replace GS=4.21 if Element_Name=="Written Comprehension"
replace MC=3.07 if Element_Name=="Written Comprehension"
replace EI=3.29 if Element_Name=="Written Comprehension"

replace AR=4.56 if Element_Name=="Deductive Reasoning"
replace MK=4.22 if Element_Name=="Deductive Reasoning"
replace WK=2.67 if Element_Name=="Deductive Reasoning"
replace PC=3.44 if Element_Name=="Deductive Reasoning"
replace GS=5.07 if Element_Name=="Deductive Reasoning"
replace MC=4.21 if Element_Name=="Deductive Reasoning"
replace EI=3.93 if Element_Name=="Deductive Reasoning"

replace AR=3.67 if Element_Name=="Inductive Reasoning"
replace MK=4.33 if Element_Name=="Inductive Reasoning"
replace WK=3.11 if Element_Name=="Inductive Reasoning"
replace PC=4.89 if Element_Name=="Inductive Reasoning"
replace GS=4.57 if Element_Name=="Inductive Reasoning"
replace MC=3.71 if Element_Name=="Inductive Reasoning"
replace EI=3.14 if Element_Name=="Inductive Reasoning"

replace AR=4.22 if Element_Name=="Information Ordering"
replace MK=4.56 if Element_Name=="Information Ordering"
replace WK=3.56 if Element_Name=="Information Ordering"
replace PC=3.78 if Element_Name=="Information Ordering"
replace GS=3.50 if Element_Name=="Information Ordering"
replace MC=2.86 if Element_Name=="Information Ordering"
replace EI=3.14 if Element_Name=="Information Ordering"

replace AR=5.78 if Element_Name=="Mathematical Reasoning"
replace MK=5.89 if Element_Name=="Mathematical Reasoning"
replace WK=1.89 if Element_Name=="Mathematical Reasoning"
replace PC=2.22 if Element_Name=="Mathematical Reasoning"
replace GS=3.71 if Element_Name=="Mathematical Reasoning"
replace MC=3.64 if Element_Name=="Mathematical Reasoning"
replace EI=2.79 if Element_Name=="Mathematical Reasoning"

replace AR=5.67 if Element_Name=="Number Facility"
replace MK=5.56 if Element_Name=="Number Facility"
replace WK=1.67 if Element_Name=="Number Facility"
replace PC=2.22 if Element_Name=="Number Facility"
replace GS=3.93 if Element_Name=="Number Facility"
replace MC=3.43 if Element_Name=="Number Facility"
replace EI=2.86 if Element_Name=="Number Facility"

replace AR=1.11 if Element_Name=="Biology"
replace MK=1.13 if Element_Name=="Biology"
replace WK=1.33 if Element_Name=="Biology"
replace PC=1.22 if Element_Name=="Biology"
replace GS=5.71 if Element_Name=="Biology"
replace MC=1.07 if Element_Name=="Biology"
replace EI=1.00 if Element_Name=="Biology"

replace AR=5.89 if Element_Name=="Mathematics K" 
replace MK=6.00 if Element_Name=="Mathematics K"
replace WK=1.67 if Element_Name=="Mathematics K"
replace PC=1.67 if Element_Name=="Mathematics K"
replace GS=3.71 if Element_Name=="Mathematics K"
replace MC=3.36 if Element_Name=="Mathematics K"
replace EI=2.93 if Element_Name=="Mathematics K"

replace AR=1.78 if Element_Name=="Computers and Electronics"
replace MK=2.00 if Element_Name=="Computers and Electronics"
replace WK=1.56 if Element_Name=="Computers and Electronics"
replace PC=1.56 if Element_Name=="Computers and Electronics"
replace GS=1.79 if Element_Name=="Computers and Electronics"
replace MC=2.57 if Element_Name=="Computers and Electronics"
replace EI=5.29 if Element_Name=="Computers and Electronics"

replace AR=1.67 if Element_Name=="Engineering and Technology"
replace MK=2.11 if Element_Name=="Engineering and Technology"
replace WK=1.22 if Element_Name=="Engineering and Technology"
replace PC=1.56 if Element_Name=="Engineering and Technology"
replace GS=3.08 if Element_Name=="Engineering and Technology"
replace MC=5.07 if Element_Name=="Engineering and Technology"
replace EI=3.79 if Element_Name=="Engineering and Technology"

replace AR=1.22 if Element_Name=="Building and Construction"
replace MK=1.22 if Element_Name=="Building and Construction"
replace WK=1.11 if Element_Name=="Building and Construction"
replace PC=1.22 if Element_Name=="Building and Construction"
replace GS=1.29 if Element_Name=="Building and Construction"
replace MC=4.07 if Element_Name=="Building and Construction"
replace EI=1.71 if Element_Name=="Building and Construction"

replace AR=1.11 if Element_Name=="Mechanical"
replace MK=1.67 if Element_Name=="Mechanical"
replace WK=1.11 if Element_Name=="Mechanical"
replace PC=1.11 if Element_Name=="Mechanical"
replace GS=2.43 if Element_Name=="Mechanical"
replace MC=5.57 if Element_Name=="Mechanical"
replace EI=2.50 if Element_Name=="Mechanical"

replace AR=2.00 if Element_Name=="Physics"
replace MK=2.11 if Element_Name=="Physics"
replace WK=1.33 if Element_Name=="Physics"
replace PC=1.22 if Element_Name=="Physics"
replace GS=5.86 if Element_Name=="Physics"
replace MC=4.79 if Element_Name=="Physics"
replace EI=3.43 if Element_Name=="Physics"

replace AR=2.22 if Element_Name=="English Language"
replace MK=2.89 if Element_Name=="English Language"
replace WK=5.67 if Element_Name=="English Language"
replace PC=5.78 if Element_Name=="English Language"
replace GS=1.79 if Element_Name=="English Language"
replace MC=1.57 if Element_Name=="English Language"
replace EI=1.43 if Element_Name=="English Language"

replace AR=2.44 if Element_Name=="Reading Comprehension"
replace MK=2.56 if Element_Name=="Reading Comprehension"
replace WK=5.78 if Element_Name=="Reading Comprehension"
replace PC=5.67 if Element_Name=="Reading Comprehension"
replace GS=3.07 if Element_Name=="Reading Comprehension"
replace MC=2.85 if Element_Name=="Reading Comprehension"
replace EI=2.71 if Element_Name=="Reading Comprehension"

replace AR=6.00 if Element_Name=="Mathematics" 
replace MK=6.00 if Element_Name=="Mathematics" 
replace WK=2.00 if Element_Name=="Mathematics" 
replace PC=1.78 if Element_Name=="Mathematics" 
replace GS=3.86 if Element_Name=="Mathematics" 
replace MC=3.57 if Element_Name=="Mathematics" 
replace EI=3.29 if Element_Name=="Mathematics" 

replace AR=3.78 if Element_Name=="Science"
replace MK=4.22 if Element_Name=="Science"
replace WK=1.78 if Element_Name=="Science"
replace PC=2.67 if Element_Name=="Science"
replace GS=5.71 if Element_Name=="Science"
replace MC=4.07 if Element_Name=="Science"
replace EI=3.29 if Element_Name=="Science"

replace AR=1.44 if Element_Name=="Technology Design"
replace MK=1.67 if Element_Name=="Technology Design"
replace WK=1.11 if Element_Name=="Technology Design"
replace PC=1.11 if Element_Name=="Technology Design"
replace GS=2.29 if Element_Name=="Technology Design"
replace MC=4.71 if Element_Name=="Technology Design"
replace EI=3.93 if Element_Name=="Technology Design"

replace AR=1.56 if Element_Name=="Equipment Selection"
replace MK=1.22 if Element_Name=="Equipment Selection"
replace WK=1.33 if Element_Name=="Equipment Selection"
replace PC=1.22 if Element_Name=="Equipment Selection"
replace GS=2.14 if Element_Name=="Equipment Selection"
replace MC=4.50 if Element_Name=="Equipment Selection"
replace EI=3.36 if Element_Name=="Equipment Selection"

replace AR=1.22 if Element_Name=="Installation"
replace MK=1.11 if Element_Name=="Installation"
replace WK=1.44 if Element_Name=="Installation"
replace PC=1.22 if Element_Name=="Installation"
replace GS=1.64 if Element_Name=="Installation"
replace MC=4.71 if Element_Name=="Installation"
replace EI=4.29 if Element_Name=="Installation"

replace AR=1.11 if Element_Name=="Operation and Control"
replace MK=1.11 if Element_Name=="Operation and Control"
replace WK=1.11 if Element_Name=="Operation and Control"
replace PC=1.11 if Element_Name=="Operation and Control"
replace GS=3.93 if Element_Name=="Operation and Control"
replace MC=4.29 if Element_Name=="Operation and Control"
replace EI=3.71 if Element_Name=="Operation and Control"

replace AR=1.00 if Element_Name=="Equipment Maintenance"
replace MK=1.33 if Element_Name=="Equipment Maintenance"
replace WK=1.11 if Element_Name=="Equipment Maintenance"
replace PC=1.22 if Element_Name=="Equipment Maintenance"
replace GS=1.43 if Element_Name=="Equipment Maintenance"
replace MC=3.43 if Element_Name=="Equipment Maintenance"
replace EI=3.00 if Element_Name=="Equipment Maintenance"

replace AR=1.56 if Element_Name=="Troubleshooting"
replace MK=1.44 if Element_Name=="Troubleshooting"
replace WK=1.56 if Element_Name=="Troubleshooting"
replace PC=1.44 if Element_Name=="Troubleshooting"
replace GS=2.50 if Element_Name=="Troubleshooting"
replace MC=4.36 if Element_Name=="Troubleshooting"
replace EI=3.93 if Element_Name=="Troubleshooting"

replace AR=1.11 if Element_Name=="Repairing"
replace MK=1.11 if Element_Name=="Repairing"
replace WK=1.11 if Element_Name=="Repairing"
replace PC=1.11 if Element_Name=="Repairing"
replace GS=1.14 if Element_Name=="Repairing"
replace MC=3.57 if Element_Name=="Repairing"
replace EI=3.00 if Element_Name=="Repairing"

replace AR=1.22 if Element_Name=="Chemistry" 
replace MK=1.33 if Element_Name=="Chemistry" 
replace WK=1.11 if Element_Name=="Chemistry" 
replace PC=1.11 if Element_Name=="Chemistry" 
replace GS=5.29 if Element_Name=="Chemistry" 
replace MC=1.43 if Element_Name=="Chemistry" 
replace EI=1.29 if Element_Name=="Chemistry" 

*data to be used by do-files in appendix
rename soc2000 soccode
save "$data/ONET/onet_categories_asvab", replace

sort soccode
merge soccode using "$data/Crosswalks/occ2000_soc"
drop if _m==2
drop _m
quietly do "$do/main/other/occ_input"
replace occ1990dd=78 if soc2010=="19-1020"
bysort occ1990dd Element_ID: egen score=mean(Data_Value)
duplicates drop occ1990dd Element_ID, force

sort occ1990dd
merge occ1990dd using "$data/occ_weights"
drop if _merge==2
replace occ_weight=0 if _merge==1
drop _merge

foreach v in AR MK WK PC GS MC EI {
gen aux_`v'=`v'*score
bysort occ1990dd: egen total_`v'=sum(aux_`v')
bysort occ1990dd: egen all_`v'=sum(`v')
gen `v'_score=total_`v'/all_`v'
}

keep occ1990dd censtitle *_score occ_weight*
duplicates drop occ1990dd, force

foreach var in AR MK WK PC GS MC EI{
gen `var'_zscore=.
sum `var'_score
replace `var'_zscore=(`var'_score-r(mean))/r(sd) 
}

keep occ1990dd censtitle *_zscore occ_weight*

pca AR_zscore MK_zscore
predict math

pca WK_zscore PC_zscore
predict verbal

pca GS_zscore MC_zscore EI_zscore
predict technical

foreach var in math verbal technical {
xtile `var'_perc_occ_weighted=`var' [fweight=occ_weight], nq(100)  
xtile `var'_perc_occ=`var', nq(100)  
}	
keep occ1990dd censtitle math* verbal* technical*
drop if occ1990dd==.
sort occ1990dd 
save "$data/occ_perc_weighted", replace

*3.2 social

use "$data/ONET/onet_categories", replace

*keep social descriptors
keep if Element_Name=="Coordination" | Element_Name=="Instructing" | Element_Name=="Negotiation" | Element_Name=="Service Orientation" | Element_Name=="Social Perceptiveness" | Element_Name=="Persuasion" 
**data to be used by do-files in appendix folder
rename soc2000 soccode
save "$data/ONET/onet_categories_social", replace

sort soccode
merge soccode using "$data/Crosswalks/occ2000_soc"
drop if _m==2
drop _m
quietly do "$do/main/other/occ_input"
replace occ1990dd=78 if soc2010=="19-1020"
bysort occ1990dd Element_ID: egen score=mean(Data_Value)
replace Element_Name="ServiceOrientation" if Element_Name=="Service Orientation"
replace Element_Name="SocialPerceptiveness" if Element_Name=="Social Perceptiveness"

*add weights
sort occ1990dd
merge occ1990dd using "$data/occ_weights"
drop if _merge==2
replace occ_weight=0 if _merge==1
drop _merge 
duplicates drop occ1990dd Element_ID, force

foreach var in Coordination Instructing Negotiation ServiceOrientation SocialPerceptiveness Persuasion {
gen `var'_aux=.
sum score  if Element_Name=="`var'"
replace `var'_aux=(score-r(mean))/r(sd) if Element_Name=="`var'"
bysort occ1990dd: egen `var'_zscore=mean(`var'_aux)
drop *_aux
}

drop if occ1990dd==.
duplicates drop occ1990dd, force
keep occ1990dd *_zscore occ_weight* censtitle

pca Coordination_zscore Instructing_zscore Negotiation_zscore ServiceOrientation_zscore SocialPerceptiveness_zscore Persuasion_zscore
predict social

xtile social_perc_occ_weighted=social [fweight=occ_weight], nq(100) 
xtile social_perc_occ=social, nq(100) 
				
drop if occ1990dd==.
sort occ1990dd 
merge occ1990dd using "$data/occ_perc_weighted"
drop _m
sort occ1990dd 

keep censtitle occ1990dd *_perc_occ *_perc_occ_weighted
save "$data/occ_perc_weighted", replace

************************************************************************************************************************* 

*III. build data_analysis

use "$data/data_month", clear

sort ID year
merge ID using "$data/sample_selection"
keep if _m==3
drop _m

*add occ percentiles
rename OCC occ1990dd
sort occ1990dd 
merge occ1990dd using "$data/occ_perc_weighted"
drop if _m==2
drop _m


*************************************************************************************************************************

*2. job transitions & controls

*************************************************************************************************************************

*2.1. job transitions

*seprations: emp to unemp flows
sort ID year month
gen emp_unemp=1 if (JOB==4 & JN[_n-1]>0 & JN[_n-1]~=. & ID==ID[_n-1]) |  (JOB==5 & JN[_n-1]~=. & JN[_n-1]>0 & ID==ID[_n-1]) |  (JOB==2 & JN[_n-1]~=. & JN[_n-1]>0 & ID==ID[_n-1]) 

*auxiliar variable: to identify eue transitions and previous employment data.
gen aux_emp_unemp=emp_unemp
replace aux=aux_emp_unemp[_n-1] if (JN==. & JN[_n-1]==. & ID==ID[_n-1])

*emp to emp flows
sort ID year month
gen emp_emp=1 if JN~=JN[_n-1] & JN[_n-1]>0 & JN[_n]>0 & JN[_n-1]~=. & JN~=. & ID==ID[_n-1]

*non-employment to emp flows
sort ID year month
gen unemp_to_emp=1 if (JOB[_n-1]==4 & JN>0 & JN~=. & ID==ID[_n-1]) |  (JOB[_n-1]==5 & JN~=. & JN>0 & ID==ID[_n-1]) |  (JOB[_n-1]==2 & JN~=. & JN>0 & ID==ID[_n-1])
gen eue=unemp_to_emp if aux_emp_unemp[_n-1]==1  

*all transitions
gen all_switch=emp_emp
replace all_switch=unemp_to_emp if unemp_to_emp==1 & emp_emp==.

*new hire dummies
*new hire: regardless of type 
gen dummy=0 if all_switch==. & JN~=.
replace dummy=1 if all_switch==1

*ee transition
gen dummy1=0 if (emp_emp==. & all_switch==1) | (all_switch==.  & JN~=-.)
replace dummy1=1 if emp_emp==1 

*ue transition
gen dummy2=0 if (emp_emp==1) | (all_switch==. & JN~=.)
replace dummy2=1 if all_switch==1 & emp_emp==.

*previous unemployment
sort ID  year month
gen aux_JN=JN
replace aux_JN=aux_JN[_n-1]  if JN==. & ID==ID[_n-1]
gen JN_prev=aux_JN[_n-1] if dummy==1 & ID==ID[_n-1]

*unemployment duration
sort ID year month
gen unempl_duration=1 if JN==. & ID==ID[_n-1]
replace unempl_duration=unempl_duration[_n-1]+1 if JN==.  & ID==ID[_n-1] & JN[_n-1]==.

**recoding*
**stayers + ee transitions
	sort ID year month
	gen dummy_new=dummy
	replace dummy_new=. if year==1979 & month==1
	replace dummy_new=. if dummy_new[_n-1]==. & dummy==0 & dummy[_n-1]==0
	gen dummy1_new=dummy1 if dummy_new~=.
	foreach var in dummy dummy1 {
	rename `var' `var'_old
	rename `var'_new `var'
	}


*breaks of three months as ee
sort ID year month
gen dummy1_alt3=dummy1
replace dummy1_alt3=1 if unempl_duration[_n-1]<=3 & dummy1==0 & ID==ID[_n-1] 
replace dummy1_alt3=0 if JN==JN_prev

gen dummy2_alt3=dummy2
replace dummy2_alt3=0 if unempl_duration[_n-1]<=3 & dummy2==1 & ID==ID[_n-1] 

*recode recalls as non-new-hires
sort ID year month
gen recall=1 if JN==JN_prev & JN~=. & JN_prev~=. & dummy==1

sort ID year month
gen dummy1_alt4=dummy1 
replace dummy1_alt4=0 if recall==1 & unempl_duration[_n-1]<=3

gen dummy2_alt4=dummy2 
replace dummy2_alt4=0 if recall==1  & unempl_duration[_n-1]<=3

*separation dummy
sort ID year month
ge separation=1 if  emp_unemp[_n+1]==1 & ID==ID[_n+1]
replace separation=0 if  emp_unemp[_n+1]==. & ID==ID[_n+1] & JN~=. & JN[_n+1]~=. & dummy[_n+1]==0
replace separation=1 if  emp_unemp[_n+1]==. & ID==ID[_n+1] & JN~=. & JN[_n+1]~=. & dummy1[_n+1]==1
replace separation=0 if  JN~=. & year==2012 & month==12
replace separation=0 if  emp_unemp[_n+1]==.  & JN~=. & JN[_n+1]==. & ID==ID[_n+1] 

bysort ID JN: egen ave_hours=mean(HOURSM)


*************************************************************************************************************************

*2.2. wage: trimming as in Guvenen at al (2018)

*wage: trimming as in Guvenen at al (2018)

winsor lhrp, gen(lhrp_w) p(0.001)
gen lhrp2=lhrp if lhrp==lhrp_w


*************************************************************************************************************************

*2.3. controls

**a. industry fe

gen INDN_aux=INDN
tostring INDN, replace
**transform 4-digit industry codes into 3-digit
gen aux=length(INDN)
gen aux2=substr(INDN,aux,aux) if year>=2002 & INDN~="-4" & INDN~="-5" & INDN~="-3" & INDN~="-2" & INDN~="-1" & INDN~="0"
gen aux4=substr(INDN,1,aux-1) if aux==4
replace INDN=aux4 if aux==4 
destring INDN, replace
drop aux aux2 aux4


**from 3-digit industry codes to one-digit classification

**1970's
gen industry=1 if INDN>=017 & INDN<=029 & year<=2001
replace industry=2 if INDN>=047 & INDN<=058 & year<=2001
replace industry=3 if INDN>=067 & INDN<=078 & year<=2001
replace industry=4 if INDN>=107 & INDN<=398 & year<=2001
replace industry=5 if INDN>=407 & INDN<=499 & year<=2001
replace industry=6 if INDN>=507 & INDN<=699 & year<=2001
replace industry=7 if INDN>= 707 & INDN<=719 & year<=2001 
replace industry=8 if INDN>=727 & INDN<=767 & year<=2001
replace industry=9 if INDN>=769 & INDN<=799 & year<=2001
replace industry=10 if INDN>=807 & INDN<=817 & year<=2001
replace industry=11 if INDN>=828 & INDN<=899 & year<=2001
replace industry=12 if INDN>=907 & INDN<=947 & year<=2001

**2000's
replace industry=1 if INDN>=017 & INDN<=029 & year>=2002
replace industry=2 if INDN>=037 & INDN<=049 & year>=2002
replace industry=3 if INDN==077  & year>=2002
replace industry=4 if INDN>=107 & INDN<=399 & year>=2002
replace industry=4 if INDN>=647 & INDN<=659 & year>=2002
replace industry=4 if INDN>=678 & INDN<=679 & year>=2002
replace industry=5 if INDN>=57 & INDN<=69 & year>=2002
replace industry=5 if INDN>=607 & INDN<=639 & year>=2002
replace industry=5 if INDN>=667 & INDN<=669 & year>=2002
replace industry=6 if INDN>=407 & INDN<=579 & year>=2002
replace industry=6 if INDN>=868 & INDN<=869 & year>=2002
replace industry=7 if INDN>= 687 & INDN<=719 & year>=2002
replace industry=8 if INDN>=877 & INDN<=879 & year>=2002
replace industry=8 if INDN==887 & year>=2002
replace industry=9 if INDN>=866 & INDN<=867 & year>=2002
replace industry=9 if INDN>=829 & INDN<=929 & year>=2002
replace industry=10 if INDN>=856 & INDN<=859 & year>=2002
replace industry=11 if INDN==677 & year>=2002
replace industry=11 if INDN>=727 & INDN<=779 & year>=2002
replace industry=11 if INDN>=786 & INDN<=847 & year>=2002
replace industry=12 if INDN>=937 & INDN<=989 & year>=2002

sort ID year month
quietly replace industry=industry[_n-1] if JN==JN[_n-1] & ID==ID[_n-1] & industry==. & industry[_n-1]>0 & industry[_n-1]~=. & JN~=.
quietly forvalues i = 1/85 {
quietly replace industry=industry[_n+1] if JN==JN[_n+1] & ID==ID[_n+1] & industry==. & industry[_n+1]>0  & industry[_n+1]~=. & JN~=.
}

**b. occupation fe
**major occupation categories as in Autor and Dorn (2013)
gen occupation_agg=1 if (occ1990dd>=3 & occ1990dd<=37) | (occ1990dd>=43 & occ1990dd<=200) | (occ1990dd>=203 & occ1990dd<=235) | (occ1990dd>=243 & occ1990dd<=258) | (occ1990dd>=417 & occ1990dd<=423)
replace occupation_agg=2 if (occ1990dd>=303 & occ1990dd<=389) | (occ1990dd>=274 & occ1990dd<=283)
replace occupation_agg=3 if  (occ1990dd>=405 & occ1990dd<=408) | (occ1990dd==415) | (occ1990dd>=425 & occ1990dd<=427) | (occ1990dd>=433 & occ1990dd<=444) | (occ1990dd>=445 & occ1990dd<=447) | (occ1990dd>=448 & occ1990dd<=455) | (occ1990dd>=457 & occ1990dd<=458) | (occ1990dd>=459 & occ1990dd<=467) | (occ1990dd==468) | (occ1990dd>=469 & occ1990dd<=472)
replace occupation_agg=4  if (occ1990dd>=628 & occ1990dd<=699)
replace occupation_agg=5  if (occ1990dd>=703 & occ1990dd<=799)
replace occupation_agg=6  if (occ1990dd>=803 & occ1990dd<=889) |  (occ1990dd>=558 & occ1990dd<=599) | (occ1990dd>=503 & occ1990dd<=549) |  (occ1990dd>=614 & occ1990dd<=617) | (occ1990dd>=473 & occ1990dd<=475) | (occ1990dd>=479 & occ1990dd<=498)

**c. age
gen age=year-yob-1 if month<mob
replace age=year-yob if month>=mob
gen agesq=age^2
gen agecb=age^3

**d. gender
gen gender=0 if male==1
replace gender=1 if female==1

**e. race
gen race=0 if white==1
replace race=1 if black==1
replace race=2 if hisp==1

**f. region of residence
gen regionfe=region if region>0

**g. education
gen educ_aux=hgraderev[_n-1] if year==year[_n-1]+1 & month==1 & ID==ID[_n-1] & hgraderev[_n-1]>0 & hgraderev<0
gen educ_aux2=hgraderev[_n+1] if year==year[_n+1]-1 & month==12 & ID==ID[_n+1] & hgraderev[_n+1]>0 & hgraderev<0
bysort ID year: egen educ_aux_max=max(educ_aux)
bysort ID year: egen educ_aux_max2=max(educ_aux2)
replace educ_aux_max=hgraderev if (hgraderev~=. & educ_aux_max==.) | (hgraderev>0 & educ_aux_max==.)
replace educ_aux_max2=hgraderev if (hgraderev~=. & educ_aux_max2==.) | (hgraderev>0 & educ_aux_max2==.)
gen hgraderev_alternative=educ_aux_max if educ_aux_max2==educ_aux_max
bysort ID: egen max_educ=max(hgraderev)
bysort ID: egen gradyear_2=max(gradyear)
replace hgraderev_alternative=max_educ if (hgraderev_alternative==. & year>gradyear_2) | (hgraderev_alternative<0 & year>gradyear_2)  
sort ID year month
gen aux_educ3=1 if hgraderev_alternative~=hgraderev_alternative[_n-1] & ID==ID[_n-1] & hgraderev_alternative~=. & hgraderev_alternative[_n-1]~=.
gen aux_grad_year_3=year if aux_educ3==1 & hgraderev_alternative==max_educ
bysort ID: egen gradyear_3=max(aux_grad_year_3)
replace hgraderev_alternative=max_educ if (hgraderev_alternative==. & year>gradyear_3) | (hgraderev_alternative<0 & year>gradyear_3)  
*has a college degree
gen dummy_educ=1 if hgraderev_alternative>15 & hgraderev_alternative~=.
*does not have a college degree
replace dummy_educ=0 if hgraderev_alternative<=15 & hgraderev_alternative>0 & hgraderev_alternative~=.

**h. time trend
sort ID year month
gen time_trend=1 if year==1979 & month==1
replace time_trend=time_trend[_n-1]+1 if ID==ID[_n-1]
label var time_trend "time trend"

********************************************************************************************************

*3. mismatch measure

**3.1 difference between ability and occupational rank in each percentile
foreach var in math verbal technical social {

gen aux4_`var'_weighted=`var'_perc-`var'_perc_occ_weighted
gen aux2_`var'_weighted=abs(`var'_perc-`var'_perc_occ_weighted)

gen aux4_`var'_pos_weighted=aux4_`var'_weighted
replace aux4_`var'_pos_weighted=0 if aux4_`var'_weighted<=0 & aux4_`var'_weighted~=.

gen aux4_`var'_neg_weighted=abs(aux4_`var'_weighted)
replace aux4_`var'_neg_weighted=0 if aux4_`var'_weighted>=0 & aux4_`var'_weighted~=.
}


**3.2 weights
quietly pca aux2_math_weighted aux2_verbal_weighted aux2_technical_weighted aux2_social_weighted if regionfe~=.
matrix M =  e(L)

gen aux_wmath=M[1,1]/M[4,1]
gen aux_wverbal=M[2,1]/M[4,1]
gen aux_wtech=M[3,1]/M[4,1]
gen aux_wsocial=M[4,1]/M[4,1]

foreach var in math verbal tech social {
gen `var'_weight_w=aux_w`var'*(1/(aux_wmath+aux_wverbal+aux_wtech+aux_wsocial))
label var `var'_weight_w "`var' weight"
}
drop aux_w*

**3.3 total mismatch
gen mismatch1w=0.25*abs(aux4_math_weighted)+0.25*abs(aux4_verbal_weighted)+0.25*abs(aux4_technical_weighted)+0.25*abs(aux4_social_weighted)
gen mismatch1w_w=math_weight_w*abs(aux4_math_weighted)+verbal_weight_w*abs(aux4_verbal_weighted)+tech_weight_w*abs(aux4_technical_weighted)+social_weight_w*abs(aux4_social_weighted)
label var mismatch1w "skill mismatch (equally weighted)"
label var mismatch1w_w "skill mismatch (skill-weighted)"

**3.4 positive and negative mismatch
gen mismatch1w_pos=0.25*aux4_math_pos_weighted+0.25*aux4_verbal_pos_weighted+0.25*aux4_technical_pos_weighted+0.25*aux4_social_pos_weighted
gen mismatch1w_neg=0.25*abs(aux4_math_neg_weighted)+0.25*abs(aux4_verbal_neg_weighted)+0.25*abs(aux4_technical_neg_weighted)+0.25*abs(aux4_social_neg_weighted)
label var mismatch1w_pos "overq. (equally weighted)"
label var mismatch1w_neg "underq. (equally weighted)"


********************************************************************************************************

*4. tenure measures

**4.1 employment tenure
sort ID year month
gen time=1 
replace time=time[_n-1]+1 if JN==JN[_n-1] & ID==ID[_n-1] & JN~=. & JN[_n-1]~=.
replace time=. if JN==.
label var time "employment tenure"

**functional form for the baseline hazard.
**parametric
*1. log
gen lntime=ln(time)
*2. polynomial 
gen time2=time^2
gen time3=time^3

**4.2 tenure in previous job.
foreach var in time {
sort ID year month
gen `var'_aux=`var'[_n-1] if aux_emp_unemp==emp_unemp
replace `var'_aux=`var'_aux[_n-1] if aux_emp_unemp==1  & emp_unemp==. 
gen `var'_prev=`var'_aux[_n-1] if ID==ID[_n-1] & eue==1
replace `var'_prev=`var'[_n-1] if ID==ID[_n-1] & emp_emp==1
drop `var'_aux
}

**4.3 labormarket experience.
sort ID year month
gen time_aux2=1 
replace time_aux2=0 if JN==.
bysort ID: ge time_cum=sum(time_aux2)
drop time_aux2
label var time_cum "labor market experience"

**4.4 occupation tenure

*aux var with previous occuppation during 
sort ID year month
foreach var in occ1990dd { 
gen `var'_aux1=`var'
replace `var'_aux1=`var'_aux1[_n-1] if `var'==. & ID==ID[_n-1] 
}

sort ID year month
gen time_occ=1 
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & occ1990dd~=. & occ1990dd[_n-1]~=. & JN~=. & JN[_n-1]~=. 
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
replace time_occ=time_occ[_n-1] if JN==.
replace time_occ=time_occ[_n-1]+1 if occ1990dd==occ1990dd_aux1[_n-1] & ID==ID[_n-1] & JN~=.
label var time_occ "occ. tenure"

gen time_occ_sq=time_occ*time_occ
label var time_occ "occ. tenure sq"

gen time_occ_cb=time_occ*time_occ*time_occ
label var time_occ "occ. tenure cb"


foreach var in occ1990dd time_occ {
sort ID year month
gen `var'_aux=`var'[_n-1] if aux_emp_unemp==emp_unemp
replace `var'_aux=`var'_aux[_n-1] if aux_emp_unemp==1  & emp_unemp==. 
gen `var'_prev=`var'_aux[_n-1] if ID==ID[_n-1] & eue==1
replace `var'_prev=`var'[_n-1] if ID==ID[_n-1] & emp_emp==1
*drop `var'_aux
}
rename occ1990dd_prev occ_prev
label var occ_prev "previous occupation"

*************************************************************************************************************************

*5. occupaiton switch

gen dummy_occ=1 if occ1990dd!=occ_prev & occ_prev!=. & occ1990dd!=. & dummy==1 & occ1990dd>0 & occ_prev>0 
replace dummy_occ=0 if occ1990dd==occ_prev & occ_prev!=. & occ1990dd!=.  & dummy==1 & occ1990dd>0 & occ_prev>0 
label var dummy_occ "occupation switch"

*************************************************************************************************************************

*6. add business cycle measures 
 
sort year month 
merge year month using "$data/Macro indicators/agg_unemp"
drop if _m==2
drop _m


sort year month regionfe 
merge year month regionfe using "$data/Macro indicators/reg_unemp"
drop if _m==2
drop _m

*************************************************************************************************************************

*7. variables at start of the employment spell
 
sort ID year month
foreach var in unempl age agesq agecb {
gen initial_`var'=`var' if dummy==1
replace initial_`var'=initial_`var'[_n-1] if  JN==JN[_n-1]  & ID==ID[_n-1]
}


keep if gender==0
egen num_miss=rowmiss(mismatch1w lhrp2 dummy_educ industry)

save "$data/data_analysis", replace

*************************************************************************************************************************

*8. cumulative mis

use "$data/data_analysis", clear

keep if mismatch1w~=.
gen occ_time = time_occ
gen mismatch1w_time=mismatch1w*time_occ

gen mismatch1w_cum=0
gen time_occ_cum=0

quietly forvalues i = 1/50 {
quietly sort ID year month
quietly replace mismatch1w_cum=mismatch1w_time[_n-1]+mismatch1w_cum[_n-1] if ID==ID[_n-1] & JN~=JN[_n-1] & dummy_occ==1
quietly replace mismatch1w_cum=mismatch1w_time[_n-1] if ID==ID[_n-1] & dummy==1 & dummy_occ==0 & mismatch1w_cum[_n-1]==0
quietly replace mismatch1w_cum=mismatch1w_time[_n-1] if ID==ID[_n-1] & dummy==1 & dummy_occ==. & mismatch1w_cum[_n-1]==0
quietly replace mismatch1w_cum=mismatch1w_cum[_n-1] if ID==ID[_n-1] & dummy==1 & dummy_occ==0 & mismatch1w_cum[_n-1]~=0
quietly replace mismatch1w_cum=mismatch1w_cum[_n-1] if ID==ID[_n-1] & dummy==1 & dummy_occ==. & mismatch1w_cum[_n-1]~=0
quietly replace mismatch1w_cum=mismatch1w_cum[_n-1] if ID==ID[_n-1] & JN==JN[_n-1]

quietly replace time_occ_cum=time_occ[_n-1]+time_occ_cum[_n-1] if ID==ID[_n-1] & JN~=JN[_n-1] & dummy_occ==1
quietly replace time_occ_cum=time_occ[_n-1] if ID==ID[_n-1] & dummy==1 & dummy_occ==0 & time_occ_cum[_n-1]==0
quietly replace time_occ_cum=time_occ[_n-1] if ID==ID[_n-1] & dummy==1 & dummy_occ==. & time_occ_cum[_n-1]==0
quietly replace time_occ_cum=time_occ_cum[_n-1] if ID==ID[_n-1] & dummy==1 & dummy_occ==0 & time_occ_cum[_n-1]~=0
quietly replace time_occ_cum=time_occ_cum[_n-1] if ID==ID[_n-1] & dummy==1 & dummy_occ==. & time_occ_cum[_n-1]~=0
quietly replace time_occ_cum=time_occ_cum[_n-1] if ID==ID[_n-1] & JN==JN[_n-1]
}

gen cum=mismatch1w_cum/time_occ_cum
gen cum2=cum
replace cum2=mismatch1w if cum==.
keep if cum2~=.

keep ID year month cum2
label var cum2 "cumulative mismatch"

sort ID year month
save "$data/cum", replace


use "$data/data_analysis", clear
sort ID year month
merge ID year month using "$data/cum"
sort ID year month
save "$data/data_analysis", replace

*************************************************************************************************************************

**clean up and remove temporary files
! rm "$data/cum.dta"	
! rm "$data/occ_weights.dta"	
