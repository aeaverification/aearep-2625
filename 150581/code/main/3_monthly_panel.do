
*note: this builds converts weekly history file into a monthly panel

clear all

**1. Do a rename, reshape and save of the job number files;

quietly do "$do/main/other/job2to5.do"

**2. Build weekly history file: 

**Read in weekly labour force status for each year, rename, reshape and append;

**2.1 1978-1979**;
infile using "$data/NLSY/weekly labor status/weekly_lstatus_79.DCT", clear
	do "$do/main/other/rename_weekly_lstatus.do"
	sort ID week
	save "$data/_weeklyfile.dta", replace

**2.2 1980-1998**;
forval t=80(2)98{
	infile using "$data/NLSY/weekly labor status/weekly_lstatus_`t'.DCT", clear
	do "$do/main/other/rename_weekly_lstatus.do"
	append using "$data/_weeklyfile.dta"
	sort ID week
	save "$data/_weeklyfile.dta", replace
}

**2.3 1999-2000**;
infile using "$data/NLSY/weekly labor status/weekly_lstatus_00.DCT", clear
	do "$do/main/other/rename_weekly_lstatus.do"
	keep if SAMP<9
	append using "$data/_weeklyfile.dta"
	sort ID week
	save "$data/_weeklyfile.dta", replace

**2.4 2002-2016**;	
forval t=2(2)16{
	infile using "$data/NLSY/weekly labor status/weekly_lstatus_`t'.DCT", clear
	do "$do/main/other/rename_weekly_lstatus2.do"
	append using "$data/_weeklyfile.dta"
	sort ID week
	save "$data/_weeklyfile.dta", replace
}

***************************************************************************************************************************************
***************************************************************************************************************************************

use "$data/_weeklyfile.dta", replace
*keeping only cross-sectionsal sample
	gen year = y
	replace year = 1996 if year==1995
	replace year = 1998 if year==1997
	replace year = 2000 if year==1999
	replace year = 2002 if year==2001
	replace year = 2004 if year==2003
	replace year = 2006 if year==2005
	replace year = 2008 if year==2007
	replace year = 2010 if year==2009
	replace year = 2012 if year==2011
	replace year = 2014 if year==2013
	replace year = 2016 if year==2015
	
	sort ID year
	merge (ID year) using "$data/_individualfile_y.dta", keep(NI)
	keep if _m==3
	drop _merge year

*adds other jobs individuals might be doing that week
	sort ID week
	merge (ID week) using "$data/weekly_j2to5_long.dta"
	*keeping only cross-sectionsal sample
	drop if _m==2
	drop _merge

*variable J1 = primarly job in a given week	
gen J1 = lstatus if lstatus>=100
*coding as a valid skip 
replace J1 = -4 if lstatus<100


sort week
merge week using "$data/NLSY/weekly labor status/week_yqm.dta"
drop if _m==2
drop _merge

rename q quarter
rename y year
rename m month

sort ID week

compress

save "$data/_weeklyfile.dta", replace

***************************************************************************************************************************************
***************************************************************************************************************************************

*3. add hours for each job 

use "$data/_weeklyfile.dta", clear

** JOB NUMBER 1;
	keep ID SAMP week J1 lstatus  
	rename J1 JOB
	sort ID JOB
	merge (ID JOB) using "$data/_jobfile.dta"
	drop if _m==2
	drop _merge
	keep ID SAMP week HRS JOB IND_ lstatus
	rename JOB WJN1
	rename HRS WHJ1
	rename IND_ IND_1
	mvencode WHJ1, mv(-100)
	replace WHJ1 = -100 if WHJ1<0
	compress
	sort ID week
	save "$data/weekly_j1to5_hrs.dta", replace

** JOB NUMBER 2;
	use "$data/_weeklyfile.dta", clear
	keep ID SAMP week J2 
	rename J2 JOB 
	sort ID JOB 
	merge (ID JOB ) using "$data/_jobfile.dta"
	drop if _m==2
	drop _merge
	keep ID SAMP week HRS JOB IND_
	rename JOB WJN2
	rename HRS WHJ2
	rename IND_ IND_2	
	mvencode WHJ2, mv(-100)
	replace WHJ2 = -100 if WHJ2<0
	sort ID week
	merge (ID week) using "$data/weekly_j1to5_hrs.dta"
	drop _merge
	compress
	sort ID week
	save "$data/weekly_j1to5_hrs.dta", replace

** JOB NUMBER 3;
	use "$data/_weeklyfile.dta", clear
	keep ID SAMP week J3 
	rename J3 JOB 
	sort ID JOB 
	merge (ID JOB ) using "$data/_jobfile.dta"
	drop if _m==2
	drop _merge
	keep ID SAMP week HRS JOB IND_
	rename JOB WJN3
	rename HRS WHJ3
	rename IND_ IND_3
	mvencode WHJ3, mv(-100)
	replace WHJ3 = -100 if WHJ3<0
	sort ID week
	merge (ID week) using "$data/weekly_j1to5_hrs.dta"
	drop _merge
	compress
	sort ID week
	save "$data/weekly_j1to5_hrs.dta", replace

** JOB NUMBER 4;
	use "$data/_weeklyfile.dta", clear
	keep ID SAMP week J4 
	rename J4 JOB 
	sort ID JOB 
	merge (ID JOB ) using "$data/_jobfile.dta"
	drop if _m==2
	drop _merge
	keep ID SAMP week HRS JOB  IND_
	rename JOB WJN4
	rename HRS WHJ4
	rename IND_ IND_4
	mvencode WHJ4, mv(-100)
	replace WHJ4 = -100 if WHJ4<0
	sort ID week
	merge (ID week) using "$data/weekly_j1to5_hrs.dta"
	drop _merge
	compress
	sort ID week
	save "$data/weekly_j1to5_hrs.dta", replace
	sum ID week

** JOB NUMBER 5;
	use "$data/_weeklyfile.dta", clear
	keep ID SAMP week J5 
	rename J5 JOB 
	sort ID JOB 
	merge (ID JOB) using "$data/_jobfile.dta"
	drop if _m==2
	drop _merge
	keep ID SAMP week HRS JOB IND_ 
	rename JOB WJN5
	rename HRS WHJ5
	rename IND_ IND_4
	mvencode WHJ5, mv(-100)
	replace WHJ5 = -100 if WHJ5<0
	sort ID week
	merge (ID week) using "$data/weekly_j1to5_hrs.dta"


drop _merge
compress
sort ID week
save "$data/weekly_j1to5_hrs.dta", replace
	
***************************************************************************************************************************************
***************************************************************************************************************************************

*4.construct monthly panel

use "$data/weekly_j1to5_hrs.dta", replace	

*match week with month, quarter & year
sort week
merge week using "$data/NLSY/weekly labor status/week_yqm.dta"
*drop year 1978 from file week_ymq
keep if _m==3
drop _merge

rename q quarter
rename y year
rename m month

rename lstatus lstatus_orignal
label var lstatus "labor status: variable from the dataset"
	
*WEEKLY JOB
**identify job number in each week for job with the most hours;
gen JOBW = WJN5 if WHJ5>=max(WHJ1, WHJ2, WHJ3, WHJ4, WHJ5)
replace JOBW = WJN4 if WHJ4>=max(WHJ1, WHJ2, WHJ3, WHJ4, WHJ5)
replace JOBW = WJN3 if WHJ3>=max(WHJ1, WHJ2, WHJ3, WHJ4, WHJ5)
replace JOBW = WJN2 if WHJ2>=max(WHJ1, WHJ2, WHJ3, WHJ4, WHJ5)
replace JOBW = WJN1 if WHJ1>=max(WHJ1, WHJ2, WHJ3, WHJ4, WHJ5)
label var JOBW "Job number of job with highest hours in given week"

**identify of what the person is doing that week
**if on the labor market, it has only one job attached.
gen lstatus=lstatus_orignal
**focus on one main job in that week
replace lstatus=JOBW if lstatus_orignal>=100 

**identify hours for job in each week for job with the most hours;
gen mhrs_week= WHJ5 if WHJ5>=max(WHJ1, WHJ2, WHJ3, WHJ4, WHJ5)
replace mhrs_week = WHJ4 if WHJ4>=max(WHJ1, WHJ2, WHJ3, WHJ4, WHJ5)
replace mhrs_week = WHJ3 if WHJ3>=max(WHJ1, WHJ2, WHJ3, WHJ4, WHJ5)
replace mhrs_week = WHJ2 if WHJ2>=max(WHJ1, WHJ2, WHJ3, WHJ4, WHJ5)
replace mhrs_week = WHJ1 if WHJ1>=max(WHJ1, WHJ2, WHJ3, WHJ4, WHJ5)
replace mhrs_week = 38 if lstatus_orignal==4 | lstatus_orignal==5 | lstatus_orignal==7 | lstatus_orignal==2
replace mhrs_week = 0 if lstatus_orignal==3| lstatus_orignal==0 | lstatus_orignal==1 
replace mhrs_week = 0 if mhrs_week==-100

bysort ID year month lstatus: egen mhrs_month= sum(mhrs_week)
label var lstatus "hours worker per month in each lstatus"

gen aux=1
bysort ID year month lstatus: egen total_weeks= sum(aux)
drop aux

by ID year month, sort: gen pid_month = _n 
egen max_month = max(mhrs_month), by(ID year month)
gen JOBM=lstatus if mhrs_month==max_month & pid_month==1
gen HOURSM=mhrs_month if mhrs_month==max_month & pid_month==1
gen hours_week=mhrs_week if mhrs_month==max_month & pid_month==1
gen number_week=total_weeks if mhrs_month==max_month & pid_month==1

forval i=1/5{
	bysort ID year month: replace JOBM=lstatus[_n+`i'] if mhrs_month~=max_month & mhrs_month[_n+`i']==max_month & pid_month==1
	bysort ID year month: replace HOURSM=mhrs_month[_n+`i'] if mhrs_month~=max_month & mhrs_month[_n+`i']==max_month & pid_month==1
	bysort ID year month: replace hours_week=mhrs_week[_n+`i'] if mhrs_month~=max_month & mhrs_month[_n+`i']==max_month & pid_month==1
	bysort ID year month: replace number_week=total_weeks[_n+`i'] if mhrs_month~=max_month & mhrs_month[_n+`i']==max_month & pid_month==1

	}
bysort ID year month: replace JOBM=JOBM[_n-1] if pid_month~=1
bysort ID year month: replace HOURSM=HOURSM[_n-1] if pid_month~=1
bysort ID year month: replace hours_week=hours_week[_n-1] if pid_month~=1
bysort ID year month: replace number_week=number_week[_n-1] if pid_month~=1

replace HOURSM=0 if JOBM<100
label var JOBM "job number/lstatus with highest hours in given month"
label var HOURSM "hours worked in a given month"

sort ID week
compress

duplicates drop ID year month, force

save "$data/data_month.dta", replace

***************************************************************************************************************************************
***************************************************************************************************************************************

*5. add information about the job and individual

use "$data/data_month.dta", clear

keep ID SAMP year month quarter JOBM HOURSM hours_week number_week
rename JOBM JOB
sort ID JOB

*5.1 add job info
	merge (ID JOB) using "$data/_jobfile.dta"
	drop if _m==2
	drop _m

*5.2 add individual info
	sort ID
	merge m:1 ID using "$data/_individualfile.dta"
	drop _m
	sort ID year
	merge ID year using "$data/_individualfile_y.dta"
	drop _m

save "$data/data_month.dta", replace

***************************************************************************************************************************************
***************************************************************************************************************************************

**clean up and remove temporary files
! rm "$data/_individualfile.dta"	
! rm "$data/_individualfile_y.dta"	
! rm "$data/_jobfile.dta"	
! rm "$data/weekly_j1to5_hrs.dta"	
! rm "$data/_weeklyfile.dta"	
! rm "$data/weekly_j2to5_long.dta"	
! rm "$data/Macro indicators/cpi.dta"




