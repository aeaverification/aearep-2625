
*note: this file puts together information at the job level

clear all

tempfile a b

******************************************************************************************************************************************************

*1. industry

infile using "$data/NLSY/job info/industry.dct", clear

  rename R0000100 ID
  rename R0046300 IND91979 
  rename R0090100 IND21979   
  rename R0090200 IND31979   
  rename R0090300 IND41979   
  rename R0090400 IND51979   
  rename R0263300 IND91980 
  rename R0338400 IND11980   
  rename R0349900 IND21980   
  rename R0361400 IND31980   
  rename R0372900 IND41980   
  rename R0384400 IND51980   
  rename R0446300 IND91981 
  rename R0546100 IND11981   
  rename R0559200 IND21981   
  rename R0572300 IND31981   
  rename R0585400 IND41981   
  rename R0598500 IND51981   
  rename R0702000 IND91982 
  rename R0840600 IND11982   
  rename R0853700 IND21982   
  rename R0866800 IND31982   
  rename R0879900 IND41982   
  rename R0893000 IND51982   
  rename R0944900 IND91983 
  rename R1087800 IND11983   
  rename R1101000 IND21983   
  rename R1114200 IND31983   
  rename R1127400 IND41983   
  rename R1140600 IND51983   
  rename R1255300 IND91984 
  rename R1463500 IND11984   
  rename R1476600 IND21984   
  rename R1489700 IND31984   
  rename R1502800 IND41984   
  rename R1515900 IND51984   
  rename R1650100 IND91985 
  rename R1810300 IND11985   
  rename R1823000 IND21985   
  rename R1835700 IND31985   
  rename R1848400 IND41985   
  rename R1861100 IND51985   
  rename R1922700 IND91986 
  rename R2172000 IND11986   
  rename R2185600 IND21986   
  rename R2199200 IND31986   
  rename R2212800 IND41986   
  rename R2226400 IND51986   
  rename R2317500 IND91987 
  rename R2376800 IND11987   
  rename R2388100 IND21987   
  rename R2399400 IND31987   
  rename R2410700 IND41987   
  rename R2422000 IND51987   
  rename R2525300 IND91988 
  rename R2771600 IND11988   
  rename R2784500 IND21988   
  rename R2797400 IND31988   
  rename R2810300 IND41988   
  rename R2823200 IND51988   
  rename R2924300 IND91989 
  rename R3013400 IND11989   
  rename R3026500 IND21989   
  rename R3039600 IND31989   
  rename R3052700 IND41989   
  rename R3065800 IND51989   
  rename R3127000 IND91990 
  rename R3340800 IND11990   
  rename R3354800 IND21990   
  rename R3368800 IND31990   
  rename R3382800 IND41990   
  rename R3396800 IND51990   
  rename R3522700 IND91991 
  rename R3605100 IND11991   
  rename R3617200 IND21991   
  rename R3629300 IND31991   
  rename R3641400 IND41991   
  rename R3653500 IND51991   
  rename R3727700 IND91992 
  rename R3955300 IND11992   
  rename R3967500 IND21992   
  rename R3979700 IND31992   
  rename R3991900 IND41992   
  rename R4004100 IND51992   
  rename R4182000 IND91993 
  rename R4206200 IND21993   
  rename R4213400 IND31993   
  rename R4221000 IND41993   
  rename R4228400 IND51993   
  rename R4587903 IND11994   
  rename R4631901 IND21994   
  rename R4675901 IND31994   
  rename R4715201 IND41994   
  rename R4749401 IND51994   
  rename R5270500 IND11996   
  rename R5310800 IND21996   
  rename R5349800 IND31996   
  rename R5386900 IND41996   
  rename R5421400 IND51996   
  rename R6472100 IND11998   
  rename R6472200 IND21998   
  rename R6472300 IND31998   
  rename R6472400 IND41998   
  rename R6472500 IND51998   
  rename R6591300 IND12000   
  rename R6591400 IND22000   
  rename R6591500 IND32000   
  rename R6591600 IND42000   
  rename R6591700 IND52000  
  rename R7209100 IND12002   
  rename R7209200 IND22002   
  rename R7209300 IND32002   
  rename R7209400 IND42002   
  rename R7209500 IND52002    
  rename R7897500 IND12004   
  rename R7897600 IND22004   
  rename R7897700 IND32004   
  rename R7897800 IND42004   
  rename R7897900 IND52004   
  rename T0137900 IND12006   
  rename T0138000 IND22006   
  rename T0138100 IND32006   
  rename T0138200 IND42006   
  rename T0138300 IND52006   
  rename T1297500 IND12008   
  rename T1297600 IND22008   
  rename T1297700 IND32008   
  rename T1297800 IND42008   
  rename T1297900 IND52008   
  rename T2326000 IND12010   
  rename T2326100 IND22010   
  rename T2326200 IND32010   
  rename T2326300 IND42010   
  rename T2326400 IND52010    
  rename T3308200 IND12012   
  rename T3308300 IND22012   
  rename T3308400 IND32012   
  rename T3308500 IND42012   
  rename T3308600 IND52012   
  rename T4282300 IND12014   
  rename T4282400 IND22014 
  rename T4282500 IND32014   
  rename T4282600 IND42014  
  rename T4282700 IND52014  
  rename T5256400 IND12016   
  rename T5256500 IND22016  
  rename T5256600 IND32016  
  rename T5256700 IND42016   
  rename T5256800 IND52016  
  drop R*
  
reshape i ID
reshape j year
reshape xij IND1 IND2 IND3 IND4 IND5 IND9
reshape long


mvencode IND1 IND2 IND3 IND4 IND5 IND9, mv(-999)

sort ID year

save "$data/job_annual_file.dta", replace

******************************************************************************************************************************************************

*2. occupation

infile using "$data/NLSY/job info/occupation.dct", clear

** Renaming the occupation variables

rename R0000100 ID
rename R0046400 OCC11979
rename R0089700 OCC21979
rename R0089800 OCC31979
rename R0089900 OCC41979
rename R0090000 OCC51979
rename R0338300 OCC11980
rename R0349800 OCC21980
rename R0361300 OCC31980
rename R0372800 OCC41980
rename R0384300 OCC51980
rename R0263400 OCC91980
rename R0546000 OCC11981
rename R0559100 OCC21981
rename R0572200 OCC31981
rename R0585300 OCC41981
rename R0598400 OCC51981
rename R0446400 OCC91981
rename R0840500 OCC11982
rename R0853600 OCC21982
rename R0866700 OCC31982
rename R0879800 OCC41982
rename R0892900 OCC51982
rename R0702100 OCC91982
rename R1087700 OCC11983
rename R1100900 OCC21983
rename R1114100 OCC31983
rename R1127300 OCC41983
rename R1140500 OCC51983
rename R0945000 OCC91983
rename R1463400 OCC11984
rename R1476500 OCC21984
rename R1489600 OCC31984
rename R1502700 OCC41984
rename R1515800 OCC51984
rename R1255400 OCC91984
rename R1810200 OCC11985
rename R1822900 OCC21985
rename R1835600 OCC31985
rename R1848300 OCC41985
rename R1861000 OCC51985
rename R1650200 OCC91985
rename R2171900 OCC11986
rename R2185500 OCC21986
rename R2199100 OCC31986
rename R2212700 OCC41986
rename R2226300 OCC51986
rename R1922800 OCC91986
rename R2376700 OCC11987
rename R2388000 OCC21987
rename R2399300 OCC31987
rename R2410600 OCC41987
rename R2421900 OCC51987
rename R2317600 OCC91987
rename R2771500 OCC11988
rename R2784400 OCC21988
rename R2797300 OCC31988
rename R2810200 OCC41988
rename R2823100 OCC51988
rename R2525400 OCC91988
rename R3013300 OCC11989
rename R3026400 OCC21989
rename R3039500 OCC31989
rename R3052600 OCC41989
rename R3065700 OCC51989
rename R2924400 OCC91989
rename R3340700 OCC11990
rename R3354700 OCC21990
rename R3368700 OCC31990
rename R3382700 OCC41990
rename R3396700 OCC51990
rename R3127100 OCC91990
rename R3605000 OCC11991
rename R3617100 OCC21991
rename R3629200 OCC31991
rename R3641300 OCC41991
rename R3653400 OCC51991
rename R3522800 OCC91991
rename R3955200 OCC11992
rename R3967400 OCC21992
rename R3979600 OCC31992
rename R3991800 OCC41992
rename R4004000 OCC51992
rename R3727800 OCC91992
rename R4182100 OCC11993
rename R4206100 OCC21993
rename R4213300 OCC31993
rename R4220900 OCC41993
rename R4228300 OCC51993
rename R4587904 OCC11994
rename R4631902 OCC21994
rename R4675902 OCC31994
rename R4715202 OCC41994
rename R4749402 OCC51994
rename R5270600 OCC11996
rename R5310900 OCC21996
rename R5349900 OCC31996
rename R5387000 OCC41996
rename R5421500 OCC51996
rename R6472600 OCC11998
rename R6472700 OCC21998
rename R6472800 OCC31998
rename R6472900 OCC41998
rename R6473000 OCC51998
rename R6591800 OCC12000
rename R6591900 OCC22000
rename R6592000 OCC32000
rename R6592100 OCC42000
rename R6592200 OCC52000
rename R7209600 OCC12002   
rename R7209700 OCC22002   
rename R7209800 OCC32002   
rename R7209900 OCC42002   
rename R7210000 OCC52002   
rename R7898000 OCC12004   
rename R7898100 OCC22004   
rename R7898200 OCC32004   
rename R7898300 OCC42004   
rename R7898400 OCC52004   
rename T0138400 OCC12006   
rename T0138500 OCC22006   
rename T0138600 OCC32006   
rename T0138700 OCC42006   
rename T0138800 OCC52006   
rename T1298000 OCC12008   
rename T1298100 OCC22008   
rename T1298200 OCC32008   
rename T1298300 OCC42008   
rename T1298400 OCC52008   
rename T2326500 OCC12010   
rename T2326600 OCC22010   
rename T2326700 OCC32010   
rename T2326800 OCC42010   
rename T2326900 OCC52010   
rename T3308700 OCC12012   
rename T3308800 OCC22012   
rename T3308900 OCC32012   
rename T3309000 OCC42012   
rename T3309100 OCC52012   
rename T4282800 OCC12014   
rename T4282900 OCC22014  
rename T4283000 OCC32014  
rename T4283100 OCC42014   
rename T4283200 OCC52014   
rename T5256900 OCC12016   
rename T5257000 OCC22016   
rename T5257100 OCC32016   
rename T5257200 OCC42016  
rename T5257300 OCC52016 
  
*reshaping dataset from wide long
reshape i ID
reshape j year
*reshape xi SAMP
reshape xij OCC1 OCC2 OCC3 OCC4 OCC5 OCC9
reshape long

mvencode OCC1 OCC2 OCC3 OCC4 OCC5 OCC9, mv(-999)

sort ID year

save `b', replace

*

keep if year<=2000

forval i=1/5{
rename OCC`i' occ1970
sort occ1970
merge occ1970 using "$data/crosswalks/occ1970_occ1990dd"
drop if _m==2
drop _m
rename occ1970 OCC`i'
rename occ1990dd OCC`i'_occ1990
}

rename OCC9 occ1970
sort occ1970
merge occ1970 using "$data/crosswalks/occ1970_occ1990dd"
drop if _m==2
drop _m
rename occ1970 OCC9
rename occ1990dd OCC9_occ1990

foreach var in OCC1 OCC2 OCC3 OCC4 OCC5 OCC9 {
replace `var'_occ1990=`var' if `var'<0
replace `var'_occ1990=999 if `var'==995
replace `var'_occ1990=905 if `var'==590
}


save `a', replace

*******

use `b', replace
keep if year>2000

forval i=1/5{
tostring OCC`i', replace
gen aux=length(OCC`i')
gen aux2=substr(OCC`i',1,aux-1) if year>=2004 & OCC`i'~="-4" & OCC`i'~="-5" & OCC`i'~="-3" & OCC`i'~="-2" & OCC`i'~="-1" & OCC`i'~="0"
replace OCC`i'=aux2 if aux2~=""
destring OCC`i', replace
rename OCC`i' occ2000
sort occ2000
merge occ2000 using "$data/crosswalks/occ2000_occ1990dd"
drop if _m==2
drop _m
rename occ2000 OCC`i'
rename occ1990dd OCC`i'_occ1990
drop aux aux2
}

*occ9
tostring OCC9, replace
gen aux=length(OCC9)
gen aux2=substr(OCC9,1,aux-1) if year>=2004 & OCC9~="-4" & OCC9~="-5" & OCC9~="-3" & OCC9~="-2" & OCC9~="-1" & OCC9~="0"
replace OCC9=aux2 if aux2~=""
destring OCC9, replace
rename OCC9 occ2000
sort occ2000
merge occ2000 using "$data/crosswalks/occ2000_occ1990dd"
drop if _m==2
drop _m
rename occ2000 OCC9
rename occ1990dd OCC9_occ1990

foreach var in OCC1 OCC2 OCC3 OCC4 OCC5 OCC9 {
replace `var'_occ1990=`var' if `var'<0
replace `var'_occ1990=68 if `var'==121 | `var'==123
replace `var'_occ1990=59 if `var'==134
replace `var'_occ1990=418 if `var'==383
replace `var'_occ1990=684 if `var'==802 | `var'==812
replace `var'_occ1990=805 if `var'==911
replace `var'_occ1990=859 if `var'==950
replace `var'_occ1990=905 if `var'==983 | `var'==984
replace `var'_occ1990=999 if `var'==999
}

append using `a'
drop aux aux2
sort ID year

merge (ID year) using "$data/job_annual_file.dta"
drop _merge

sort ID year
save "$data/job_annual_file.dta", replace

******************************************************************************************************************************************************

*3. wages

do "$do/main/other/cpi.do"  

infile using "$data/NLSY/job info/HRP.dct", clear

cap drop R0214800 R0214700 A0002500:
cap drop R0173600
rename R0000100 ID  
rename R0091710 HRP11979 
rename R0091910 HRP21979 
rename R0092110 HRP31979 
rename R0092310 HRP41979 
rename R0092510 HRP51979 
rename R0338910 HRP11980 
rename R0350410 HRP21980 
rename R0361910 HRP31980 
rename R0373410 HRP41980 
rename R0384910 HRP51980 
rename R0546610 HRP11981 
rename R0559710 HRP21981 
rename R0572810 HRP31981 
rename R0585910 HRP41981 
rename R0599010 HRP51981 
rename R0841010 HRP11982 
rename R0854110 HRP21982 
rename R0867210 HRP31982 
rename R0880310 HRP41982 
rename R0893410 HRP51982 
rename R1088210 HRP11983 
rename R1101410 HRP21983 
rename R1114610 HRP31983 
rename R1127810 HRP41983 
rename R1141010 HRP51983 
rename R1463910 HRP11984 
rename R1477010 HRP21984 
rename R1490110 HRP31984 
rename R1503210 HRP41984 
rename R1516310 HRP51984 
rename R1810710 HRP11985 
rename R1823410 HRP21985 
rename R1836110 HRP31985 
rename R1848810 HRP41985 
rename R1861510 HRP51985 
rename R2172410 HRP11986 
rename R2186010 HRP21986 
rename R2199610 HRP31986 
rename R2213210 HRP41986 
rename R2226810 HRP51986 
rename R2377210 HRP11987 
rename R2388510 HRP21987 
rename R2399810 HRP31987 
rename R2411110 HRP41987 
rename R2422410 HRP51987 
rename R2772210 HRP11988 
rename R2785110 HRP21988 
rename R2798010 HRP31988 
rename R2810910 HRP41988 
rename R2823810 HRP51988 
rename R3014010 HRP11989 
rename R3027110 HRP21989 
rename R3040210 HRP31989 
rename R3053310 HRP41989 
rename R3066410 HRP51989 
rename R3341500 HRP11990 
rename R3355500 HRP21990 
rename R3369500 HRP31990 
rename R3383500 HRP41990 
rename R3397500 HRP51990 
rename R3605800 HRP11991 
rename R3617900 HRP21991 
rename R3630000 HRP31991 
rename R3642100 HRP41991 
rename R3654200 HRP51991 
rename R3956000 HRP11992 
rename R3968200 HRP21992 
rename R3980400 HRP31992 
rename R3992600 HRP41992 
rename R4004800 HRP51992 
rename R4416900 HRP11993 
rename R4417000 HRP21993 
rename R4417100 HRP31993 
rename R4417200 HRP41993 
rename R4417300 HRP51993 
rename R5079900 HRP11994 
rename R5080000 HRP21994 
rename R5080100 HRP31994 
rename R5080200 HRP41994 
rename R5080300 HRP51994 
rename R5165200 HRP11996 
rename R5165300 HRP21996 
rename R5165400 HRP31996 
rename R5165500 HRP41996 
rename R5165600 HRP51996 
rename R6478000 HRP11998 
rename R6478100 HRP21998 
rename R6478200 HRP31998 
rename R6478300 HRP41998 
rename R6478400 HRP51998 
rename R7005700 HRP12000 
rename R7005800 HRP22000 
rename R7005900 HRP32000 
rename R7006000 HRP42000 
rename R7006100 HRP52000 
rename R7702900 HRP12002  
rename R7703000 HRP22002 
rename R7703100 HRP32002 
rename R7703200 HRP42002 
rename R7703300 HRP52002 
rename R8495200 HRP12004 
rename R8495300 HRP22004 
rename R8495400 HRP32004 
rename R8495500 HRP42004 
rename R8495600 HRP52004 
rename T0986800 HRP12006 
rename T0986900 HRP22006 
rename T0987000 HRP32006 
rename T0987100 HRP42006 
rename T0987200 HRP52006 
rename T2209100 HRP12008 
rename T2209200 HRP22008 
rename T2209300 HRP32008 
rename T2209400 HRP42008 
rename T2209500 HRP52008 
rename T3106900 HRP12010 
rename T3107000 HRP22010 
rename T3107100 HRP32010 
rename T3107200 HRP42010 
rename T3107300 HRP52010 
rename T4110900 HRP12012 
rename T4111100 HRP22012 
rename T4111300 HRP32012 
rename T4111500 HRP42012 
rename T4111700 HRP52012
rename T5019700 HRP12014 
rename T5019900 HRP22014 
rename T5020100 HRP32014 
rename T5020300 HRP42014 
rename T5020500 HRP52014 
rename T5768600 HRP12016 
rename T5768800 HRP22016 
rename T5769000 HRP32016 
rename T5769200 HRP42016 
rename T5769400 HRP52016
  
reshape clear
reshape i ID
reshape j year
reshape xij HRP1 HRP2 HRP3 HRP4 HRP5
reshape long

sort ID year

sort year	

merge (year) using "$data/Macro indicators/cpi.dta"
drop if _m==2
drop _m

forval  i=1/5{
gen lhrp`i'=log(HRP`i'*conv) if HRP`i' >=0
}

sort ID year
merge (ID year) using "$data/job_annual_file.dta"
drop _merge

sort ID year
save "$data/job_annual_file.dta", replace

******************************************************************************************************************************************************

*4. hours

infile using "$data/NLSY/job info/hoursbyjob.dct", clear

*drop R0173600 R0214700 R0214800

rename R0000100 ID 
rename R0070500 HW11979 
rename R0070600 HW21979 
rename R0070700 HW31979 
rename R0070800 HW41979 
rename R0070900 HW51979 
rename R0337800 HW11980 
rename R0349300 HW21980 
rename R0360800 HW31980 
rename R0372300 HW41980 
rename R0383800 HW51980 
rename R0545600 HW11981 
rename R0558700 HW21981 
rename R0571800 HW31981 
rename R0584900 HW41981 
rename R0598000 HW51981 
rename R0839900 HW11982 
rename R0853000 HW21982 
rename R0866100 HW31982 
rename R0879200 HW41982 
rename R0892300 HW51982 
rename R1087300 HW11983 
rename R1100500 HW21983 
rename R1113700 HW31983 
rename R1126900 HW41983 
rename R1140100 HW51983 
rename R1463000 HW11984 
rename R1476100 HW21984 
rename R1489200 HW31984 
rename R1502300 HW41984 
rename R1515400 HW51984 
rename R1809800 HW11985 
rename R1822500 HW21985 
rename R1835200 HW31985 
rename R1847900 HW41985 
rename R1860600 HW51985 
rename R2171500 HW11986 
rename R2185100 HW21986 
rename R2198700 HW31986 
rename R2212300 HW41986 
rename R2225900 HW51986 
rename R2376300 HW11987 
rename R2387600 HW21987 
rename R2398900 HW31987 
rename R2410200 HW41987 
rename R2421500 HW51987 
rename R2770900 HW11988 
rename R2771200 HH11988 
rename R2783800 HW21988 
rename R2784100 HH21988 
rename R2796700 HW31988 
rename R2797000 HH31988 
rename R2809600 HW41988 
rename R2809900 HH41988 
rename R2822500 HW51988 
rename R2822800 HH51988 
rename R3012700 HW11989 
rename R3013000 HH11989 
rename R3025800 HW21989 
rename R3026100 HH21989 
rename R3038900 HW31989 
rename R3039200 HH31989 
rename R3052000 HW41989 
rename R3052300 HH41989 
rename R3065100 HW51989 
rename R3065400 HH51989 
rename R3340100 HW11990 
rename R3340400 HH11990 
rename R3354100 HW21990 
rename R3354400 HH21990 
rename R3368100 HW31990 
rename R3368400 HH31990 
rename R3382100 HW41990 
rename R3382400 HH41990 
rename R3396100 HW51990 
rename R3396400 HH51990 
rename R3604400 HW11991 
rename R3604700 HH11991 
rename R3616500 HW21991 
rename R3616800 HH21991 
rename R3628600 HW31991 
rename R3628900 HH31991 
rename R3640700 HW41991 
rename R3641000 HH41991 
rename R3652800 HW51991 
rename R3653100 HH51991 
rename R3954600 HW11992 
rename R3954900 HH11992 
rename R3966800 HW21992 
rename R3967100 HH21992 
rename R3979000 HW31992 
rename R3979300 HH31992 
rename R3991200 HW41992 
rename R3991500 HH41992 
rename R4003400 HW51992 
rename R4003700 HH51992 
rename R4193200 HW11993 
rename R4205500 HW21993 
rename R4205800 HH21993 
rename R4212700 HW31993 
rename R4213000 HH31993 
rename R4220300 HW41993 
rename R4220600 HH41993 
rename R4227800 HW51993 
rename R4582900 HW11994 
rename R4583200 HH11994 
rename R4627100 HW21994 
rename R4627400 HH21994 
rename R4671000 HW31994 
rename R4671300 HH31994 
rename R4710600 HW41994 
rename R4710900 HH41994 
rename R4744900 HW51994 
rename R4745200 HH51994 
rename R5267100 HW11996 
rename R5267400 HH11996 
rename R5307400 HW21996 
rename R5307700 HH21996 
rename R5346600 HW31996 
rename R5346900 HH31996 
rename R5383700 HW41996 
rename R5384000 HH41996 
rename R5418200 HW51996 
rename R5418500 HH51996 
rename R5912600 HW11998 
rename R5912700 HW21998 
rename R5912800 HW31998 
rename R5912900 HW41998 
rename R5913000 HW51998 
rename R5916200 HH11998 
rename R5916300 HH21998 
rename R5916400 HH31998 
rename R5916500 HH41998 
rename R5916600 HH51998 
rename R6578200 HW12000 
rename R6578300 HW22000 
rename R6578400 HW32000 
rename R6578500 HW42000 
rename R6578600 HW52000 
rename R6579700 HH12000 
rename R6579800 HH22000 
rename R6579900 HH32000 
rename R6580000 HH42000 
rename R6580100 HH52000 

** this is renaming for matching with CPS job
rename R0043600  HW91979
rename R0068000  J911979
rename R0068100  J921979
rename R0068200  J931979
rename R0068300  J941979
rename R0068400  J951979
rename R0264300  HW91980
rename R0337700  J911980
rename R0349200  J921980
rename R0360700  J931980
rename R0372200  J941980
rename R0383700  J951980
rename R0446900  HW91981
rename R0545500  J911981
rename R0558600  J921981
rename R0571700  J931981
rename R0584800  J941981
rename R0597900  J951981
rename R0702600  HW91982
rename R0840100  J911982
rename R0853200  J921982
rename R0866300  J931982
rename R0879400  J941982
rename R0892500  J951982
rename R0945700  HW91983
rename R1087200  J911983
rename R1100400  J921983
rename R1113600  J931983
rename R1126800  J941983
rename R1140000  J951983
rename R1256100  HW91984
rename R1462900  J911984
rename R1476000  J921984
rename R1489100  J931984
rename R1502200  J941984
rename R1515300  J951984
rename R1650900  HW91985
rename R1809700  J911985
rename R1822400  J921985
rename R1835100  J931985
rename R1847800  J941985
rename R1860500  J951985
rename R1923500  HW91986
rename R2171400  J911986
rename R2185000  J921986
rename R2198600  J931986
rename R2212200  J941986
rename R2225800  J951986
rename R2318300  HW91987
rename R2376200  J911987
rename R2387500  J921987
rename R2398800  J931987
rename R2410100  J941987
rename R2421400  J951987
rename R2526100  HW91988
rename R2526400  HH91988
rename R2770800  J911988
rename R2783700  J921988
rename R2796600  J931988
rename R2809500  J941988
rename R2822400  J951988
rename R2925100  HW91989
rename R2925400  HH91989
rename R3012600  J911989
rename R3025700  J921989
rename R3038800  J931989
rename R3051900  J941989
rename R3065000  J951989
rename R3127900  HW91990
rename R3128200  HH91990
rename R3340000  J911990
rename R3354000  J921990
rename R3368000  J931990
rename R3382000  J941990
rename R3396000  J951990
rename R3523600  HW91991
rename R3523900  HH91991
rename R3604300  J911991
rename R3616400  J921991
rename R3628500  J931991
rename R3640600  J941991
rename R3652700  J951991
rename R3728600  HW91992
rename R3728900  HH91992
rename R3954500  J911992
rename R3966700  J921992
rename R3978900  J931992
rename R3991100  J941992
rename R4003300  J951992
rename R4182600  HW91993
rename R4182900  HH91993
rename R4193100  J911993

rename R7192900 HW12002   
rename R7193000 HW22002   
rename R7193100 HW32002   
rename R7193200 HW42002   
rename R7193300 HW52002   
rename R7194400 HH12002   
rename R7194500 HH22002   
rename R7194600 HH32002   
rename R7194700 HH42002   
rename R7194800 HH52002   
rename R7879600 HW12004   
rename R7879700 HW22004   
rename R7879800 HW32004   
rename R7879900 HW42004   
rename R7880000 HW52004   
rename R7881100 HH12004   
rename R7881200 HH22004   
rename R7881300 HH32004   
rename R7881400 HH42004   
rename R7881500 HH52004   
rename T0121900 HW12006   
rename T0122000 HW22006   
rename T0122100 HW32006   
rename T0122200 HW42006   
rename T0122300 HW52006    
rename T0123400 HH12006   
rename T0123500 HH22006   
rename T0123600 HH32006   
rename T0123700 HH42006   
rename T0123800 HH52006    
rename T1281800 HW12008   
rename T1281900 HW22008   
rename T1282000 HW32008   
rename T1282100 HW42008   
rename T1282200 HW52008   
rename T1283300 HH12008   
rename T1283400 HH22008   
rename T1283500 HH32008   
rename T1283600 HH42008    
rename T2302300 HW12010   
rename T2302400 HW22010   
rename T2302500 HW32010   
rename T2302600 HW42010   
rename T2302700 HW52010   
rename T2303800 HH12010   
rename T2303900 HH22010   
rename T2304000 HH32010   
rename T2304100 HH42010   
rename T2304200 HH52010   
rename T3280800 HW12012   
rename T3280900 HW22012   
rename T3281000 HW32012   
rename T3281100 HW42012   
rename T3281200 HW52012   
rename T3282300 HH12012   
rename T3282400 HH22012   
rename T3282500 HH32012   
rename T3282600 HH42012   
rename T3282700 HH52012  
rename T4254500 HH12014   
rename T4254600 HH22014  
rename T4254700 HH32014   
rename T4254800 HH42014  
rename T4254900 HH52014  
rename T4255000 HW12014   
rename T4255100 HW22014   
rename T4255200 HW32014   
rename T4255300 HW42014   
rename T4255400 HW52014 
rename T5227800 HH12016   
rename T5227900 HH22016   
rename T5228000 HH32016  
rename T5228100 HH42016   
rename T5228200 HH52016 
rename T5228300 HW12016  
rename T5228400 HW22016  
rename T5228500 HW32016   
rename T5228600 HW42016   
rename T5228700 HW52016    

	drop R*
	*rename R0000100 ID
	reshape i ID
	reshape j year
	reshape xij HW1 HW2 HW3 HW4 HW5 HH1 HH2 HH3 HH4 HH5 J91 J92 J93 J94 J95 HW9 HH9
	*reshape xi SAMP
	reshape long
	*gen dataset=original

** IDENTIFY HOURS WORKED ON EACH JOB (INCLUDING CPS JOB = JOB 9);
** missing values coded as -999, will not show up in final figures for hrs worked by job;

mvencode HH1 HH2 HH3 HH4 HH5 HH9 HW1 HW2 HW3 HW4 HW5 HW9, mv(-999)

* creating a new variable that will contain my hours calculation for each job;
	
forval i=1/5{
	gen HRS`i' = HW`i'
* replacing hours on job with hours including hours worked at home if the latter is greater than the former;
	replace HRS`i' = HH`i' if HH`i'>0 & HH`i'>HW`i'
* replacing maximum hours for any job at 96, for consistency across years;
*look why
	replace HRS`i' = 96 if HRS`i'>96
* replacing with HRS1 again for 1987 and before (since no separate hrs at home those years - just in case);
	replace HRS`i' = HW`i' if year<1988
}

	gen HRS9 = HW9
	replace HRS9 = HH9 if HH9>0 & HH9>HW9
	replace HRS9 = 96 if HRS9>96
* replacing with HRS1 again for 1987 and before (since no separate hrs at home those years - just in case);
	replace HRS9 = HW9 if year<1988
	
sort ID year

merge (ID year) using "$data/job_annual_file.dta"
drop _merge

	forval i=1/5{
	replace HRS`i' = HRS9 if year<1993 & J9`i'==1 & year~=1979
	replace HRS`i' = HRS9 if year==1979 & J9`i'==1 & HRS`i'<0 & HRS9>0
	replace OCC`i' = OCC9 if year<1993 & J9`i'==1 & year~=1979
	replace OCC`i'_occ1990 = OCC9_occ1990 if year<1993 & J9`i'==1 & year~=1979
	replace IND`i' = IND9 if year<1993 & J9`i'==1 & year~=1979
	replace IND`i' = IND9 if year==1979 & J91==1 & IND1==-999
	}

drop HW* HH* J*

sort ID year
save "$data/job_annual_file.dta", replace


******************************************************************************************************************************************************

*5. Permanent Job Identifier
	
infile using "$data/NLSY/job info/job_matching.DCT", clear

rename R0000100 ID 
rename W0012600 JOB11979 
rename W0012700 JOB21979 
rename W0012800 JOB31979 
rename W0012900 JOB41979 
rename W0013000 JOB51979 
rename W0079600 JOB11980 
rename W0079700 JOB21980 
rename W0079800 JOB31980 
rename W0079900 JOB41980 
rename W0080000 JOB51980 
rename W0105500 PJOB11980 
rename W0105600 PJOB21980 
rename W0105700 PJOB31980 
rename W0105800 PJOB41980 
rename W0105900 PJOB51980 
rename W0119500 JOB11981 
rename W0119600 JOB21981 
rename W0119700 JOB31981 
rename W0119800 JOB41981 
rename W0119900 JOB51981 
rename W0145400 PJOB11981 
rename W0145500 PJOB21981 
rename W0145600 PJOB31981 
rename W0145700 PJOB41981 
rename W0145800 PJOB51981 
rename W0159400 JOB11982 
rename W0159500 JOB21982 
rename W0159600 JOB31982 
rename W0159700 JOB41982 
rename W0159800 JOB51982 
rename W0185300 PJOB11982 
rename W0185400 PJOB21982 
rename W0185500 PJOB31982 
rename W0185600 PJOB41982 
rename W0185700 PJOB51982 
rename W0199300 JOB11983 
rename W0199400 JOB21983 
rename W0199500 JOB31983 
rename W0199600 JOB41983 
rename W0199700 JOB51983 
rename W0225200 PJOB11983 
rename W0225300 PJOB21983 
rename W0225400 PJOB31983 
rename W0225500 PJOB41983 
rename W0225600 PJOB51983 
rename W0239300 JOB11984 
rename W0239400 JOB21984 
rename W0239500 JOB31984 
rename W0239600 JOB41984 
rename W0239700 JOB51984 
rename W0265600 PJOB11984 
rename W0265700 PJOB21984 
rename W0265800 PJOB31984 
rename W0265900 PJOB41984 
rename W0266000 PJOB51984 
rename W0279700 JOB11985 
rename W0279800 JOB21985 
rename W0279900 JOB31985 
rename W0280000 JOB41985 
rename W0280100 JOB51985 
rename W0305600 PJOB11985 
rename W0305700 PJOB21985 
rename W0305800 PJOB31985 
rename W0305900 PJOB41985 
rename W0306000 PJOB51985 
rename W0319600 JOB11986 
rename W0319700 JOB21986 
rename W0319800 JOB31986 
rename W0319900 JOB41986 
rename W0320000 JOB51986 
rename W0345500 PJOB11986 
rename W0345600 PJOB21986 
rename W0345700 PJOB31986 
rename W0345800 PJOB41986 
rename W0345900 PJOB51986 
rename W0359500 JOB11987 
rename W0359600 JOB21987 
rename W0359700 JOB31987 
rename W0359800 JOB41987 
rename W0359900 JOB51987 
rename W0385400 PJOB11987 
rename W0385500 PJOB21987 
rename W0385600 PJOB31987 
rename W0385700 PJOB41987 
rename W0385800 PJOB51987 
rename W0399400 JOB11988 
rename W0399500 JOB21988 
rename W0399600 JOB31988 
rename W0399700 JOB41988 
rename W0399800 JOB51988 
rename W0425300 PJOB11988 
rename W0425400 PJOB21988 
rename W0425500 PJOB31988 
rename W0425600 PJOB41988 
rename W0425700 PJOB51988 
rename W0439400 JOB11989 
rename W0439500 JOB21989 
rename W0439600 JOB31989 
rename W0439700 JOB41989 
rename W0439800 JOB51989 
rename W0465700 PJOB11989 
rename W0465800 PJOB21989 
rename W0465900 PJOB31989 
rename W0466000 PJOB41989 
rename W0466100 PJOB51989 
rename W0479800 JOB11990 
rename W0479900 JOB21990 
rename W0480000 JOB31990 
rename W0480100 JOB41990 
rename W0480200 JOB51990 
rename W0505700 PJOB11990 
rename W0505800 PJOB21990 
rename W0505900 PJOB31990 
rename W0506000 PJOB41990 
rename W0506100 PJOB51990 
rename W0519700  JOB11991 
rename W0519800   JOB21991 
rename W0519900   JOB31991 
rename W0520000   JOB41991 
rename W0520100   JOB51991 
rename W0545600 PJOB11991 
rename W0545700 PJOB21991 
rename W0545800 PJOB31991 
rename W0545900 PJOB41991 
rename W0546000 PJOB51991 
rename W0559600 JOB11992 
rename W0559700   JOB21992 
rename W0559800 JOB31992 
rename W0559900 JOB41992 
rename W0560000 JOB51992 
rename W0585500 PJOB11992 
rename W0585600 PJOB21992 
rename W0585700 PJOB31992 
rename W0585800 PJOB41992 
rename W0585900 PJOB51992 
rename W0599500 JOB11993 
rename W0599600 JOB21993 
rename W0599700 JOB31993 
rename W0599800 JOB41993 
rename W0599900 JOB51993 
rename W0625400 PJOB11993 
rename W0625500 PJOB21993 
rename W0625600 PJOB31993 
rename W0625700 PJOB41993 
rename W0625800 PJOB51993 
rename W0639400 JOB11994 
rename W0639500 JOB21994 
rename W0639600 JOB31994 
rename W0639700 JOB41994 
rename W0639800 JOB51994 
rename W0665300 PJOB11994 
rename W0665400 PJOB21994 
rename W0665500 PJOB31994 
rename W0665600 PJOB41994 
rename W0665700 PJOB51994 
rename W0684600  JOB11996 
rename W0684700  JOB21996 
rename W0684800  JOB31996 
rename W0684900  JOB41996 
rename W0685000  JOB51996 
rename W0731700 PJOB11996 
rename W0731800 PJOB21996 
rename W0731900 PJOB31996 
rename W0732000 PJOB41996 
rename W0732100 PJOB51996 
rename W0756200 JOB11998 
rename W0756300 JOB21998 
rename W0756400 JOB31998 
rename W0756500 JOB41998 
rename W0756600 JOB51998 
rename W0802900 PJOB11998 
rename W0803000 PJOB21998 
rename W0803100 PJOB31998 
rename W0803200 PJOB41998 
rename W0803300 PJOB51998 
rename W0827300 JOB12000 
rename W0827400 JOB22000 
rename W0827500 JOB32000 
rename W0827600 JOB42000 
rename W0827700 JOB52000 
rename W0874000 PJOB12000 
rename W0874100 PJOB22000 
rename W0874200 PJOB32000 
rename W0874300 PJOB42000 
rename W0874400 PJOB52000 

rename W0898500 JOB12002 
rename W0898600 JOB22002
rename W0898700 JOB32002
rename W0898800 JOB42002
rename W0898900 JOB52002
rename W0931100 PJOB12002 
rename W0931200 PJOB22002
rename W0931300 PJOB32002
rename W0931400 PJOB42002
rename W0931500 PJOB52002 
rename W0948100 JOB12004 
rename W0948200 JOB22004
rename W0948300 JOB32004 
rename W0948400 JOB42004
rename W0948500 JOB52004
rename W0996600 PJOB12004
rename W0996700 PJOB22004
rename W0996800 PJOB32004
rename W0996900 PJOB42004
rename W0997000 PJOB52004
rename W1016200 JOB12006
rename W1016300 JOB22006
rename W1016400 JOB32006
rename W1016500 JOB42006
rename W1016600 JOB52006
rename W1058600 PJOB12006 
rename W1058700 PJOB22006
rename W1058800 PJOB32006
rename W1058900 PJOB42006
rename W1059000 PJOB52006
rename W1076200 JOB12008 
rename W1076300 JOB22008
rename W1076400 JOB32008 
rename W1076500 JOB42008 
rename W1076600 JOB52008
rename W1115500 PJOB12008
rename W1115600 PJOB22008
rename W1115700 PJOB32008
rename W1115800 PJOB42008 
rename W1115900 PJOB52008
rename W1135400 JOB12010
rename W1135500 JOB22010
rename W1135600 JOB32010
rename W1135700 JOB42010
rename W1135800 JOB52010
rename W1173000 PJOB12010
rename W1173100 PJOB22010
rename W1173200 PJOB32010
rename W1173300 PJOB42010
rename W1173400 PJOB52010
rename W1196100 JOB12012
rename W1196200 JOB22012
rename W1196300 JOB32012
rename W1196400 JOB42012
rename W1196500 JOB52012
rename W1248700 PJOB12012
rename W1248800 PJOB22012
rename W1248900 PJOB32012
rename W1249000 PJOB42012
rename W1249100 PJOB52012

rename W1320300 JOB12014 
rename W1320400 JOB22014 
rename W1320500 JOB32014  
rename W1320600 JOB42014  
rename W1320700 JOB52014 
rename W1324400 PJOB12014
rename W1324500 PJOB22014
rename W1324600 PJOB32014 
rename W1324700 PJOB42014 
rename W1324800 PJOB52014  
rename W1351200 JOB12016
rename W1351300 JOB22016
rename W1351400 JOB32016
rename W1351500 JOB42016
rename W1351600 JOB52016  
rename W1355900 PJOB12016
rename W1356000 PJOB22016
rename W1356100 PJOB32016
rename W1356200 PJOB42016
rename W1356300 PJOB52016

reshape i ID
reshape j year
reshape xij JOB1 JOB2 JOB3 JOB4 JOB5 PJOB1 PJOB2 PJOB3 PJOB4 PJOB5
reshape long

sort ID year
merge ID year using "$data/_individualfile_y.dta", keep(NI)

** Drop all rounds where individual was not interviewed;
drop if NI==1

gen JN1 = 0
gen JN2 = 0
gen JN3 = 0
gen JN4 = 0
gen JN5 = 0

replace JN1 = JOB1 if year==1979
replace JN2 = JOB2 if year==1979
replace JN3 = JOB3 if year==1979
replace JN4 = JOB4 if year==1979
replace JN5 = JOB5 if year==1979

local i = 1980

*matches jobs through the years
while `i'<=2016 {
	if year==1995|year==1997|year==1999 |year==2001 |year==2005 |year==2007 |year==2009 |year==2011 {
			local i = `i'+1
	}
	di `i'
	local j=1
	while `j'<=5{
		replace JN1 = JN`j'[_n-1] if PJOB1 == `j' & year==`i' & JOB`j'[_n-1]>0
		replace JN1 = JOB1 if PJOB1 == `j' & year==`i' & JOB`j'[_n-1]<0

		replace JN2 = JN`j'[_n-1] if PJOB2 == `j' & year==`i' & JOB`j'[_n-1]>0
		replace JN2 = JOB2 if PJOB2 == `j' & year==`i' & JOB`j'[_n-1]<0
		
		replace JN3 = JN`j'[_n-1] if PJOB3 == `j' & year==`i' & JOB`j'[_n-1]>0
		replace JN3 = JOB3 if PJOB3 == `j' & year==`i' & JOB`j'[_n-1]<0
		
		replace JN4 = JN`j'[_n-1] if PJOB4 == `j' & year==`i' & JOB`j'[_n-1]>0
		replace JN4 = JOB4 if PJOB4 == `j' & year==`i' & JOB`j'[_n-1]<0	
		
		replace JN5 = JN`j'[_n-1] if PJOB5 == `j' & year==`i' & JOB`j'[_n-1]>0
		replace JN5 = JOB5 if PJOB5 == `j' & year==`i' & JOB`j'[_n-1]<0

		local j=`j'+1
	}

	replace JN1 = JOB1 if PJOB1 <0 & year==`i'
	replace JN2 = JOB2 if PJOB2 <0 & year==`i'
	replace JN3 = JOB3 if PJOB3 <0 & year==`i'
	replace JN4 = JOB4 if PJOB4 <0 & year==`i'
	replace JN5 = JOB5 if PJOB5 <0 & year==`i'


	local i = `i'+1
}

label var JN1 "Permanent job number, Job1"
label var JOB1 "Annual job number, Job1"
label var PJOB1 "Job at previous interview, Job1"

keep ID year JN* JOB* PJOB*
sort ID year

merge (ID year) using "$data/job_annual_file.dta"
keep if _m==3
drop _merge

save "$data/job_annual_file.dta", replace

******************************************************************************************************************************************************

*5. keeping only data for cross-sectional sample

sort ID
merge ID using "$data/_individualfile.dta", keep(SAMP)
keep if _m==3
drop _m

******************************************************************************************************************************************************
******************************************************************************************************************************************************

**cleaning

gen aux=1

sort ID year
sort ID year
bysort ID: gen pid=1 if year==1979
bysort ID: replace pid=pid[_n-1]+1 if year>year[_n-1]


**identifies distint job spells
forval i=1/5{
gen aux_spell`i'=1 if year==1979

replace aux_spell`i'=aux_spell`i'[_n-1] if JN`i'==JN`i'[_n-1] & ID==ID[_n-1]

replace aux_spell`i'=aux_spell`i'[_n-1]+1 if JN`i'~=JN`i'[_n-1] & pid~=1 & ID==ID[_n-1]
replace aux_spell`i'=aux_spell`i'[_n-1] if aux_spell`i'==. & JN`i'==JN`i'[_n-1] & ID==ID[_n-1]

replace aux_spell`i'=aux_spell`i'[_n-1]+1 if JN`i'~=JN`i'[_n-1] & pid~=1 & ID==ID[_n-1]
replace aux_spell`i'=aux_spell`i'[_n-1] if aux_spell`i'==. & JN`i'==JN`i'[_n-1] & ID==ID[_n-1]

replace aux_spell`i'=aux_spell`i'[_n-1]+1 if JN`i'~=JN`i'[_n-1] & pid~=1 & ID==ID[_n-1]
replace aux_spell`i'=aux_spell`i'[_n-1] if aux_spell`i'==. & JN`i'==JN`i'[_n-1] & ID==ID[_n-1]

replace aux_spell`i'=aux_spell`i'[_n-1]+1 if JN`i'~=JN`i'[_n-1] & pid~=1 & ID==ID[_n-1]
replace aux_spell`i'=aux_spell`i'[_n-1] if aux_spell`i'==. & JN`i'==JN`i'[_n-1] & ID==ID[_n-1]

replace aux_spell`i'=aux_spell`i'[_n-1]+1 if JN`i'~=JN`i'[_n-1] & pid~=1 & ID==ID[_n-1]
replace aux_spell`i'=aux_spell`i'[_n-1] if aux_spell`i'==. & JN`i'==JN`i'[_n-1] & ID==ID[_n-1]

replace aux_spell`i'=aux_spell`i'[_n-1]+1 if JN`i'~=JN`i'[_n-1] & pid~=1 & ID==ID[_n-1]
replace aux_spell`i'=aux_spell`i'[_n-1] if aux_spell`i'==. & JN`i'==JN`i'[_n-1] & ID==ID[_n-1]

replace aux_spell`i'=aux_spell`i'[_n-1]+1 if JN`i'~=JN`i'[_n-1] & pid~=1 & ID==ID[_n-1]
replace aux_spell`i'=aux_spell`i'[_n-1] if aux_spell`i'==. & JN`i'==JN`i'[_n-1] & ID==ID[_n-1]

replace aux_spell`i'=aux_spell`i'[_n-1]+1 if JN`i'~=JN`i'[_n-1] & pid~=1 & ID==ID[_n-1]
replace aux_spell`i'=aux_spell`i'[_n-1] if aux_spell`i'==. & JN`i'==JN`i'[_n-1] & ID==ID[_n-1]
}








forval i=1/5{
bysort ID JN`i' aux_spell`i': egen spell`i'=sum(aux) 
replace spell`i'=-4 if JN`i'==-4
 
bysort ID JN`i' OCC`i' aux_spell`i': egen aux_occ=sum(aux) 
replace aux_occ=-4 if JN`i'==-4
replace aux_occ=-4 if OCC`i'==-4

sort ID year
bysort ID: gen pid_2=1 if aux_spell`i'~=aux_spell`i'[_n-1]
bysort ID: replace pid_2=pid_2[_n-1]+1 if aux_spell`i'==aux_spell`i'[_n-1]

egen max`i' = max(aux_occ), by(ID aux_spell`i')

sort ID year
gen OCCN`i'=OCC`i' if aux_occ==max`i'  & pid_2 ==1
gen OCCN`i'_occ1990=OCC`i'_occ1990 if aux_occ==max`i' & pid_2 ==1

forval j=1/19{
	replace OCCN`i'=OCC`i'[_n+`j'] if aux_occ~=max`i' & aux_occ[_n+`j']==max`i' & pid_2==1 & aux_spell`i'==aux_spell`i'[_n+`j']
	replace OCCN`i'_occ1990=OCC`i'_occ1990[_n+`j'] if aux_occ~=max`i' & aux_occ[_n+`j']==max`i' & pid_2==1 & aux_spell`i'==aux_spell`i'[_n+`j']

	}	
sort ID year JN`i' pid_2
replace OCCN`i'=OCCN`i'[_n-1] if aux_spell`i'==aux_spell`i'[_n-1] & pid_2~=1 & ID==ID[_n-1]
replace OCCN`i'_occ1990=OCCN`i'_occ1990[_n-1] if aux_spell`i'==aux_spell`i'[_n-1] & pid_2~=1 & ID==ID[_n-1]
drop aux_occ pid_2
}

replace OCCN1_occ1990=208 if ID==654 & year==1992
replace OCCN1_occ1990=208 if ID==654 & year==1991
replace OCCN1_occ1990=208 if ID==3023 & year==1998
replace OCCN1_occ1990=208 if ID==3023 & year==2000
replace OCCN1_occ1990=208 if ID==3023 & year==1996
replace OCCN1_occ1990=549 if ID==9313 & year==1991
replace OCCN1_occ1990=549 if ID==9313 & year==1992
replace OCCN1_occ1990=207 if ID==6647 & year==1991
replace OCCN1_occ1990=207 if ID==6647 & year==1992
replace OCCN2_occ1990=451 if ID==3245 & year==1990
replace OCCN2_occ1990=451 if ID==3245 & year==1991


drop max*

forval i=1/5{

bysort ID JN`i' aux_spell`i': egen aux_ind=sum(aux) 
replace aux_ind=-4 if JN`i'==-4
replace aux_ind=-4 if OCC`i'==-4

sort ID year
bysort ID: gen pid_2=1 if aux_spell`i'~=aux_spell`i'[_n-1]
bysort ID: replace pid_2=pid_2[_n-1]+1 if aux_spell`i'==aux_spell`i'[_n-1]

egen max`i' = max(aux_ind), by(ID aux_spell`i')

sort ID year
gen INDN`i'=IND`i' if aux_ind==max`i'  & pid_2 ==1

forval j=1/19{
	replace INDN`i'=IND`i'[_n+`j'] if aux_ind~=max`i' & aux_ind[_n+`j']==max`i' & pid_2==1 & aux_spell`i'==aux_spell`i'[_n+`j']
	}	
sort ID year JN1 pid
replace INDN`i'=INDN`i'[_n-1] if aux_spell`i'==aux_spell`i'[_n-1] & pid_2~=1 & ID==ID[_n-1]
drop aux_ind pid_2

}

drop pid* max*


******************************************************************************************************************************************************
	
** This creates indicator variables for jobs 1-5 noting that there are missing hours despite there being a job identified;

gen IND_1 = 1 if HRS1<0 & JOB1>100
gen IND_2 = 1 if HRS2<0 & JOB2>100
gen IND_3 = 1 if HRS3<0 & JOB3>100
gen IND_4 = 1 if HRS4<0 & JOB4>100
gen IND_5 = 1 if HRS5<0 & JOB5>100

mvencode IND_1 IND_2 IND_3 IND_4 IND_5, mv(0)

sort ID year

drop OCC1* OCC2* OCC3* OCC4* OCC5* OCC9*  
drop IND1 IND2 IND3 IND4 IND5 IND9
drop aux aux_spell* spell* HRS9


forval i=1/5{
rename OCCN`i' OCC1970`i'
rename OCCN`i'_occ1990 OCC`i'
}

** Turn this into a long file, with unit of observation at the ID*year*job level;

	gen ident = ID*1000+year
	reshape clear
	reshape i ident 
	reshape j NUMBER
	reshape xi ID year
	reshape xij JOB JN lhrp HRS IND_ PJOB OCC OCC1970 INDN 
	
	reshape long

	drop ident
	keep if JOB>=100


sort ID JN year

foreach var in OCC INDN {
bysort ID JN: egen `var'_mode=mode(`var')
gen aux`var'=1 if `var'_mode~=`var'
}

sort ID JN year

bysort ID JN: egen year_min=min(year)
bysort ID JN: egen year_max=max(year)

bysort ID JN: egen NUMBER_min=min(NUMBER)
bysort ID JN: egen NUMBER_max=max(NUMBER)

foreach var in OCC INDN {
gen `var'_aux_min=`var' if year_min==year & `var'_mode==. 
gen `var'_aux_max=`var' if year_max==year & `var'_mode==. 

gen `var'_aux_min_2=`var' if NUMBER_min==NUMBER & `var'_mode==. 
gen `var'_aux_max_2=`var' if NUMBER_max==NUMBER & `var'_mode==. 

bysort ID JN: egen `var'_mean_min=mean(`var'_aux_min)
bysort ID JN: egen `var'_mean_max=mean(`var'_aux_max)

bysort ID JN: egen `var'_mean_min_2=mean(`var'_aux_min_2)
bysort ID JN: egen `var'_mean_max_2=mean(`var'_aux_max_2)


gen `var'_new=`var'_mode if `var'_mode~=.
replace `var'_new=`var'_mean_min if `var'_mode==. & `var'_mean_min>0 
replace `var'_new=`var'_mean_max if `var'_mode==. & `var'_mean_max>0 & `var'_mean_min<0 

}

rename OCC OCC_old
rename OCC_new OCC

rename INDN INDN_old
rename INDN_new INDN

keep JOB JN lhrp HRS IND_ PJOB OCC OCC1970 INDN year ID 

sort ID JOB

save "$data/_jobfile.dta", replace

******************************************************************************************************************************************************
******************************************************************************************************************************************************

// Clean up and remove temporary files

! rm "$data/job_annual_file.dta"		


