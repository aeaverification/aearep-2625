
* "Wage Cyclicality and Labor Market Sorting" // Master file

clear all

global data "/Users/liuqianyi/Desktop/LDI Replication Lab/150581/data/"
global do "/Users/liuqianyi/Desktop/LDI Replication Lab/150581/code/"
global output "/Users/liuqianyi/Desktop/LDI Replication Lab/150581/output/"

do "$do/data.do"  
do "$do/figures_tables.do"
do "$do/figures_tables_app.do"


