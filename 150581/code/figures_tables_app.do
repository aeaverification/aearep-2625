
*note: this file reproduces figures and tables in the online appendix

*-----------------------------------------------------------------
*	 Tables
*-----------------------------------------------------------------

*-----------------------------------------------------------------
*	 Table A.1 
*-----------------------------------------------------------------

use "$data/occ_perc_weighted", clear

gen occupation_agg=1 if (occ1990dd>=3 & occ1990dd<=37) | (occ1990dd>=43 & occ1990dd<=200) | (occ1990dd>=203 & occ1990dd<=235) | (occ1990dd>=243 & occ1990dd<=258) | (occ1990dd>=417 & occ1990dd<=423)
replace occupation_agg=2 if (occ1990dd>=303 & occ1990dd<=389) | (occ1990dd>=274 & occ1990dd<=283)
replace occupation_agg=3 if  (occ1990dd>=405 & occ1990dd<=408) | (occ1990dd==415) | (occ1990dd>=425 & occ1990dd<=427) | (occ1990dd>=433 & occ1990dd<=444) | (occ1990dd>=445 & occ1990dd<=447) | (occ1990dd>=448 & occ1990dd<=455) | (occ1990dd>=457 & occ1990dd<=458) | (occ1990dd>=459 & occ1990dd<=467) | (occ1990dd==468) | (occ1990dd>=469 & occ1990dd<=472)
replace occupation_agg=4  if (occ1990dd>=628 & occ1990dd<=699)
replace occupation_agg=5  if (occ1990dd>=703 & occ1990dd<=799)
replace occupation_agg=6  if (occ1990dd>=803 & occ1990dd<=889) |  (occ1990dd>=558 & occ1990dd<=599) | (occ1990dd>=503 & occ1990dd<=549) |  (occ1990dd>=614 & occ1990dd<=617) | (occ1990dd>=473 & occ1990dd<=475) | (occ1990dd>=479 & occ1990dd<=498)
mat m=J(6,4,.)
forvalues i=1(1)6 {
sum math_perc_occ_w if occupation_agg==`i'
mat m[`i',1]=round(r(mean),.1)
sum verbal_perc_occ_w if occupation_agg==`i'
mat m[`i',2]=round(r(mean),.1)
sum social_perc_occ_w if occupation_agg==`i'
mat m[`i',3]=round(r(mean),.1)
sum technical_perc_occ_w if occupation_agg==`i'
mat m[`i',4]=round(r(mean),.1)	
}
frmttable using "$output/table_a1.tex", replace tex statmat(m) ctitle("", "Math", "Verbal", "Social", "Technical")  rtitle("Manag./Professional/Financial sales occs" \ "Admin. support and Retail sales occs" \ "Low-skill services occs" \ "Precision production and Craft occs" \ "Machine operators, Assemblers and Inspectors occs" \ "Transp./Construction/Mechanics/Mining/Agric. occs")

*-----------------------------------------------------------------
*	 Table A.2 
*-----------------------------------------------------------------

use "$data/data_analysis", clear

mat m=J(4,4,.)
pwcorr  math_perc verbal_perc social_perc technical_perc math_perc_occ verbal_perc_occ social_perc_occ technical_perc_occ if num_miss==0 & HOURSM>=75 & age>=20 
mat m_1 =r(C)
forvalues i=1(1)4{
mat m[`i',1]=round(m_1[5,`i'],.01)
mat m[`i',2]=round(m_1[6,`i'],.01)
mat m[`i',3]=round(m_1[7,`i'],.01)
mat m[`i',4]=round(m_1[8,`i'],.01)
}
frmttable using "$output/table_a2.tex", replace tex statmat(m) ctitle("", "Math", "Verbal", "Social", "Technical") rtitle("Math" \"Verbal"\"Social"\"Technical") 

*-----------------------------------------------------------------
*	 Tables C.1 - C.3
*-----------------------------------------------------------------

replace unempl=unempl/100

*-----------------------------------------------------------------
*	 Tables C.1 
*-----------------------------------------------------------------

*3months
reghdfe lhrp2 unempl c.unempl#i.dummy1_alt3 c.unempl#i.dummy2_alt3 i.dummy1_alt3 i.dummy2_alt3 mismatch1w age agesq i.dummy_educ c.time##c.time time_trend i.month if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store C_robs3

reghdfe  lhrp2 unempl c.unempl#i.dummy1_alt3 c.unempl#i.dummy2_alt3  c.mismatch1w#c.unempl c.unempl#i.dummy1_alt3#c.mismatch1w c.unempl#i.dummy2_alt3#c.mismatch1w c.mismatch1w#i.dummy1_alt3 c.mismatch1w#i.dummy2_alt3 mismatch1w i.dummy1_alt3 i.dummy2_alt3  age agesq i.dummy_educ c.time##c.time time_trend i.month if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store E_robs3

reghdfe  lhrp2 unempl c.unempl#i.dummy1_alt3 c.unempl#i.dummy2_alt3  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1_alt3#c.mismatch1w_pos c.unempl#i.dummy2_alt3#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1_alt3 c.mismatch1w_pos#i.dummy2_alt3 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1_alt3#c.mismatch1w_neg c.unempl#i.dummy2_alt3#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1_alt3 c.mismatch1w_neg#i.dummy2_alt3 c.mismatch1w_neg mismatch1w_pos i.dummy1_alt3 i.dummy2_alt3  age agesq i.dummy_educ c.time##c.time time_trend i.month if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store F_robs3

*recalls
reghdfe lhrp2 unempl c.unempl#i.dummy1_alt4 c.unempl#i.dummy2_alt4 i.dummy1_alt4 i.dummy2_alt4 mismatch1w age agesq i.dummy_educ c.time##c.time time_trend i.month if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store C_robs4

reghdfe  lhrp2 unempl c.unempl#i.dummy1_alt4 c.unempl#i.dummy2_alt4  c.mismatch1w#c.unempl c.unempl#i.dummy1_alt4#c.mismatch1w c.unempl#i.dummy2_alt4#c.mismatch1w c.mismatch1w#i.dummy1_alt4 c.mismatch1w#i.dummy2_alt4 mismatch1w i.dummy1_alt4 i.dummy2_alt4  age agesq i.dummy_educ c.time##c.time time_trend i.month if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store E_robs4

reghdfe  lhrp2 unempl c.unempl#i.dummy1_alt4 c.unempl#i.dummy2_alt4  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1_alt4#c.mismatch1w_pos c.unempl#i.dummy2_alt4#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1_alt4 c.mismatch1w_pos#i.dummy2_alt4 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1_alt4#c.mismatch1w_neg c.unempl#i.dummy2_alt4#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1_alt4 c.mismatch1w_neg#i.dummy2_alt4 c.mismatch1w_neg mismatch1w_pos i.dummy1_alt4 i.dummy2_alt4  age agesq i.dummy_educ c.time##c.time time_trend i.month if  HOURSM>=75  & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store F_robs4

label var dummy1_alt3 "EE'\textsubscript{\textit{i,t}}"
label var dummy2_alt3 "UE\textsubscript{\textit{i,t}}"
label var mismatch1w "\$ m_{i,t} $"
label var mismatch1w_pos "\$ m^+_{i,t} $"
label var mismatch1w_neg "\$ m^-_{i,t} $"
label var unempl "U\textsubscript{\textit{i,t}}"
esttab C_robs3 E_robs3 F_robs3 C_robs4 E_robs4 F_robs4 using "$output/table_c1.tex",  replace substitute(\_ _, =1 , ref. ) label depvars nomti se(3) b(3) star(* 0.1 ** 0.05 *** 0.01) noobs keep(unempl 1.dummy1_alt3#c.unempl 1.dummy2_alt3#c.unempl  mismatch1w c.mismatch1w#c.unempl 1.dummy1_alt3#c.unempl#c.mismatch1w 1.dummy2_alt3#c.unempl#c.mismatch1w 1.dummy2_alt3#c.unempl#c.mismatch1w_pos 1.dummy2_alt3#c.unempl#c.mismatch1w_neg 1.dummy1_alt3#c.unempl#c.mismatch1w_pos 1.dummy1_alt3#c.unempl#c.mismatch1w_neg c.mismatch1w_pos#c.unempl c.mismatch1w_neg#c.unempl) nonotes nobaselevels  alignment(S S S S S S) stats(N r2, fmt(0 3) layout("\multicolumn{1}{c}{@}" "\multicolumn{1}{S}{@}") labels(`"Observations"' `"Adjusted \$R^{2}$"')) noconstant nobaselevels mgroups("> 3 months" "Recalls", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span})) rename(1.dummy1_alt4 1.dummy1_alt3 1.dummy2_alt4 1.dummy2_alt3 1.dummy1_alt4#c.unempl 1.dummy1_alt3#c.unempl 1.dummy2_alt4#c.unempl 1.dummy2_alt3#c.unempl 1.dummy2_alt4#c.unempl#c.mismatch1w 1.dummy2_alt3#c.unempl#c.mismatch1w 1.dummy2_alt4#c.unempl#c.mismatch1w_pos 1.dummy2_alt3#c.unempl#c.mismatch1w_pos 1.dummy1_alt4#c.unempl#c.mismatch1w_pos 1.dummy1_alt3#c.unempl#c.mismatch1w_pos 1.dummy2_alt4#c.unempl#c.mismatch1w_neg 1.dummy2_alt3#c.unempl#c.mismatch1w_neg 1.dummy1_alt4#c.unempl#c.mismatch1w_neg 1.dummy1_alt3#c.unempl#c.mismatch1w_neg)



*-----------------------------------------------------------------
*	 Tables C.2
*-----------------------------------------------------------------

*occupational skill requirements
reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2 i.dummy1 i.dummy2 mismatch1w age agesq i.dummy_educ c.time##c.time time_trend i.month *_perc_occ_weighted if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store C_robs6

reghdfe  lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w#c.unempl c.unempl#i.dummy1#c.mismatch1w c.unempl#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 mismatch1w i.dummy1 i.dummy2  age agesq i.dummy_educ c.time##c.time time_trend i.month *_perc_occ_weighted if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store E_robs6

reghdfe  lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos c.unempl#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg c.unempl#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend i.month *_perc_occ_weighted if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store F_robs6

*occupational tenure
reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2 i.dummy1 i.dummy2 mismatch1w age agesq i.dummy_educ c.time##c.time time_trend i.month time_occ time_occ_sq time_occ_cb if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store C_robs7

reghdfe  lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w#c.unempl c.unempl#i.dummy1#c.mismatch1w c.unempl#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 mismatch1w i.dummy1 i.dummy2  age agesq i.dummy_educ c.time##c.time time_trend i.month time_occ time_occ_sq time_occ_cb  if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store E_robs7

reghdfe  lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos c.unempl#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg c.unempl#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend i.month time_occ time_occ_sq time_occ_cb if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store F_robs7

*cumulative mismatch
reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2 i.dummy1 i.dummy2 mismatch1w age agesq i.dummy_educ c.time##c.time time_trend i.month cum2 if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store C_robs9

reghdfe  lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w#c.unempl c.unempl#i.dummy1#c.mismatch1w c.unempl#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 mismatch1w i.dummy1 i.dummy2  age agesq i.dummy_educ c.time##c.time time_trend i.month cum2  if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store E_robs9

reghdfe  lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos c.unempl#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg c.unempl#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend i.month cum2 if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store F_robs9

label var dummy "NH\textsubscript{\textit{i.t}}"
label var dummy1 "EE'\textsubscript{\textit{i,t}}"
label var dummy2 "UE\textsubscript{\textit{i,t}}"
esttab  C_robs6 E_robs6 F_robs6 C_robs7 E_robs7 F_robs7 C_robs9 E_robs9 F_robs9 using "$output/table_c2.tex", replace substitute(\_ _, =1 , ref. ) label depvars nomti se(3) b(3) star(* 0.1 ** 0.05 *** 0.01) noobs keep(unempl 1.dummy1#c.unempl 1.dummy2#c.unempl  mismatch1w c.mismatch1w#c.unempl 1.dummy1#c.unempl#c.mismatch1w 1.dummy2#c.unempl#c.mismatch1w c.mismatch1w_pos#c.unempl 1.dummy2#c.unempl#c.mismatch1w_pos c.mismatch1w_neg#c.unempl  1.dummy2#c.unempl#c.mismatch1w_neg 1.dummy1#c.unempl#c.mismatch1w_pos 1.dummy1#c.unempl#c.mismatch1w_neg c.mismatch1w_neg#c.unempl c.mismatch1w_pos#c.unempl) nonotes nobaselevels  alignment(S S S S S S) stats(N r2, fmt(0 3) layout("\multicolumn{1}{c}{@}" "\multicolumn{1}{S}{@}") labels(`"Observations"' `"Adjusted \$R^{2}$"')) noconstant nobaselevels interaction(" $\cdot$ ") mgroups("Occ. Req." "Occ. Tenure" "Cum. Mismatch" , pattern(1 0 0 1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))


*-----------------------------------------------------------------
*	 Tables C.3
*-----------------------------------------------------------------

*skill-specific weights
reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2 i.dummy1 i.dummy2 mismatch1w_w age agesq i.dummy_educ c.time##c.time time_trend i.month if  HOURSM>=75  & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store C_robs5

reghdfe  lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_w#c.unempl c.unempl#i.dummy1#c.mismatch1w_w c.unempl#i.dummy2#c.mismatch1w_w c.mismatch1w_w#i.dummy1 c.mismatch1w_w#i.dummy2  mismatch1w_w i.dummy1 i.dummy2  age agesq i.dummy_educ c.time##c.time time_trend i.month if  HOURSM>=75  & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store E_robs5

gen mismatch1w_pos_w=math_weight_w*aux4_math_pos_weighted+verbal_weight_w*aux4_verbal_pos_weighted+tech_weight_w*aux4_technical_pos_weighted+social_weight_w*aux4_social_pos_weighted
gen mismatch1w_neg_w=math_weight_w*abs(aux4_math_weighted)+verbal_weight_w*abs(aux4_verbal_weighted)+tech_weight_w*abs(aux4_technical_weighted)+social_weight_w*abs(aux4_social_weighted)
reghdfe  lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos_w#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos_w c.unempl#i.dummy2#c.mismatch1w_pos_w c.mismatch1w_pos_w#i.dummy1 c.mismatch1w_pos_w#i.dummy2 c.mismatch1w_neg_w#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg_w c.unempl#i.dummy2#c.mismatch1w_neg_w c.mismatch1w_neg_w#i.dummy1 c.mismatch1w_neg_w#i.dummy2 c.mismatch1w_neg_w mismatch1w_pos_w i.dummy1 i.dummy2  age agesq i.dummy_educ c.time##c.time time_trend i.month if JOB>=100 & JOB~=.  & HOURSM>=75 & gender==0 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store F_robs5

*regional unemployment rate 
replace unempl_region=unempl_region/100
reghdfe lhrp2 unempl_region c.unempl_region#i.dummy1 c.unempl_region#i.dummy2 i.dummy1 i.dummy2 mismatch1w age agesq i.dummy_educ c.time##c.time c.time_trend#i.regionfe i.month if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store C_robs8

reghdfe  lhrp2 unempl_region c.unempl_region#i.dummy1 c.unempl_region#i.dummy2  c.mismatch1w#c.unempl_region c.unempl_region#i.dummy1#c.mismatch1w c.unempl_region#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 mismatch1w i.dummy1 i.dummy2  age agesq i.dummy_educ c.time##c.time c.time_trend#i.regionfe  i.month   if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store E_robs8

reghdfe  lhrp2 unempl_region c.unempl_region#i.dummy1 c.unempl_region#i.dummy2  c.mismatch1w_pos#c.unempl_region c.unempl_region#i.dummy1#c.mismatch1w_pos c.unempl_region#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl_region c.unempl_region#i.dummy1#c.mismatch1w_neg c.unempl_region#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time c.time_trend#i.regionfe  i.month if HOURSM>=75  & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store F_robs8

label var mismatch1w_w "\$ m_{i,t} $"
label var mismatch1w_pos_w "\$ m^+_{i,t} $"
label var mismatch1w_neg_w "\$ m^-_{i,t} $"
esttab C_robs5 E_robs5 F_robs5 C_robs8 E_robs8 F_robs8 using "$output/table_c3.tex", replace  substitute(\_ _, =1 , ref. ) label depvars nomti se(3) b(3) star(* 0.1 ** 0.05 *** 0.01) noobs keep(unempl 1.dummy1#c.unempl 1.dummy2#c.unempl  mismatch1w_w c.mismatch1w_w#c.unempl 1.dummy1#c.unempl#c.mismatch1w_w 1.dummy2#c.unempl#c.mismatch1w_w 1.dummy2#c.unempl#c.mismatch1w_pos_w 1.dummy2#c.unempl#c.mismatch1w_neg_w 1.dummy1#c.unempl#c.mismatch1w_pos_w 1.dummy1#c.unempl#c.mismatch1w_neg_w  c.mismatch1w_pos_w#c.unempl c.mismatch1w_neg_w#c.unempl ) nonotes nobaselevels  alignment(S S S S S S) stats(N r2, fmt(0 3) layout("\multicolumn{1}{c}{@}" "\multicolumn{1}{S}{@}") labels(`"Observations"' `"Adjusted \$R^{2}$"')) noconstant nobaselevels mgroups("Weighted Mis." "Regional Unemp.", pattern(1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span})) rename(unempl_region unempl 1.dummy1#c.unempl_region 1.dummy1#c.unempl 1.dummy2#c.unempl_region 1.dummy2#c.unempl  mismatch1w mismatch1w_w c.mismatch1w#c.unempl_region c.mismatch1w_w#c.unempl 1.dummy1#c.unempl_region#c.mismatch1w 1.dummy1#c.unempl#c.mismatch1w_w 1.dummy2#c.unempl_region#c.mismatch1w 1.dummy2#c.unempl#c.mismatch1w_w 1.dummy2#c.unempl_region#c.mismatch1w_pos 1.dummy2#c.unempl#c.mismatch1w_pos_w 1.dummy2#c.unempl_region#c.mismatch1w_neg  1.dummy2#c.unempl#c.mismatch1w_neg_w 1.dummy1#c.unempl_region#c.mismatch1w_pos 1.dummy1#c.unempl#c.mismatch1w_pos_w 1.dummy1#c.unempl_region#c.mismatch1w_neg 1.dummy1#c.unempl#c.mismatch1w_neg_w c.mismatch1w_neg#c.unempl_region c.mismatch1w_neg_w#c.unempl c.mismatch1w_pos#c.unempl_region c.mismatch1w_pos_w#c.unempl) interaction(" $\cdot$ ")


*-----------------------------------------------------------------
*	 Figures
*-----------------------------------------------------------------

replace unempl=unempl*100

*-----------------------------------------------------------------
*	 Figure B.1
*-----------------------------------------------------------------

local c=20
local c_1=19

pctile quant_pos=mismatch1w_pos if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_pos)
pctile quant_neg=mismatch1w_neg if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_neg)

sum mismatch1w_neg  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local a_1=r(mean)
local a_2=r(p25)
local a_3=r(p50)
local a_4=r(p75)

sum mismatch1w_pos  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local b_1=r(mean)
local b_2=r(p25)
local b_3=r(p50)
local b_4=r(p75)

mkmat quant_pos if quant_pos~=., matrix(x_pos)
mkmat quant_neg if quant_neg~=., matrix(x_neg)

*coefidence interval: 95%
scalar sig1 = 0.05	 


forvalues j=1(1)4{
foreach var in stayers ee ue{

	gen b_`var'_pos_`j'=.
	gen up_`var'_pos_`j' =.
	gen dn_`var'_pos_`j' =.
	
	gen b_`var'_neg_`j'=.
	gen up_`var'_neg_`j' =.
	gen dn_`var'_neg_`j' =.
}
}


reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos c.unempl#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg c.unempl#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if  HOURSM>=75  & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)

forvalues j=1(1)4{
	forvalues i=1(1)`c_1'{
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a_`j''
	replace b_stayers_pos_`j'=r(estimate)*-100 if _n == `i'
	replace up_stayers_pos_`j'=r(ub)*-100 if _n == `i'
	replace dn_stayers_pos_`j'=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy1#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a_`j''+_b[unempl#1.dummy1#mismatch1w_neg]*`a_`j''
	replace b_ee_pos_`j'=r(estimate)*-100 if _n == `i'
	replace up_ee_pos_`j'=r(ub)*-100 if _n == `i'
	replace dn_ee_pos_`j'=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy2#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a_`j''+_b[unempl#1.dummy2#mismatch1w_neg]*`a_`j''
	replace b_ue_pos_`j'=r(estimate)*-100 if _n == `i'
	replace up_ue_pos_`j'=r(ub)*-100 if _n == `i'
	replace dn_ue_pos_`j'=r(lb)*-100 if _n == `i'
	
	
	
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*`b_`j'' +_b[unempl#mismatch1w_neg]*x_neg[`i',1]
	replace b_stayers_neg_`j'=r(estimate)*-100 if _n == `i'
	replace up_stayers_neg_`j'=r(ub)*-100 if _n == `i'
	replace dn_stayers_neg_`j'=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*`b_`j''+_b[unempl#1.dummy1#mismatch1w_pos]*`b_`j''+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy1#mismatch1w_neg]*x_neg[`i',1]
	replace b_ee_neg_`j'=r(estimate)*-100 if _n == `i'
	replace up_ee_neg_`j'=r(ub)*-100 if _n == `i'
	replace dn_ee_neg_`j'=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*`b_`j''+_b[unempl#1.dummy2#mismatch1w_pos]*`b_`j''+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy2#mismatch1w_neg]*x_neg[`i',1]
	replace b_ue_neg_`j'=r(estimate)*-100 if _n == `i'
	replace up_ue_neg_`j'=r(ub)*-100 if _n == `i'
	replace dn_ue_neg_`j'=r(lb)*-100 if _n == `i'
	
}
}


twoway  (scatter b_stayers_pos_1  values_quant_pos, mcolor(blue))  (scatter b_stayers_pos_2  values_quant_pos, mcolor(blue)) (scatter b_stayers_pos_3  values_quant_pos,mcolor(blue))  (scatter b_stayers_pos_4  values_quant_pos,mcolor(blue)), xtitle("Overqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(1 2 3 4) label(1 "Mean (baseline)") label(2 "P25") label(3 "P50") label(4 "P75")  col(1) ring(0) position(11) bmargin(medsmall)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Stayers") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity") 
graph export "$output/figure_b1a_left.pdf",  replace

twoway (scatter b_ue_pos_1  values_quant_pos, mcolor(red))  (scatter b_ue_pos_2  values_quant_pos, mcolor(red)) (scatter b_ue_pos_3  values_quant_pos,mcolor(red))  (scatter b_ue_pos_4  values_quant_pos,mcolor(red)), xtitle("Overqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground))legend(order(1 2 3 4) label(1 "Mean (baseline)") label(2 "P25") label(3 "P50") label(4 "P75")   col(1) ring(0) position(11) bmargin(medsmall)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("New Hires from Unemployment") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity") 
graph export "$output/figure_b1a_right.pdf", replace

twoway (scatter b_stayers_neg_1  values_quant_neg, mcolor(blue))  (scatter b_stayers_neg_2  values_quant_neg, mcolor(blue)) (scatter b_stayers_neg_3  values_quant_neg,mcolor(blue))  (scatter b_stayers_neg_4  values_quant_neg,mcolor(blue)), xtitle("Underqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground))legend(order(1 2 3 4) label(1 "Mean (baseline)") label(2 "P25") label(3 "P50") label(4 "P75")  col(1) ring(0) position(11) bmargin(medsmall)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Stayers") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity") 
graph export "$output/figure_b1b_left.pdf", replace

twoway (scatter b_ue_neg_1  values_quant_neg, mcolor(red))  (scatter b_ue_neg_2  values_quant_neg, mcolor(red)) (scatter b_ue_neg_3  values_quant_neg,mcolor(red))  (scatter b_ue_neg_4  values_quant_neg,mcolor(red)), xtitle("Underqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground))legend(order(1 2 3 4) label(1 "Mean (baseline)") label(2 "P25") label(3 "P50") label(4 "P75")  col(1) ring(0) position(11) bmargin(medsmall)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("New Hires from Unemployment") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity") 
graph export "$output/figure_b1b_right.pdf",  replace


*-----------------------------------------------------------------
*	 Figure E.1
*-----------------------------------------------------------------

gen bad_state=1 if unempl>=6.5
replace bad_state=0 if unempl<6.5

*separation into ue
sort ID year month
gen separation_ue=1 if  emp_unemp[_n+1]==1 & ID==ID[_n+1]
replace separation_ue=0 if  emp_unemp[_n+1]==. & ID==ID[_n+1] & JN~=. & JN[_n+1]~=. & dummy[_n+1]==0
replace separation_ue=0 if  emp_unemp[_n+1]==. & ID==ID[_n+1] & JN~=. & JN[_n+1]~=. & dummy1[_n+1]==1
replace separation_ue=0 if  JN~=. & year==2012 & month==12
replace separation_ue=0 if  emp_unemp[_n+1]==.  & JN~=. & JN[_n+1]==. & ID==ID[_n+1] 

cloglog separation_ue i.bad_state##c.lntime mismatch1w time_occ i.race i.dummy_educ i.occupation_agg i.industry i.month initial_agecb  if ave_hours>=75 & JN~=. & initial_age>=20, cluster(ID) robust
margins,at(bad_state=0 lntime==(0 .69314718 1.0986123 1.3862944 1.6094379 1.79176 1.9459101 2.0794415 2.1972246 2.3025851 2.3978953 2.48491 2.89037 3.17805 3.4012 3.58352 3.7376696 3.8712 3.98898 4.09434 4.18965 4.27667 4.35667 4.43081 4.49981 4.56435 4.62497) (means) _all) post 
matrix M=r(table)
matrix Z_b=M[1,1..27]'
matrix Z_lb=M[5,1..27]'
matrix Z_ub=M[6,1..27]'
svmat Z_b, name(b_c_low1)
svmat Z_lb, name(lb_c_low1)
svmat Z_ub, name(ub_c_low1)

cloglog separation_ue i.bad_state##c.lntime mismatch1w time_occ i.race i.dummy_educ i.occupation_agg i.industry i.month initial_agecb  if ave_hours>=75 & JN~=. & initial_age>=20, cluster(ID) robust
margins,at(bad_state=1 lntime==(0 .69314718 1.0986123 1.3862944 1.6094379 1.79176 1.9459101 2.0794415 2.1972246 2.3025851 2.3978953 2.48491 2.89037 3.17805 3.4012 3.58352 3.7376696 3.8712 3.98898 4.09434 4.18965 4.27667 4.35667 4.43081 4.49981 4.56435 4.62497) (means) _all) post 
matrix M=r(table)
matrix Z_b=M[1,1..27]'
matrix Z_lb=M[5,1..27]'
matrix Z_ub=M[6,1..27]'
svmat Z_b, name(b_c_high1)
svmat Z_lb, name(lb_c_high1)
svmat Z_ub, name(ub_c_high1)

sort ID year month
gen time_hazard=1 if _n==1
replace time_hazard=2 if _n==2
replace time_hazard=3 if _n==3
replace time_hazard=4 if _n==4
replace time_hazard=5 if _n==5
replace time_hazard=6 if _n==6
replace time_hazard=7 if _n==7
replace time_hazard=8 if _n==8
replace time_hazard=9 if _n==9
replace time_hazard=10 if _n==10
replace time_hazard=11 if _n==11
replace time_hazard=12 if _n==12
replace time_hazard=18 if _n==13
replace time_hazard=24 if _n==14
replace time_hazard=30 if _n==15
replace time_hazard=36 if _n==16
replace time_hazard=42 if _n==17
replace time_hazard=48 if _n==18
replace time_hazard=54 if _n==19
replace time_hazard=60 if _n==20
replace time_hazard=66 if _n==21
replace time_hazard=72 if _n==22
replace time_hazard=78 if _n==23
replace time_hazard=84 if _n==24
replace time_hazard=90 if _n==25
replace time_hazard=96 if _n==26
replace time_hazard=102 if _n==27

gen ratio=b_c_high1/b_c_low1
twoway   (line ratio time_hazard if time_hazard<=60, lcolor(blue) lwidth(thick)), xtitle("Tenure (months)") ylabel(0(0.5)2) scheme(plotplainblind) ytitle("")
graph export "$output/figure_e1.pdf", replace

*-----------------------------------------------------------------
*	 Figure D.1
*-----------------------------------------------------------------

do "$do/appendix/1_sample_criteria_mil1"
do "$do/appendix/2_sample_criteria_mil4"
do "$do/appendix/3_sample_criteria_olf15"
do "$do/appendix/4_sample_criteria_olf20"
do "$do/appendix/5_sample_criteria_full"

**clean up and remove temporary files
! rm "$data/appendix/sample_selection.dta"	
! rm "$data/appendix/occ_weights.dta"	
! rm "$data/appendix/occ_perc_weighted.dta"	
! rm "$data/ONET/onet_categories_asvab.dta"	
! rm "$data/ONET/onet_categories_social.dta"	
! rm "$data/data_month.dta"	


*********************************************************************************

use "$data/data_analysis", clear

local c=20
local c_1=19

pctile quant_pos=mismatch1w_pos if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_pos)
pctile quant_neg=mismatch1w_neg if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_neg)
pctile quant=mismatch1w if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant)

sum mismatch1w_neg  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local a=r(mean)

sum mismatch1w_pos  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local b=r(mean)

mkmat quant if quant~=., matrix(x)
mkmat quant_pos if quant_pos~=., matrix(x_pos)
mkmat quant_neg if quant_neg~=., matrix(x_neg)

*coefidence interval: 95%
scalar sig1 = 0.05	 

foreach var in stayers ee ue{

	gen b_`var'=.
	gen up_`var' =.
	gen dn_`var' =.
	
	gen b_`var'_pos=.
	gen up_`var'_pos =.
	gen dn_`var'_pos =.
	
	gen b_`var'_neg=.
	gen up_`var'_neg =.
	gen dn_`var'_neg =.

}
	
*coefidence interval: 95%
scalar sig1 = 0.05	 

*baseline regression
reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w#c.unempl c.unempl#i.dummy1#c.mismatch1w c.unempl#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 c.mismatch1w i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if  HOURSM>=75  & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)


forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#mismatch1w]*x[`i',1]
replace b_stayers=r(estimate)*-100 if _n == `i'
replace up_stayers=r(ub)*-100 if _n == `i'
replace dn_stayers=r(lb)*-100 if _n == `i'

lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy1#mismatch1w]*x[`i',1]
replace b_ee=r(estimate)*-100 if _n == `i'
replace up_ee=r(ub)*-100 if _n == `i'
replace dn_ee=r(lb)*-100 if _n == `i'

lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy2#mismatch1w]*x[`i',1]
replace b_ue=r(estimate)*-100 if _n == `i'
replace up_ue=r(ub)*-100 if _n == `i'
replace dn_ue=r(lb)*-100 if _n == `i'

}

reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos c.unempl#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg c.unempl#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)


	forvalues i=1(1)`c_1'{
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'
	replace b_stayers_pos=r(estimate)*-100 if _n == `i'
	replace up_stayers_pos=r(ub)*-100 if _n == `i'
	replace dn_stayers_pos=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy1#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy1#mismatch1w_neg]*`a'
	replace b_ee_pos=r(estimate)*-100 if _n == `i'
	replace up_ee_pos=r(ub)*-100 if _n == `i'
	replace dn_ee_pos=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy2#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy2#mismatch1w_neg]*`a'
	replace b_ue_pos=r(estimate)*-100 if _n == `i'
	replace up_ue_pos=r(ub)*-100 if _n == `i'
	replace dn_ue_pos=r(lb)*-100 if _n == `i'
	
	
	
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*`b' +_b[unempl#mismatch1w_neg]*x_neg[`i',1]
	replace b_stayers_neg=r(estimate)*-100 if _n == `i'
	replace up_stayers_neg=r(ub)*-100 if _n == `i'
	replace dn_stayers_neg=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy1#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy1#mismatch1w_neg]*x_neg[`i',1]
	replace b_ee_neg=r(estimate)*-100 if _n == `i'
	replace up_ee_neg=r(ub)*-100 if _n == `i'
	replace dn_ee_neg=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy2#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy2#mismatch1w_neg]*x_neg[`i',1]
	replace b_ue_neg=r(estimate)*-100 if _n == `i'
	replace up_ue_neg=r(ub)*-100 if _n == `i'
	replace dn_ue_neg=r(lb)*-100 if _n == `i'
	
}

keep b_* up_* dn_* values_quant quant

keep if values_quant~=.
sort values_quant
save "$data/appendix/data_graph_robs", replace

*********************************************************************************

use "$data/appendix/data_analysis_mil1", clear

local c=20
local c_1=19

*to make graph graphs

pctile quant_pos=mismatch1w_pos if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_pos)
pctile quant_neg=mismatch1w_neg if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_neg)
pctile quant=mismatch1w if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant)

sum mismatch1w_neg  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local a=r(mean)

sum mismatch1w_pos  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local b=r(mean)

mkmat quant if quant~=., matrix(x)
mkmat quant_pos if quant_pos~=., matrix(x_pos)
mkmat quant_neg if quant_neg~=., matrix(x_neg)

*coefidence interval: 95%
scalar sig1 = 0.05	 

foreach var in stayers ee ue{

	gen b0_`var'=.
	gen up0_`var' =.
	gen dn0_`var' =.
	
	gen b0_`var'_pos=.
	gen up0_`var'_pos =.
	gen dn0_`var'_pos =.
	
	gen b0_`var'_neg=.
	gen up0_`var'_neg =.
	gen dn0_`var'_neg =.
}

*baseline regression
reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w#c.unempl c.unempl#i.dummy1#c.mismatch1w c.unempl#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 c.mismatch1w i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if  HOURSM>=75  & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)



forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#mismatch1w]*x[`i',1]
replace b0_stayers=r(estimate)*-100 if _n == `i'
replace up0_stayers=r(ub)*-100 if _n == `i'
replace dn0_stayers=r(lb)*-100 if _n == `i'

lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy1#mismatch1w]*x[`i',1]
replace b0_ee=r(estimate)*-100 if _n == `i'
replace up0_ee=r(ub)*-100 if _n == `i'
replace dn0_ee=r(lb)*-100 if _n == `i'

lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy2#mismatch1w]*x[`i',1]
replace b0_ue=r(estimate)*-100 if _n == `i'
replace up0_ue=r(ub)*-100 if _n == `i'
replace dn0_ue=r(lb)*-100 if _n == `i'

}

reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos c.unempl#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg c.unempl#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)


	forvalues i=1(1)`c_1'{
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'
	replace b0_stayers_pos=r(estimate)*-100 if _n == `i'
	replace up0_stayers_pos=r(ub)*-100 if _n == `i'
	replace dn0_stayers_pos=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy1#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy1#mismatch1w_neg]*`a'
	replace b0_ee_pos=r(estimate)*-100 if _n == `i'
	replace up0_ee_pos=r(ub)*-100 if _n == `i'
	replace dn0_ee_pos=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy2#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy2#mismatch1w_neg]*`a'
	replace b0_ue_pos=r(estimate)*-100 if _n == `i'
	replace up0_ue_pos=r(ub)*-100 if _n == `i'
	replace dn0_ue_pos=r(lb)*-100 if _n == `i'
	
	
	
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*`b' +_b[unempl#mismatch1w_neg]*x_neg[`i',1]
	replace b0_stayers_neg=r(estimate)*-100 if _n == `i'
	replace up0_stayers_neg=r(ub)*-100 if _n == `i'
	replace dn0_stayers_neg=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy1#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy1#mismatch1w_neg]*x_neg[`i',1]
	replace b0_ee_neg=r(estimate)*-100 if _n == `i'
	replace up0_ee_neg=r(ub)*-100 if _n == `i'
	replace dn0_ee_neg=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy2#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy2#mismatch1w_neg]*x_neg[`i',1]
	replace b0_ue_neg=r(estimate)*-100 if _n == `i'
	replace up0_ue_neg=r(ub)*-100 if _n == `i'
	replace dn0_ue_neg=r(lb)*-100 if _n == `i'
	
}

keep b0_* up0_* dn0_* values_quant quant

keep if values_quant~=.
sort values_quant
merge values_quant using "$data/appendix/data_graph_robs"
drop _merge
sort values_quant
save "$data/appendix/data_graph_robs", replace

*********************************************************************************

use "$data/appendix/data_analysis_mil4", clear

local c=20
local c_1=19

*to make graph graphs

pctile quant_pos=mismatch1w_pos if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_pos)
pctile quant_neg=mismatch1w_neg if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_neg)
pctile quant=mismatch1w if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant)

sum mismatch1w_neg  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local a=r(mean)

sum mismatch1w_pos  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local b=r(mean)

mkmat quant if quant~=., matrix(x)
mkmat quant_pos if quant_pos~=., matrix(x_pos)
mkmat quant_neg if quant_neg~=., matrix(x_neg)

*coefidence interval: 95%
scalar sig1 = 0.05	 

foreach var in stayers ee ue{

	gen b4_`var'=.
	gen up4_`var' =.
	gen dn4_`var' =.
	
	gen b4_`var'_pos=.
	gen up4_`var'_pos =.
	gen dn4_`var'_pos =.
	
	gen b4_`var'_neg=.
	gen up4_`var'_neg =.
	gen dn4_`var'_neg =.
}


*baseline regression
reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w#c.unempl c.unempl#i.dummy1#c.mismatch1w c.unempl#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 c.mismatch1w i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)


forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#mismatch1w]*x[`i',1]
replace b4_stayers=r(estimate)*-100 if _n == `i'
replace up4_stayers=r(ub)*-100 if _n == `i'
replace dn4_stayers=r(lb)*-100 if _n == `i'

lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy1#mismatch1w]*x[`i',1]
replace b4_ee=r(estimate)*-100 if _n == `i'
replace up4_ee=r(ub)*-100 if _n == `i'
replace dn4_ee=r(lb)*-100 if _n == `i'

lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy2#mismatch1w]*x[`i',1]
replace b4_ue=r(estimate)*-100 if _n == `i'
replace up4_ue=r(ub)*-100 if _n == `i'
replace dn4_ue=r(lb)*-100 if _n == `i'

}

reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos c.unempl#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg c.unempl#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)


	forvalues i=1(1)`c_1'{
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'
	replace b4_stayers_pos=r(estimate)*-100 if _n == `i'
	replace up4_stayers_pos=r(ub)*-100 if _n == `i'
	replace dn4_stayers_pos=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy1#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy1#mismatch1w_neg]*`a'
	replace b4_ee_pos=r(estimate)*-100 if _n == `i'
	replace up4_ee_pos=r(ub)*-100 if _n == `i'
	replace dn4_ee_pos=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy2#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy2#mismatch1w_neg]*`a'
	replace b4_ue_pos=r(estimate)*-100 if _n == `i'
	replace up4_ue_pos=r(ub)*-100 if _n == `i'
	replace dn4_ue_pos=r(lb)*-100 if _n == `i'
	
	
	
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*`b' +_b[unempl#mismatch1w_neg]*x_neg[`i',1]
	replace b4_stayers_neg=r(estimate)*-100 if _n == `i'
	replace up4_stayers_neg=r(ub)*-100 if _n == `i'
	replace dn4_stayers_neg=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy1#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy1#mismatch1w_neg]*x_neg[`i',1]
	replace b4_ee_neg=r(estimate)*-100 if _n == `i'
	replace up4_ee_neg=r(ub)*-100 if _n == `i'
	replace dn4_ee_neg=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy2#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy2#mismatch1w_neg]*x_neg[`i',1]
	replace b4_ue_neg=r(estimate)*-100 if _n == `i'
	replace up4_ue_neg=r(ub)*-100 if _n == `i'
	replace dn4_ue_neg=r(lb)*-100 if _n == `i'
	
}


keep b4_* up4_* dn4_* values_quant quant

keep if values_quant~=.
sort values_quant
merge values_quant using "$data/appendix/data_graph_robs"
drop _merge
sort values_quant
save "$data/appendix/data_graph_robs", replace

*********************************************************************************

use "$data/appendix/data_analysis_olf15", clear

local c=20
local c_1=19

pctile quant_pos=mismatch1w_pos if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_pos)
pctile quant_neg=mismatch1w_neg if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_neg)
pctile quant=mismatch1w if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant)

sum mismatch1w_neg  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local a=r(mean)

sum mismatch1w_pos  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local b=r(mean)

mkmat quant if quant~=., matrix(x)
mkmat quant_pos if quant_pos~=., matrix(x_pos)
mkmat quant_neg if quant_neg~=., matrix(x_neg)

*coefidence interval: 95%
scalar sig1 = 0.05	 

foreach var in stayers ee ue{

	gen b15_`var'=.
	gen up15_`var' =.
	gen dn15_`var' =.
	
	gen b15_`var'_pos=.
	gen up15_`var'_pos =.
	gen dn15_`var'_pos =.
	
	gen b15_`var'_neg=.
	gen up15_`var'_neg =.
	gen dn15_`var'_neg =.
}


*baseline regression
reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w#c.unempl c.unempl#i.dummy1#c.mismatch1w c.unempl#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 c.mismatch1w i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)



forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#mismatch1w]*x[`i',1]
replace b15_stayers=r(estimate)*-100 if _n == `i'
replace up15_stayers=r(ub)*-100 if _n == `i'
replace dn15_stayers=r(lb)*-100 if _n == `i'

lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy1#mismatch1w]*x[`i',1]
replace b15_ee=r(estimate)*-100 if _n == `i'
replace up15_ee=r(ub)*-100 if _n == `i'
replace dn15_ee=r(lb)*-100 if _n == `i'

lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy2#mismatch1w]*x[`i',1]
replace b15_ue=r(estimate)*-100 if _n == `i'
replace up15_ue=r(ub)*-100 if _n == `i'
replace dn15_ue=r(lb)*-100 if _n == `i'

}

reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos c.unempl#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg c.unempl#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)


	forvalues i=1(1)`c_1'{
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'
	replace b15_stayers_pos=r(estimate)*-100 if _n == `i'
	replace up15_stayers_pos=r(ub)*-100 if _n == `i'
	replace dn15_stayers_pos=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy1#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy1#mismatch1w_neg]*`a'
	replace b15_ee_pos=r(estimate)*-100 if _n == `i'
	replace up15_ee_pos=r(ub)*-100 if _n == `i'
	replace dn15_ee_pos=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy2#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy2#mismatch1w_neg]*`a'
	replace b15_ue_pos=r(estimate)*-100 if _n == `i'
	replace up15_ue_pos=r(ub)*-100 if _n == `i'
	replace dn15_ue_pos=r(lb)*-100 if _n == `i'
	
	
	
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*`b' +_b[unempl#mismatch1w_neg]*x_neg[`i',1]
	replace b15_stayers_neg=r(estimate)*-100 if _n == `i'
	replace up15_stayers_neg=r(ub)*-100 if _n == `i'
	replace dn15_stayers_neg=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy1#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy1#mismatch1w_neg]*x_neg[`i',1]
	replace b15_ee_neg=r(estimate)*-100 if _n == `i'
	replace up15_ee_neg=r(ub)*-100 if _n == `i'
	replace dn15_ee_neg=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy2#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy2#mismatch1w_neg]*x_neg[`i',1]
	replace b15_ue_neg=r(estimate)*-100 if _n == `i'
	replace up15_ue_neg=r(ub)*-100 if _n == `i'
	replace dn15_ue_neg=r(lb)*-100 if _n == `i'
	
}



keep b15_* up15_* dn15_* values_quant quant

keep if values_quant~=.
sort values_quant
merge values_quant using "$data/appendix/data_graph_robs"
drop _merge
sort values_quant
save "$data/appendix/data_graph_robs", replace


*********************************************************************************

use "$data/appendix/data_analysis_olf20", clear

local c=20
local c_1=19

pctile quant_pos=mismatch1w_pos if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_pos)
pctile quant_neg=mismatch1w_neg if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_neg)
pctile quant=mismatch1w if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant)

sum mismatch1w_neg  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local a=r(mean)

sum mismatch1w_pos  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local b=r(mean)

mkmat quant if quant~=., matrix(x)
mkmat quant_pos if quant_pos~=., matrix(x_pos)
mkmat quant_neg if quant_neg~=., matrix(x_neg)

*coefidence interval: 95%
scalar sig1 = 0.05	 

foreach var in stayers ee ue{

	gen b20_`var'=.
	gen up20_`var' =.
	gen dn20_`var' =.
	
	gen b20_`var'_pos=.
	gen up20_`var'_pos =.
	gen dn20_`var'_pos =.
	
	gen b20_`var'_neg=.
	gen up20_`var'_neg =.
	gen dn20_`var'_neg =.
}


*baseline regression
reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w#c.unempl c.unempl#i.dummy1#c.mismatch1w c.unempl#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 c.mismatch1w i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)


forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#mismatch1w]*x[`i',1]
replace b20_stayers=r(estimate)*-100 if _n == `i'
replace up20_stayers=r(ub)*-100 if _n == `i'
replace dn20_stayers=r(lb)*-100 if _n == `i'

lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy1#mismatch1w]*x[`i',1]
replace b20_ee=r(estimate)*-100 if _n == `i'
replace up20_ee=r(ub)*-100 if _n == `i'
replace dn20_ee=r(lb)*-100 if _n == `i'

lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy2#mismatch1w]*x[`i',1]
replace b20_ue=r(estimate)*-100 if _n == `i'
replace up20_ue=r(ub)*-100 if _n == `i'
replace dn20_ue=r(lb)*-100 if _n == `i'

}

reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos c.unempl#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg c.unempl#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)


	forvalues i=1(1)`c_1'{
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'
	replace b20_stayers_pos=r(estimate)*-100 if _n == `i'
	replace up20_stayers_pos=r(ub)*-100 if _n == `i'
	replace dn20_stayers_pos=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy1#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy1#mismatch1w_neg]*`a'
	replace b20_ee_pos=r(estimate)*-100 if _n == `i'
	replace up20_ee_pos=r(ub)*-100 if _n == `i'
	replace dn20_ee_pos=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy2#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy2#mismatch1w_neg]*`a'
	replace b20_ue_pos=r(estimate)*-100 if _n == `i'
	replace up20_ue_pos=r(ub)*-100 if _n == `i'
	replace dn20_ue_pos=r(lb)*-100 if _n == `i'
	
	
	
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*`b' +_b[unempl#mismatch1w_neg]*x_neg[`i',1]
	replace b20_stayers_neg=r(estimate)*-100 if _n == `i'
	replace up20_stayers_neg=r(ub)*-100 if _n == `i'
	replace dn20_stayers_neg=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy1#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy1#mismatch1w_neg]*x_neg[`i',1]
	replace b20_ee_neg=r(estimate)*-100 if _n == `i'
	replace up20_ee_neg=r(ub)*-100 if _n == `i'
	replace dn20_ee_neg=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy2#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy2#mismatch1w_neg]*x_neg[`i',1]
	replace b20_ue_neg=r(estimate)*-100 if _n == `i'
	replace up20_ue_neg=r(ub)*-100 if _n == `i'
	replace dn20_ue_neg=r(lb)*-100 if _n == `i'
	
}


keep b20_* up20_* dn20_* values_quant quant

keep if values_quant~=.
sort values_quant
merge values_quant using "$data/appendix/data_graph_robs"
drop _merge
sort values_quant
save "$data/appendix/data_graph_robs", replace

*********************************************************************************

use "$data/appendix/data_analysis_full", clear

local c=20
local c_1=19

*to make graph graphs

pctile quant_pos=mismatch1w_pos if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_pos)
pctile quant_neg=mismatch1w_neg if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_neg)
pctile quant=mismatch1w if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant)

sum mismatch1w_neg  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local a=r(mean)

sum mismatch1w_pos  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local b=r(mean)

mkmat quant if quant~=., matrix(x)
mkmat quant_pos if quant_pos~=., matrix(x_pos)
mkmat quant_neg if quant_neg~=., matrix(x_neg)

*coefidence interval: 95%
scalar sig1 = 0.05	 

foreach var in stayers ee ue{

	gen b21_`var'=.
	gen up21_`var' =.
	gen dn21_`var' =.
	
	gen b21_`var'_pos=.
	gen up21_`var'_pos =.
	gen dn21_`var'_pos =.
	
	gen b21_`var'_neg=.
	gen up21_`var'_neg =.
	gen dn21_`var'_neg =.
}


reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w#c.unempl c.unempl#i.dummy1#c.mismatch1w c.unempl#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 c.mismatch1w i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)


forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#mismatch1w]*x[`i',1]
replace b21_stayers=r(estimate)*-100 if _n == `i'
replace up21_stayers=r(ub)*-100 if _n == `i'
replace dn21_stayers=r(lb)*-100 if _n == `i'

lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy1#mismatch1w]*x[`i',1]
replace b21_ee=r(estimate)*-100 if _n == `i'
replace up21_ee=r(ub)*-100 if _n == `i'
replace dn21_ee=r(lb)*-100 if _n == `i'

lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy2#mismatch1w]*x[`i',1]
replace b21_ue=r(estimate)*-100 if _n == `i'
replace up21_ue=r(ub)*-100 if _n == `i'
replace dn21_ue=r(lb)*-100 if _n == `i'

}

reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos c.unempl#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg c.unempl#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)


	forvalues i=1(1)`c_1'{
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'
	replace b21_stayers_pos=r(estimate)*-100 if _n == `i'
	replace up21_stayers_pos=r(ub)*-100 if _n == `i'
	replace dn21_stayers_pos=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy1#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy1#mismatch1w_neg]*`a'
	replace b21_ee_pos=r(estimate)*-100 if _n == `i'
	replace up21_ee_pos=r(ub)*-100 if _n == `i'
	replace dn21_ee_pos=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy2#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy2#mismatch1w_neg]*`a'
	replace b21_ue_pos=r(estimate)*-100 if _n == `i'
	replace up21_ue_pos=r(ub)*-100 if _n == `i'
	replace dn21_ue_pos=r(lb)*-100 if _n == `i'
	
	
	
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*`b' +_b[unempl#mismatch1w_neg]*x_neg[`i',1]
	replace b21_stayers_neg=r(estimate)*-100 if _n == `i'
	replace up21_stayers_neg=r(ub)*-100 if _n == `i'
	replace dn21_stayers_neg=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy1#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy1#mismatch1w_neg]*x_neg[`i',1]
	replace b21_ee_neg=r(estimate)*-100 if _n == `i'
	replace up21_ee_neg=r(ub)*-100 if _n == `i'
	replace dn21_ee_neg=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy2#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy2#mismatch1w_neg]*x_neg[`i',1]
	replace b21_ue_neg=r(estimate)*-100 if _n == `i'
	replace up21_ue_neg=r(ub)*-100 if _n == `i'
	replace dn21_ue_neg=r(lb)*-100 if _n == `i'
	
}


keep b21_* up21_* dn21_* values_quant quant

keep if values_quant~=.
sort values_quant
merge values_quant using "$data/appendix/data_graph_robs"
drop _merge
sort values_quant
save "$data/appendix/data_graph_robs", replace

*********************************************************************************

use "$data/appendix/data_graph_robs", replace

twoway  (rcap up_stayers dn_stayers values_quant,  color(blue) lcolor(blue) lcolor(%30)) (scatter b_stayers  values_quant, mcolor(blue)) (scatter b0_stayers  values_quant,mcolor(blue))  (scatter b4_stayers  values_quant,mcolor(blue)) (scatter b20_stayers  values_quant,mcolor(blue)) (scatter b15_stayers  values_quant,mcolor(blue)) (scatter b21_stayers  values_quant,mcolor(blue)), xtitle("Mismatch (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 6 5 7) label(2 "Baseline") label(3 "Ever in military") label(4 "> 4 years in military") label(5 "> 15 years olf") label(6 "> 20 years olf")  label(7 "> 1820h  in 1979")  col(3) position(6) bmargin(medsmall) col(2)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Stayers") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity") 
graph export "$output/figure_d1a_left.pdf", replace

twoway  (rcap up_ee dn_ee values_quant,  color(green) lcolor(green) lcolor(%30)) (scatter b_ee  values_quant, mcolor(green))  (scatter b0_ee  values_quant, mcolor(green)) (scatter b4_ee  values_quant,mcolor(green)) (scatter b20_ee  values_quant,mcolor(green)) (scatter b15_ee  values_quant,mcolor(green)) (scatter b21_ee  values_quant,mcolor(green)), xtitle("Mismatch (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 6 5 7) label(2 "Baseline") label(3 "Any year in military") label(4 "> 4 years in military") label(5 "> 20  labor force") label(6 "> 15 labor force")  label(7 "> 1820h  in 1979")  col(3) position(6) bmargin(medsmall)) ylabel(-2(2)6) ytitle("Wage semi-elasticity")  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Switchers") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind)
graph export "$output/figure_d1a_right.pdf",  replace

twoway  (rcap up_ue dn_ue values_quant,  color(red) lcolor(red) lcolor(%30)) (scatter b_ue values_quant, mcolor(red)) (scatter b0_ue  values_quant,mcolor(red)) (scatter b4_ue  values_quant,mcolor(red)) (scatter b20_ue  values_quant,mcolor(red)) (scatter b15_ue  values_quant,mcolor(red)) (scatter b21_ue  values_quant,mcolor(red)), xtitle("Mismatch (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 6 5 7) label(2 "Baseline") label(3 "Ever in military") label(4 "> 4 years in military") label(5 "> 20 years olf") label(6 "> 15 years olf")   label(7 "> 1820h  in 1979") col(3) position(6) bmargin(medsmall)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("New Hires from Unemployment") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity")
graph export "$output/figure_d1a_middle.pdf",  replace


*********************************************************************************

twoway  (rcap up_stayers_pos dn_stayers_pos values_quant,  color(blue) lcolor(blue) lcolor(%30)) (scatter b_stayers_pos  values_quant, mcolor(blue)) (scatter b0_stayers_pos  values_quant,mcolor(blue))  (scatter b4_stayers_pos  values_quant,mcolor(blue)) (scatter b20_stayers_pos  values_quant,mcolor(blue)) (scatter b15_stayers_pos  values_quant,mcolor(blue)) (scatter b21_stayers_pos  values_quant,mcolor(blue)), xtitle("Overqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 6 5 7) label(2 "Baseline") label(3 "Ever in military") label(4 "> 4 years in military") label(5 "> 15 years olf") label(6 "> 20 years olf") label(7 "> 1820h  in 1979") col(3) position(6) bmargin(medsmall) col(2)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Stayers") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity") 
graph export "$output/figure_d1b_left.pdf", replace

twoway  (rcap up_ee_pos dn_ee_pos values_quant,  color(green) lcolor(green) lcolor(%30)) (scatter b_ee_pos  values_quant, mcolor(green))  (scatter b0_ee_pos  values_quant, mcolor(green)) (scatter b4_ee_pos  values_quant,mcolor(green)) (scatter b20_ee_pos  values_quant,mcolor(green)) (scatter b15_ee_pos  values_quant,mcolor(green)) (scatter b21_ee_pos  values_quant,mcolor(green)), xtitle("Overqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 6 5) label(2 "Baseline") label(3 "Ever in military") label(4 "> 4 years in military") label(5 "> 20  years olf") label(6 "> 15 years olf") label(7 "> 1820h  in 1979") col(3) position(6) bmargin(medsmall)) ylabel(-2(2)6) ytitle("Wage semi-elasticity")  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Switchers") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind)
graph export "$output/figure_d1b_right.pdf",  replace

twoway  (rcap up_ue_pos dn_ue_pos values_quant,  color(red) lcolor(red) lcolor(%30)) (scatter b_ue_pos values_quant, mcolor(red)) (scatter b0_ue_pos  values_quant,mcolor(red)) (scatter b4_ue_pos  values_quant,mcolor(red)) (scatter b20_ue_pos  values_quant,mcolor(red)) (scatter b15_ue_pos  values_quant,mcolor(red)) (scatter b21_ue_pos  values_quant,mcolor(red)), xtitle("Overqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 6 5 7) label(2 "Baseline") label(3 "Ever in military") label(4 "> 4 years in military") label(5 "> 20 years olf") label(6 "> 15 years olf")   label(7 "> 1820h  in 1979")  col(3) position(6) bmargin(medsmall)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("New Hires from Unemployment") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity")
graph export "$output/figure_d1b_middle.pdf",  replace


*********************************************************************************

twoway  (rcap up_stayers_neg dn_stayers_neg values_quant,  color(blue) lcolor(blue) lcolor(%30)) (scatter b_stayers_neg  values_quant, mcolor(blue)) (scatter b0_stayers_neg  values_quant,mcolor(blue))  (scatter b4_stayers_neg  values_quant,mcolor(blue)) (scatter b20_stayers_neg  values_quant,mcolor(blue)) (scatter b15_stayers_neg  values_quant,mcolor(blue)) (scatter b21_stayers_neg  values_quant,mcolor(blue)), xtitle("Underqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 6 5 7) label(2 "Baseline") label(3 "Ever in military") label(4 "> 4 years in military") label(5 "> 15 years olf") label(6 "> 20 years olf")  label(7 "> 1820h  in 1979")  col(3) position(6) bmargin(medsmall) col(2)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Stayers") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity") 
graph export "$output/figure_d1c_left.pdf", replace

twoway  (rcap up_ee_neg dn_ee_neg values_quant,  color(green) lcolor(green) lcolor(%30)) (scatter b_ee_neg  values_quant, mcolor(green))  (scatter b0_ee_neg  values_quant, mcolor(green)) (scatter b4_ee_neg  values_quant,mcolor(green)) (scatter b20_ee_neg  values_quant,mcolor(green)) (scatter b15_ee_neg  values_quant,mcolor(green)) (scatter b21_ee_neg  values_quant,mcolor(green)), xtitle("Underqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 6 5 7) label(2 "Baseline") label(3 "Ever in military") label(4 "> 4 years in military") label(5 "> 20  labor force") label(6 "> 15 years olf")  label(7 "> 1820h  in 1979") col(3) position(6) bmargin(medsmall)) ylabel(-2(2)6) ytitle("Wage semi-elasticity")  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Switchers") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind)
graph export "$output/figure_d1c_right.pdf",  replace

twoway  (rcap up_ue_neg dn_ue_neg values_quant,  color(red) lcolor(red) lcolor(%30)) (scatter b_ue_neg values_quant, mcolor(red)) (scatter b0_ue_neg  values_quant,mcolor(red)) (scatter b4_ue_neg  values_quant,mcolor(red)) (scatter b20_ue_neg  values_quant,mcolor(red)) (scatter b15_ue_neg  values_quant,mcolor(red))  (scatter b21_ue_neg  values_quant,mcolor(red)), xtitle("Underqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 6 5 7) label(2 "Baseline") label(3 "Ever in military") label(4 "> 4 years in military") label(5 "> 20 years olf") label(6 "> 15 years olf")  label(7 "> 1820h  in 1979") col(3) position(6) bmargin(medsmall)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("New Hires from Unemployment") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity")
graph export "$output/figure_d1c_middle.pdf",  replace


*********************************************************************************

**clean up and remove temporary files

! rm "$data/appendix/data_analysis_full.dta"	
! rm "$data/appendix/data_analysis_olf20.dta"	
! rm "$data/appendix/data_analysis_olf15.dta"	
! rm "$data/appendix/data_analysis_mil1.dta"	
! rm "$data/appendix/data_analysis_mil4.dta"	
! rm "$data/occ_perc_weighted.dta"	



