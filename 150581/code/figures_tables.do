
*note: this file reproduces tables, figures and in-text numbers that do not derive directly from the tables and tables.

*-----------------------------------------------------------------
*	 Tables
*-----------------------------------------------------------------

*-----------------------------------------------------------------
*	 Table 1 
*-----------------------------------------------------------------

use "$data/data_analysis", clear

gen time_cum_years=(time_cum/12)
replace time_cum_years=. if JN==.
gen time_years=time/12
gen nonwhite=1 if white==0
replace nonwhite=0 if race==0
bysort ID: egen transitions=sum(dummy) if  HOURSM>=75 & age>=20
bysort ID: egen transitions_ee=sum(dummy1) if  HOURSM>=75 & age>=20


label var age "Age (years)"
label var time "Job tenure (months)"
label var time_cum_years "Labor market experience (years)"
label var nonwhite "Non-white" 
label var dummy_educ "College graduate" 
label var lhrp2 "Hourly wage (log)"
label var mismatch1w "Skill Mismatch"
label var mismatch1w_pos "Overqualification"
label var mismatch1w_neg "Underqualification"
label var transitions "Total $\#$ transitions (EE+UE)"
label var transitions_ee "Total $\#$  EE transitions" 

estpost sum  age nonwhite dummy_educ time time_cum_years lhrp2 transitions transitions_ee mismatch1w mismatch1w_pos mismatch1w_neg if num_miss==0 & HOURSM>=75 & age>=20
est store stats

*panel a, b and c
esttab stats using "$output/table1.tex", replace substitute(ref. ) refcat(age "Panel A: Sample characteristics" time "Panel B: Labor Market Outcomes" mismatch1w "Panel C: Mismatch", nolabel) cells("mean(fmt(2)) sd(fmt(2))") noobs collabels( "Mean" "Standard Deviation")label nonum

** "# panel observations"
count if num_miss==0 & HOURSM>=75 & age>=20
local Ntotal= r(N)

** "# individuals"
use "$data/sample_selection", clear
keep if SAMP<=4
count
local Nind= r(N)

display `Ntotal'
display `Nind'


use "$data/data_analysis", clear

*-----------------------------------------------------------------
*	 Table 2 
*-----------------------------------------------------------------

replace unempl=unempl/100

reghdfe lhrp2 unempl c.unempl#i.dummy i.dummy age agesq i.dummy_educ c.time##c.time time_trend  i.month if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store col1

reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2 i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store col2

reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2 i.dummy1 i.dummy2 mismatch1w age agesq i.dummy_educ c.time##c.time time_trend  i.month if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store col3

reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w#c.unempl c.unempl#i.dummy1#c.mismatch1w c.unempl#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 c.mismatch1w i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store col4

reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos c.unempl#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg c.unempl#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)
est store col5


label var dummy "NH\textsubscript{\textit{i.t}}"
label var dummy1 "EE'\textsubscript{\textit{i,t}}"
label var dummy2 "UE\textsubscript{\textit{i,t}}"
label var mismatch1w "\$ m_{i,t} $"
label var mismatch1w_pos "\$ m^+_{i,t} $"
label var mismatch1w_neg "\$ m^-_{i,t} $"
label var unempl "U\textsubscript{\textit{i,t}}"
esttab  col1 col2 col3 col4 col5 using "$output/table2.tex", replace  substitute(\_ _, =1 , ref. ) label depvars nomti se(3) b(3) star(* 0.1 ** 0.05 *** 0.01) noobs keep(unempl 1.dummy#c.unempl 1.dummy1#c.unempl 1.dummy2#c.unempl  mismatch1w c.mismatch1w#c.unempl 1.dummy1#c.unempl#c.mismatch1w 1.dummy2#c.unempl#c.mismatch1w c.mismatch1w_pos#c.unempl 1.dummy2#c.unempl#c.mismatch1w_pos c.mismatch1w_neg#c.unempl  1.dummy2#c.unempl#c.mismatch1w_neg 1.dummy1#c.unempl#c.mismatch1w_pos 1.dummy1#c.unempl#c.mismatch1w_neg) nonotes nobaselevels  alignment(S S S S S S) stats(N r2, fmt(0 3) layout("\multicolumn{1}{c}{@}" "\multicolumn{1}{S}{@}") labels(`"Observations"' `"Adjusted \$R^{2}$"')) noconstant nobaselevels interaction(" $\cdot$ ")


*-----------------------------------------------------------------
*	 Figures
*-----------------------------------------------------------------

replace unempl=unempl*100

*-----------------------------------------------------------------
*	 Figure 1
*-----------------------------------------------------------------

cloglog separation mismatch1w initial_unempl unempl i.race i.dummy_educ i.occupation_agg i.industry i.month initial_agecb lhrp2 lntime  if ave_hours>=75 & JN~=. & gender==0 & initial_age>=20, cluster(ID) robust
margins,at(mismatch1w=9 lntime==(0 .69314718 1.0986123 1.3862944 1.6094379 1.79176 1.9459101 2.0794415 2.1972246 2.3025851 2.3978953 2.48491 2.89037 3.17805 3.4012 3.58352 3.7376696 3.8712 3.98898 4.09434 4.18965 4.27667 4.35667 4.43081 4.49981 4.56435 4.62497)) post 
matrix M=r(table)
matrix Z_b=M[1,1..27]'
matrix Z_lb=M[5,1..27]'
matrix Z_ub=M[6,1..27]'
svmat Z_b, name(b_c_low1)
svmat Z_lb, name(lb_c_low1)
svmat Z_ub, name(ub_c_low1)

cloglog separation mismatch1w initial_unempl unempl i.race i.dummy_educ i.occupation_agg i.industry i.month initial_agecb lhrp2 lntime  if ave_hours>=75 & JN~=. & gender==0 & initial_age>=20, cluster(ID) robust
margins,at(mismatch1w=55.5 lntime==(0 .69314718 1.0986123 1.3862944 1.6094379 1.79176 1.9459101 2.0794415 2.1972246 2.3025851 2.3978953 2.48491 2.89037 3.17805 3.4012 3.58352 3.7376696 3.8712 3.98898 4.09434 4.18965 4.27667 4.35667 4.43081 4.49981 4.56435 4.62497)) post 
matrix M=r(table)
matrix Z_b=M[1,1..27]'
matrix Z_lb=M[5,1..27]'
matrix Z_ub=M[6,1..27]'
svmat Z_b, name(b_c_high1)
svmat Z_lb, name(lb_c_high1)
svmat Z_ub, name(ub_c_high1)


sort ID year month
gen time_hazard=1 if _n==1
replace time_hazard=2 if _n==2
replace time_hazard=3 if _n==3
replace time_hazard=4 if _n==4
replace time_hazard=5 if _n==5
replace time_hazard=6 if _n==6
replace time_hazard=7 if _n==7
replace time_hazard=8 if _n==8
replace time_hazard=9 if _n==9
replace time_hazard=10 if _n==10
replace time_hazard=11 if _n==11
replace time_hazard=12 if _n==12
replace time_hazard=18 if _n==13
replace time_hazard=24 if _n==14
replace time_hazard=30 if _n==15
replace time_hazard=36 if _n==16
replace time_hazard=42 if _n==17
replace time_hazard=48 if _n==18
replace time_hazard=54 if _n==19
replace time_hazard=60 if _n==20
replace time_hazard=66 if _n==21
replace time_hazard=72 if _n==22
replace time_hazard=78 if _n==23
replace time_hazard=84 if _n==24
replace time_hazard=90 if _n==25
replace time_hazard=96 if _n==26
replace time_hazard=102 if _n==27

twoway   (rarea ub_c_high1 lb_c_high1 time_hazard if time_hazard<=60 , color(red) lcolor(red) lcolor(%20) fcolor(%30))  (line b_c_high1 time_hazard if time_hazard<=60 , lcolor(red) ) (rarea ub_c_low1 lb_c_low1 time_hazard if time_hazard<=60 , color(blue) lcolor(blue) lcolor(%20) fcolor(%30))  (line b_c_low1  time_hazard if time_hazard<=60 , mcolor(blue) msymbol(circle) lcolor(blue) lpattern(solid) ytitle("Hazard rate") xtitle("Tenure (months)") legend(order(2 4) label(2 "High skill mismatch") label(4 "Low skill mismatch")  ring(0) position(2) bmargin(large)) scheme(plotplainblind)  xsize(5) ysize(5))
graph export "$output/figure1.pdf", replace


*-----------------------------------------------------------------
*	 Figure 2
*-----------------------------------------------------------------

local c=20
local c_1=19

pctile quant=mismatch1w if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant)
pctile quant_pos=mismatch1w_pos if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_pos)
pctile quant_neg=mismatch1w_neg if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_neg)

sum mismatch1w_neg  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local a=r(mean)

sum mismatch1w_pos  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local b=r(mean)

mkmat quant if quant~=., matrix(x)
mkmat quant_pos if quant_pos~=., matrix(x_pos)
mkmat quant_neg if quant_neg~=., matrix(x_neg)

scalar sig1 = 0.05	

foreach var in b up dn{
foreach var2 in stayers ee ue{
	gen `var'_`var2'=.
	gen `var'_`var2'_pos=.
	gen `var'_`var2'_neg=.
}
}

*-----------------------------------------------------------------
*	 Figure 2 - Panel A
*-----------------------------------------------------------------


reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w#c.unempl c.unempl#i.dummy1#c.mismatch1w c.unempl#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 c.mismatch1w i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)

forvalues i=1(1)`c_1'{
lincom _b[unempl]+_b[unempl#mismatch1w]*x[`i',1]
replace b_stayers=r(estimate)*-100 if _n == `i'
replace up_stayers=r(ub)*-100 if _n == `i'
replace dn_stayers=r(lb)*-100 if _n == `i'

lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy1#mismatch1w]*x[`i',1]
replace b_ee=r(estimate)*-100 if _n == `i'
replace up_ee=r(ub)*-100 if _n == `i'
replace dn_ee=r(lb)*-100 if _n == `i'

lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy2#mismatch1w]*x[`i',1]
replace b_ue=r(estimate)*-100 if _n == `i'
replace up_ue=r(ub)*-100 if _n == `i'
replace dn_ue=r(lb)*-100 if _n == `i'

}

*-----------------------------------------------------------------

twoway  (rcap up_stayers dn_stayers values_quant, color(blue) lcolor(blue) lcolor(%30))  (scatter b_stayers  values_quant, mcolor(blue) msymbol(circle) lcolor(blue) lwidth(thick) lpattern(solid) xtitle("Mismatch (percentiles)") ytitle("Wage semi-elasticity") legend(off) yline(0, lcolor(gs8) lpattern("-") lstyle(foreground))  scheme(plotplainblind) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Stayers"))
graph export "$output/figure2a_left.pdf", replace

twoway  (rcap up_ue dn_ue values_quant, color(red) lcolor(red) lcolor(%30))  (scatter b_ue  values_quant, mcolor(red) msymbol(circle) lcolor(red) lwidth(thick) lpattern(solid) xtitle("Mismatch (percentiles)") ytitle("Wage semi-elasticity") legend(off) yline(0, lcolor(gs8) lpattern("-") lstyle(foreground))  scheme(plotplainblind) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("New Hires from Unemployment"))
graph export "$output/figure2a_middle.pdf",  replace


twoway  (rcap up_ee dn_ee values_quant, color(green) lcolor(green) lcolor(%30))  (scatter b_ee  values_quant, mcolor(green) msymbol(circle) lcolor(green) lwidth(thick) lpattern(solid) xtitle("Mismatch (percentiles)") ytitle("Wage semi-elasticity") legend(off) yline(0, lcolor(gs8) lpattern("-") lstyle(foreground))  scheme(plotplainblind) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Switchers"))
graph export "$output/figure2a_right.pdf", replace


*-----------------------------------------------------------------
*	 Figure 2 - Panel B & C
*-----------------------------------------------------------------


reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos c.unempl#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg c.unempl#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)


	forvalues i=1(1)`c_1'{
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'
	replace b_stayers_pos=r(estimate)*-100 if _n == `i'
	replace up_stayers_pos=r(ub)*-100 if _n == `i'
	replace dn_stayers_pos=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy1#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy1#mismatch1w_neg]*`a'
	replace b_ee_pos=r(estimate)*-100 if _n == `i'
	replace up_ee_pos=r(ub)*-100 if _n == `i'
	replace dn_ee_pos=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy2#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy2#mismatch1w_neg]*`a'
	replace b_ue_pos=r(estimate)*-100 if _n == `i'
	replace up_ue_pos=r(ub)*-100 if _n == `i'
	replace dn_ue_pos=r(lb)*-100 if _n == `i'
	
	
	
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*`b' +_b[unempl#mismatch1w_neg]*x_neg[`i',1]
	replace b_stayers_neg=r(estimate)*-100 if _n == `i'
	replace up_stayers_neg=r(ub)*-100 if _n == `i'
	replace dn_stayers_neg=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy1#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy1#mismatch1w_neg]*x_neg[`i',1]
	replace b_ee_neg=r(estimate)*-100 if _n == `i'
	replace up_ee_neg=r(ub)*-100 if _n == `i'
	replace dn_ee_neg=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy2#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy2#mismatch1w_neg]*x_neg[`i',1]
	replace b_ue_neg=r(estimate)*-100 if _n == `i'
	replace up_ue_neg=r(ub)*-100 if _n == `i'
	replace dn_ue_neg=r(lb)*-100 if _n == `i'
	
}

*-----------------------------------------------------------------

twoway  (rcap up_stayers_pos dn_stayers_pos values_quant_pos, color(blue) lcolor(blue) lcolor(%30))  (scatter b_stayers_pos  values_quant_pos, mcolor(blue) msymbol(circle) lcolor(blue) lwidth(thick) lpattern(solid) xtitle("Overqualification (percentiles)") ytitle("Wage semi-elasticity") legend(off) yline(0, lcolor(gs8) lpattern("-") lstyle(foreground))  scheme(plotplainblind) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("JobStayers"))
graph export "$output/figure2b_left.pdf", as(pdf) replace

twoway  (rcap up_ee_pos dn_ee_pos values_quant_pos, color(green) lcolor(green) lcolor(%30))  (scatter b_ee_pos   values_quant_pos, mcolor(green) msymbol(circle) lcolor(green) lwidth(thick) lpattern(solid) xtitle("Overqualification (percentiles)") ytitle("Wage semi-elasticity") legend(off) yline(0, lcolor(gs8) lpattern("-") lstyle(foreground))  scheme(plotplainblind) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Switchers"))
graph export "$output/figure2b_right.pdf", as(pdf) replace

twoway  (rcap up_ue_pos dn_ue_pos values_quant_pos, color(red) lcolor(red) lcolor(%30))  (scatter b_ue_pos  values_quant_pos, mcolor(red) msymbol(circle) lcolor(red) lwidth(thick) lpattern(solid) xtitle("Overqualification (percentiles)") ytitle("Wage semi-elasticity") legend(off) yline(0, lcolor(gs8) lpattern("-") lstyle(foreground))  scheme(plotplainblind) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("New Hires from Unemployment"))
graph export "$output/figure2b_middle.pdf", as(pdf) replace


*-----------------------------------------------------------------

twoway  (rcap up_stayers_neg dn_stayers_neg values_quant_neg, color(blue) lcolor(blue) lcolor(%30))  (scatter b_stayers_neg  values_quant_neg, mcolor(blue) msymbol(circle) lcolor(blue) lwidth(thick) lpattern(solid) xtitle("Underqualification (percentiles)") ytitle("Wage semi-elasticity") legend(off) yline(0, lcolor(gs8) lpattern("-") lstyle(foreground))  scheme(plotplainblind) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Stayers"))
graph export "$output/figure2c_left.pdf", as(pdf) replace

twoway  (rcap up_ee_neg dn_ee_neg values_quant_neg, color(green) lcolor(green) lcolor(%30))  (scatter b_ee_neg   values_quant_pos, mcolor(green) msymbol(circle) lcolor(green) lwidth(thick) lpattern(solid) xtitle("Underqualification (percentiles)") ytitle("Wage semi-elasticity") legend(off) yline(0, lcolor(gs8) lpattern("-") lstyle(foreground))  scheme(plotplainblind) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Switchers"))
graph export "$output/figure2c_right.pdf", as(pdf) replace

twoway  (rcap up_ue_neg dn_ue_neg values_quant_neg, color(red) lcolor(red) lcolor(%30))  (scatter b_ue_neg  values_quant_pos, mcolor(red) msymbol(circle) lcolor(red) lwidth(thick) lpattern(solid) xtitle("Underqualification (percentiles)") ytitle("Wage semi-elasticity") legend(off) yline(0, lcolor(gs8) lpattern("-") lstyle(foreground))  scheme(plotplainblind) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("New Hires from Unemployment"))
graph export "$output/figure2c_middle.pdf", as(pdf) replace

*-----------------------------------------------------------------
*	 Figure 3
*-----------------------------------------------------------------

*-----------------------------------------------------------------
*	 Figure 3 - Panel A
*-----------------------------------------------------------------

*-----------------------------------------------------------------
*Robustness 1: 3Months
reghdfe  lhrp2 unempl c.unempl#i.dummy1_alt3 c.unempl#i.dummy2_alt3  c.mismatch1w#c.unempl c.unempl#i.dummy1_alt3#c.mismatch1w c.unempl#i.dummy2_alt3#c.mismatch1w c.mismatch1w#i.dummy1_alt3 c.mismatch1w#i.dummy2_alt3 mismatch1w i.dummy1_alt3 i.dummy2_alt3  age agesq i.dummy_educ c.time##c.time time_trend i.month if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)

gen b_stayers_3=.
gen up_stayers_3 =.
gen dn_stayers_3 =.
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#mismatch1w]*x[`i',1]
replace b_stayers_3=r(estimate)*-100 if _n == `i'
replace up_stayers_3=r(ub)*-100 if _n == `i'
replace dn_stayers_3=r(lb)*-100 if _n == `i'
}

*coefficient for ee
	gen b_ee_3=.
	gen up_ee_3 =.
	gen dn_ee_3 =.	
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#1.dummy1_alt3]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy1_alt3#mismatch1w]*x[`i',1]
replace b_ee_3=r(estimate)*-100 if _n == `i'
replace up_ee_3=r(ub)*-100 if _n == `i'
replace dn_ee_3=r(lb)*-100 if _n == `i'
}

*coefficient for ue
	gen b_ue_3=.
	gen up_ue_3 =.
	gen dn_ue_3 =.	
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#1.dummy2_alt3]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy2_alt3#mismatch1w]*x[`i',1]
replace b_ue_3=r(estimate)*-100 if _n == `i'
replace up_ue_3=r(ub)*-100 if _n == `i'
replace dn_ue_3=r(lb)*-100 if _n == `i'
}

*-----------------------------------------------------------------
*Robustness 2: recalls

reghdfe  lhrp2 unempl c.unempl#i.dummy1_alt4 c.unempl#i.dummy2_alt4  c.mismatch1w#c.unempl c.unempl#i.dummy1_alt4#c.mismatch1w c.unempl#i.dummy2_alt4#c.mismatch1w c.mismatch1w#i.dummy1_alt4 c.mismatch1w#i.dummy2_alt4 mismatch1w i.dummy1_alt4 i.dummy2_alt4  age agesq i.dummy_educ c.time##c.time time_trend i.month if HOURSM>=75  & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)

gen b_stayers_4=.
gen up_stayers_4 =.
gen dn_stayers_4 =.
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#mismatch1w]*x[`i',1]
replace b_stayers_4=r(estimate)*-100 if _n == `i'
replace up_stayers_4=r(ub)*-100 if _n == `i'
replace dn_stayers_4=r(lb)*-100 if _n == `i'
}

*coefficient for ee
	gen b_ee_4=.
	gen up_ee_4 =.
	gen dn_ee_4 =.	
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#1.dummy1_alt4]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy1_alt4#mismatch1w]*x[`i',1]
replace b_ee_4=r(estimate)*-100 if _n == `i'
replace up_ee_4=r(ub)*-100 if _n == `i'
replace dn_ee_4=r(lb)*-100 if _n == `i'
}

*coefficient for ue
	gen b_ue_4=.
	gen up_ue_4 =.
	gen dn_ue_4 =.	
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#1.dummy2_alt4]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy2_alt4#mismatch1w]*x[`i',1]
replace b_ue_4=r(estimate)*-100 if _n == `i'
replace up_ue_4=r(ub)*-100 if _n == `i'
replace dn_ue_4=r(lb)*-100 if _n == `i'
}

*-----------------------------------------------------------------
*Robustness 3: skill-specific weights

pctile quant_w=mismatch1w_w if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_w)
mkmat quant_w if quant~=., matrix(x_w)
reghdfe  lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_w#c.unempl c.unempl#i.dummy1#c.mismatch1w_w c.unempl#i.dummy2#c.mismatch1w_w c.mismatch1w_w#i.dummy1 c.mismatch1w_w#i.dummy2  mismatch1w_w i.dummy1 i.dummy2  age agesq i.dummy_educ c.time##c.time time_trend i.month if HOURSM>=75  & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)

gen b_stayers_5=.
gen up_stayers_5 =.
gen dn_stayers_5 =.
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#mismatch1w_w]*x_w[`i',1]
replace b_stayers_5=r(estimate)*-100 if _n == `i'
replace up_stayers_5=r(ub)*-100 if _n == `i'
replace dn_stayers_5=r(lb)*-100 if _n == `i'
}

*coefficient for ee
	gen b_ee_5=.
	gen up_ee_5 =.
	gen dn_ee_5 =.	
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_w]*x_w[`i',1]+_b[unempl#1.dummy1#mismatch1w_w]*x_w[`i',1]
replace b_ee_5=r(estimate)*-100 if _n == `i'
replace up_ee_5=r(ub)*-100 if _n == `i'
replace dn_ee_5=r(lb)*-100 if _n == `i'
}

*coefficient for ue
	gen b_ue_5=.
	gen up_ue_5 =.
	gen dn_ue_5 =.	
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_w]*x_w[`i',1]+_b[unempl#1.dummy2#mismatch1w_w]*x_w[`i',1]
replace b_ue_5=r(estimate)*-100 if _n == `i'
replace up_ue_5=r(ub)*-100 if _n == `i'
replace dn_ue_5=r(lb)*-100 if _n == `i'
}

*-----------------------------------------------------------------
*Robustness 4: occupational skill requirements

reghdfe  lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w#c.unempl c.unempl#i.dummy1#c.mismatch1w c.unempl#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 mismatch1w i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend i.month *perc_occ_weighted if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)

gen b_stayers_6=.
gen up_stayers_6 =.
gen dn_stayers_6 =.
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#mismatch1w]*x[`i',1]
replace b_stayers_6=r(estimate)*-100 if _n == `i'
replace up_stayers_6=r(ub)*-100 if _n == `i'
replace dn_stayers_6=r(lb)*-100 if _n == `i'
}

*coefficient for ee
	gen b_ee_6=.
	gen up_ee_6 =.
	gen dn_ee_6 =.	
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy1#mismatch1w]*x[`i',1]
replace b_ee_6=r(estimate)*-100 if _n == `i'
replace up_ee_6=r(ub)*-100 if _n == `i'
replace dn_ee_6=r(lb)*-100 if _n == `i'
}

*coefficient for ue
	gen b_ue_6=.
	gen up_ue_6 =.
	gen dn_ue_6 =.	
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy2#mismatch1w]*x[`i',1]
replace b_ue_6=r(estimate)*-100 if _n == `i'
replace up_ue_6=r(ub)*-100 if _n == `i'
replace dn_ue_6=r(lb)*-100 if _n == `i'
}

*-----------------------------------------------------------------
*Robustness 5: occupational tenure

reghdfe  lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w#c.unempl c.unempl#i.dummy1#c.mismatch1w c.unempl#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 mismatch1w i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend i.month c.time_occ time_occ_sq time_occ_cb if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)

gen b_stayers_7=.
gen up_stayers_7 =.
gen dn_stayers_7 =.
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#mismatch1w]*x[`i',1]
replace b_stayers_7=r(estimate)*-100 if _n == `i'
replace up_stayers_7=r(ub)*-100 if _n == `i'
replace dn_stayers_7=r(lb)*-100 if _n == `i'
}

*coefficient for ee
	gen b_ee_7=.
	gen up_ee_7 =.
	gen dn_ee_7 =.	
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy1#mismatch1w]*x[`i',1]
replace b_ee_7=r(estimate)*-100 if _n == `i'
replace up_ee_7=r(ub)*-100 if _n == `i'
replace dn_ee_7=r(lb)*-100 if _n == `i'
}

*coefficient for ue
	gen b_ue_7=.
	gen up_ue_7 =.
	gen dn_ue_7 =.	
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy2#mismatch1w]*x[`i',1]
replace b_ue_7=r(estimate)*-100 if _n == `i'
replace up_ue_7=r(ub)*-100 if _n == `i'
replace dn_ue_7=r(lb)*-100 if _n == `i'
}

*-----------------------------------------------------------------
*Robustness 6: regional unemployment rate

reghdfe lhrp2  unempl_region c.unempl_region#i.dummy1 c.unempl_region#i.dummy2  c.mismatch1w#c.unempl_region c.unempl_region#i.dummy1#c.mismatch1w c.unempl_region#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 i.dummy2 i.dummy1 mismatch1w  age agesq i.dummy_educ c.time##c.time c.time_trend#i.regionfe i.month if  HOURSM>=75 & age>=20, a(ID i.year#i.occupation_agg i.year#i.industry) cluster(ID)

gen b_stayers_8=.
gen up_stayers_8 =.
gen dn_stayers_8 =.
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#mismatch1w]*x[`i',1]
replace b_stayers_8=r(estimate)*-100 if _n == `i'
replace up_stayers_8=r(ub)*-100 if _n == `i'
replace dn_stayers_8=r(lb)*-100 if _n == `i'
}

*coefficient for ee
	gen b_ee_8=.
	gen up_ee_8 =.
	gen dn_ee_8 =.	
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy1#mismatch1w]*x[`i',1]
replace b_ee_8=r(estimate)*-100 if _n == `i'
replace up_ee_8=r(ub)*-100 if _n == `i'
replace dn_ee_8=r(lb)*-100 if _n == `i'
}

*coefficient for ue
	gen b_ue_8=.
	gen up_ue_8 =.
	gen dn_ue_8 =.	
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy2#mismatch1w]*x[`i',1]
replace b_ue_8=r(estimate)*-100 if _n == `i'
replace up_ue_8=r(ub)*-100 if _n == `i'
replace dn_ue_8=r(lb)*-100 if _n == `i'
}


*-----------------------------------------------------------------
*Robustness 7: cumulative mismatch

reghdfe  lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w#c.unempl c.unempl#i.dummy1#c.mismatch1w c.unempl#i.dummy2#c.mismatch1w c.mismatch1w#i.dummy1 c.mismatch1w#i.dummy2 mismatch1w i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend i.month cum2 if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)

gen b_stayers_9=.
gen up_stayers_9 =.
gen dn_stayers_9 =.
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#mismatch1w]*x[`i',1]
replace b_stayers_9=r(estimate)*-100 if _n == `i'
replace up_stayers_9=r(ub)*-100 if _n == `i'
replace dn_stayers_9=r(lb)*-100 if _n == `i'
}

*coefficient for ee
	gen b_ee_9=.
	gen up_ee_9 =.
	gen dn_ee_9 =.	
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy1#mismatch1w]*x[`i',1]
replace b_ee_9=r(estimate)*-100 if _n == `i'
replace up_ee_9=r(ub)*-100 if _n == `i'
replace dn_ee_9=r(lb)*-100 if _n == `i'
}

*coefficient for ue
	gen b_ue_9=.
	gen up_ue_9 =.
	gen dn_ue_9 =.	
forvalues i=1(1)19{
lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w]*x[`i',1]+_b[unempl#1.dummy2#mismatch1w]*x[`i',1]
replace b_ue_9=r(estimate)*-100 if _n == `i'
replace up_ue_9=r(ub)*-100 if _n == `i'
replace dn_ue_9=r(lb)*-100 if _n == `i'
}

*-----------------------------------------------------------------

twoway  (rcap up_stayers dn_stayers values_quant,  color(blue) lcolor(blue) lcolor(%30)) (scatter b_stayers  values_quant, mcolor(blue))  (scatter b_stayers_3  values_quant, mcolor(blue)) (scatter b_stayers_4  values_quant,mcolor(blue))  (scatter b_stayers_5  values_quant,mcolor(blue)) (scatter b_stayers_6  values_quant,mcolor(blue))  (scatter b_stayers_7  values_quant,mcolor(blue))  (scatter b_stayers_8  values_quant,mcolor(blue))  (scatter b_stayers_9  values_quant,mcolor(blue)), xtitle("Mismatch (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 8 5 6 7 9) label(2 "Baseline") label(3 "3 month") label(4 "Recalls") label(5 "Weighted Mis.") label(6 "Skill Req.") label(7 "Occ. Tenure") label(8 "Regional Unempl.") label(9 "Cum. Mismatch")   col(3) position(6) bmargin(medsmall) col(2)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Stayers") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity") 
graph export "$output/figure3a_left.pdf", replace

twoway  (rcap up_ee dn_ee values_quant,  color(green) lcolor(green) lcolor(%30)) (scatter b_ee  values_quant, mcolor(green))  (scatter b_ee_3  values_quant, mcolor(green)) (scatter b_ee_4  values_quant,mcolor(green))  (scatter b_ee_5  values_quant,mcolor(green)) (scatter b_ee_6  values_quant,mcolor(green)) (scatter b_ee_7  values_quant,mcolor(green)) (scatter b_ee_8  values_quant,mcolor(green))  (scatter b_ee_9  values_quant,mcolor(green)), xtitle("Mismatch (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 8 5 6 7 9) label(2 "Baseline") label(3 "3 month") label(4 "Recalls") label(5 "Weighted Mis.") label(6 "Skill Req.") label(7 "Occ. Tenure") label(8 "Regional Unempl.") label(9 "Cum. Mismatch")   col(3) position(6) bmargin(medsmall)) ylabel(-2(2)6) ytitle("Wage semi-elasticity")  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Switchers") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind)
graph export "$output/figure3a_right.pdf",  replace

twoway  (rcap up_ue dn_ue values_quant,  color(red) lcolor(red) lcolor(%30)) (scatter b_ue values_quant, mcolor(red))  (scatter b_ue_3  values_quant, mcolor(red)) (scatter b_ue_4  values_quant,mcolor(red))  (scatter b_ue_5  values_quant,mcolor(red)) (scatter b_ue_6  values_quant,mcolor(red)) (scatter b_ue_7  values_quant,mcolor(red)) (scatter b_ue_8  values_quant,mcolor(red))  (scatter b_ue_9  values_quant,mcolor(red)), xtitle("Mismatch (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 8 5 6 7 9) label(2 "Baseline") label(3 "3 month") label(4 "Recalls") label(5 "Weighted Mis.") label(6 "Skill Req.") label(7 "Occ. Tenure") label(8 "Regional Unempl.") label(9 "Cum. Mismatch")  col(3) position(6) bmargin(medsmall)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("New Hires from Unemployment") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity")
graph export "$output/figure3a_middle.pdf", replace


*-----------------------------------------------------------------
*	 Figure 3 - Panel B & C
*-----------------------------------------------------------------

*-----------------------------------------------------------------

*Robustness 1: 3Months

sum mismatch1w_neg  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local a=r(mean)

sum mismatch1w_pos  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local b=r(mean)

reghdfe lhrp2 unempl c.unempl#i.dummy1_alt3 c.unempl#i.dummy2_alt3  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1_alt3#c.mismatch1w_pos c.unempl#i.dummy2_alt3#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1_alt3 c.mismatch1w_pos#i.dummy2_alt3 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1_alt3#c.mismatch1w_neg c.unempl#i.dummy2_alt3#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1_alt3 c.mismatch1w_neg#i.dummy2_alt3 c.mismatch1w_neg mismatch1w_pos i.dummy1_alt3 i.dummy2_alt3 age agesq i.dummy_educ c.time##c.time time_trend  i.month if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)


	foreach var2 in stayers ee ue{
	foreach var in neg pos{
	gen b_`var2'_`var'_3=.
	gen up_`var2'_`var'_3=.
	gen dn_`var2'_`var'_3=.
	}
	}
	

	forvalues i=1(1)`c_1'{
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'
	replace b_stayers_pos_3=r(estimate)*-100 if _n == `i'
	replace up_stayers_pos_3=r(ub)*-100 if _n == `i'
	replace dn_stayers_pos_3=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1_alt3]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy1_alt3#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy1_alt3#mismatch1w_neg]*`a'
	replace b_ee_pos_3=r(estimate)*-100 if _n == `i'
	replace up_ee_pos_3=r(ub)*-100 if _n == `i'
	replace dn_ee_pos_3=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2_alt3]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy2_alt3#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy2_alt3#mismatch1w_neg]*`a'
	replace b_ue_pos_3=r(estimate)*-100 if _n == `i'
	replace up_ue_pos_3=r(ub)*-100 if _n == `i'
	replace dn_ue_pos_3=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*`b' +_b[unempl#mismatch1w_neg]*x_neg[`i',1]
	replace b_stayers_neg_3=r(estimate)*-100 if _n == `i'
	replace up_stayers_neg_3=r(ub)*-100 if _n == `i'
	replace dn_stayers_neg_3=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1_alt3]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy1_alt3#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy1_alt3#mismatch1w_neg]*x_neg[`i',1]
	replace b_ee_neg_3=r(estimate)*-100 if _n == `i'
	replace up_ee_neg_3=r(ub)*-100 if _n == `i'
	replace dn_ee_neg_3=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2_alt3]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy2_alt3#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy2_alt3#mismatch1w_neg]*x_neg[`i',1]
	replace b_ue_neg_3=r(estimate)*-100 if _n == `i'
	replace up_ue_neg_3=r(ub)*-100 if _n == `i'
	replace dn_ue_neg_3=r(lb)*-100 if _n == `i'	
}

*-----------------------------------------------------------------
*Robustness 2: recalls

sum mismatch1w_neg  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local a=r(mean)

sum mismatch1w_pos  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local b=r(mean)

reghdfe lhrp2 unempl c.unempl#i.dummy1_alt4 c.unempl#i.dummy2_alt4  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1_alt4#c.mismatch1w_pos c.unempl#i.dummy2_alt4#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1_alt4 c.mismatch1w_pos#i.dummy2_alt4 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1_alt4#c.mismatch1w_neg c.unempl#i.dummy2_alt4#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1_alt4 c.mismatch1w_neg#i.dummy2_alt4 c.mismatch1w_neg mismatch1w_pos i.dummy1_alt4 i.dummy2_alt4 age agesq i.dummy_educ c.time##c.time time_trend  i.month if  HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)


	foreach var2 in stayers ee ue{
	foreach var in neg pos{
	gen b_`var2'_`var'_4=.
	gen up_`var2'_`var'_4=.
	gen dn_`var2'_`var'_4=.
	}
	}
	

	forvalues i=1(1)`c_1'{
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'
	replace b_stayers_pos_4=r(estimate)*-100 if _n == `i'
	replace up_stayers_pos_4=r(ub)*-100 if _n == `i'
	replace dn_stayers_pos_4=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1_alt4]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy1_alt4#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy1_alt4#mismatch1w_neg]*`a'
	replace b_ee_pos_4=r(estimate)*-100 if _n == `i'
	replace up_ee_pos_4=r(ub)*-100 if _n == `i'
	replace dn_ee_pos_4=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2_alt4]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy2_alt4#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy2_alt4#mismatch1w_neg]*`a'
	replace b_ue_pos_4=r(estimate)*-100 if _n == `i'
	replace up_ue_pos_4=r(ub)*-100 if _n == `i'
	replace dn_ue_pos_4=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*`b' +_b[unempl#mismatch1w_neg]*x_neg[`i',1]
	replace b_stayers_neg_4=r(estimate)*-100 if _n == `i'
	replace up_stayers_neg_4=r(ub)*-100 if _n == `i'
	replace dn_stayers_neg_4=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1_alt4]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy1_alt4#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy1_alt4#mismatch1w_neg]*x_neg[`i',1]
	replace b_ee_neg_4=r(estimate)*-100 if _n == `i'
	replace up_ee_neg_4=r(ub)*-100 if _n == `i'
	replace dn_ee_neg_4=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2_alt4]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy2_alt4#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy2_alt4#mismatch1w_neg]*x_neg[`i',1]
	replace b_ue_neg_4=r(estimate)*-100 if _n == `i'
	replace up_ue_neg_4=r(ub)*-100 if _n == `i'
	replace dn_ue_neg_4=r(lb)*-100 if _n == `i'
	
	}

*-----------------------------------------------------------------
*Robustness 3: skill-specific weights

gen mismatch1w_pos_w=math_weight_w*aux4_math_pos_weighted+verbal_weight_w*aux4_verbal_pos_weighted+tech_weight_w*aux4_technical_pos_weighted+social_weight_w*aux4_social_pos_weighted
gen mismatch1w_neg_w=math_weight_w*abs(aux4_math_weighted)+verbal_weight_w*abs(aux4_verbal_weighted)+tech_weight_w*abs(aux4_technical_weighted)+social_weight_w*abs(aux4_social_weighted)
pctile quant_pos_w=mismatch1w_pos_w if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_pos_w)
pctile quant_neg_w=mismatch1w_neg_w if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, nq(`c') genp(values_quant_neg_w)
mkmat quant_pos_w if quant_pos~=., matrix(x_pos_w)
mkmat quant_neg_w if quant_neg~=., matrix(x_neg_w)
sum mismatch1w_neg_w  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local a2=r(mean)
sum mismatch1w_pos_w  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local b2=r(mean)

reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos_w#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos_w c.unempl#i.dummy2#c.mismatch1w_pos_w c.mismatch1w_pos_w#i.dummy1 c.mismatch1w_pos_w#i.dummy2 c.mismatch1w_neg_w#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg_w c.unempl#i.dummy2#c.mismatch1w_neg_w c.mismatch1w_neg_w#i.dummy1 c.mismatch1w_neg_w#i.dummy2 c.mismatch1w_neg_w mismatch1w_pos_w i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)



	foreach var2 in stayers ee ue{
	foreach var in neg pos{
	gen b_`var2'_`var'_5=.
	gen up_`var2'_`var'_5=.
	gen dn_`var2'_`var'_5=.
	}
	}
	

	forvalues i=1(1)`c_1'{
	lincom _b[unempl]+_b[unempl#mismatch1w_pos_w]*x_pos_w[`i',1]+_b[unempl#mismatch1w_neg_w]*`a2'
	replace b_stayers_pos_5=r(estimate)*-100 if _n == `i'
	replace up_stayers_pos_5=r(ub)*-100 if _n == `i'
	replace dn_stayers_pos_5=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos_w]*x_pos_w[`i',1]+_b[unempl#1.dummy1#mismatch1w_pos_w]*x_pos_w[`i',1]+_b[unempl#mismatch1w_neg_w]*`a2'+_b[unempl#1.dummy1#mismatch1w_neg_w]*`a2'
	replace b_ee_pos_5=r(estimate)*-100 if _n == `i'
	replace up_ee_pos_5=r(ub)*-100 if _n == `i'
	replace dn_ee_pos_5=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos_w]*x_pos_w[`i',1]+_b[unempl#1.dummy2#mismatch1w_pos_w]*x_pos_w[`i',1]+_b[unempl#mismatch1w_neg_w]*`a2'+_b[unempl#1.dummy2#mismatch1w_neg_w]*`a2'
	replace b_ue_pos_5=r(estimate)*-100 if _n == `i'
	replace up_ue_pos_5=r(ub)*-100 if _n == `i'
	replace dn_ue_pos_5=r(lb)*-100 if _n == `i'
	
	
	
	lincom _b[unempl]+_b[unempl#mismatch1w_pos_w]*`b2' +_b[unempl#mismatch1w_neg_w]*x_neg_w[`i',1]
	replace b_stayers_neg_5=r(estimate)*-100 if _n == `i'
	replace up_stayers_neg_5=r(ub)*-100 if _n == `i'
	replace dn_stayers_neg_5=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos_w]*`b2'+_b[unempl#1.dummy1#mismatch1w_pos_w]*`b2'+_b[unempl#mismatch1w_neg_w]*x_neg_w[`i',1]+_b[unempl#1.dummy1#mismatch1w_neg_w]*x_neg_w[`i',1]
	replace b_ee_neg_5=r(estimate)*-100 if _n == `i'
	replace up_ee_neg_5=r(ub)*-100 if _n == `i'
	replace dn_ee_neg_5=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos_w]*`b2'+_b[unempl#1.dummy2#mismatch1w_pos_w]*`b2'+_b[unempl#mismatch1w_neg_w]*x_neg_w[`i',1]+_b[unempl#1.dummy2#mismatch1w_neg_w]*x_neg_w[`i',1]
	replace b_ue_neg_5=r(estimate)*-100 if _n == `i'
	replace up_ue_neg_5=r(ub)*-100 if _n == `i'
	replace dn_ue_neg_5=r(lb)*-100 if _n == `i'
	
}

*-----------------------------------------------------------------
*Robustness 4: occupational skill requirements


sum mismatch1w_neg  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local a=r(mean)

sum mismatch1w_pos  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local b=r(mean)

reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos c.unempl#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg c.unempl#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month *perc_occ_weighted if JOB>=100 & JOB~=.  & HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)

	foreach var2 in stayers ee ue{
	foreach var in neg pos{
	gen b_`var2'_`var'_6=.
	gen up_`var2'_`var'_6=.
	gen dn_`var2'_`var'_6=.
	}
	}
		
	forvalues i=1(1)`c_1'{
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'
	replace b_stayers_pos_6=r(estimate)*-100 if _n == `i'
	replace up_stayers_pos_6=r(ub)*-100 if _n == `i'
	replace dn_stayers_pos_6=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy1#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy1#mismatch1w_neg]*`a'
	replace b_ee_pos_6=r(estimate)*-100 if _n == `i'
	replace up_ee_pos_6=r(ub)*-100 if _n == `i'
	replace dn_ee_pos_6=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy2#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy2#mismatch1w_neg]*`a'
	replace b_ue_pos_6=r(estimate)*-100 if _n == `i'
	replace up_ue_pos_6=r(ub)*-100 if _n == `i'
	replace dn_ue_pos_6=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]
	replace b_stayers_neg_6=r(estimate)*-100 if _n == `i'
	replace up_stayers_neg_6=r(ub)*-100 if _n == `i'
	replace dn_stayers_neg_6=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy1#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy1#mismatch1w_neg]*x_neg[`i',1]
	replace b_ee_neg_6=r(estimate)*-100 if _n == `i'
	replace up_ee_neg_6=r(ub)*-100 if _n == `i'
	replace dn_ee_neg_6=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy2#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy2#mismatch1w_neg]*x_neg[`i',1]
	replace b_ue_neg_6=r(estimate)*-100 if _n == `i'
	replace up_ue_neg_6=r(ub)*-100 if _n == `i'
	replace dn_ue_neg_6=r(lb)*-100 if _n == `i'	
}

*-----------------------------------------------------------------
*Robustness 5: occupational tenure

sum mismatch1w_neg  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local a=r(mean)

sum mismatch1w_pos  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local b=r(mean)

reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos c.unempl#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg c.unempl#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month c.time_occ time_occ_sq time_occ_cb  if JOB>=100 & JOB~=.  & HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)



	foreach var2 in stayers ee ue{
	foreach var in neg pos{
	gen b_`var2'_`var'_7=.
	gen up_`var2'_`var'_7=.
	gen dn_`var2'_`var'_7=.
	}
	}

	forvalues i=1(1)`c_1'{
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'
	replace b_stayers_pos_7=r(estimate)*-100 if _n == `i'
	replace up_stayers_pos_7=r(ub)*-100 if _n == `i'
	replace dn_stayers_pos_7=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy1#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy1#mismatch1w_neg]*`a'
	replace b_ee_pos_7=r(estimate)*-100 if _n == `i'
	replace up_ee_pos_7=r(ub)*-100 if _n == `i'
	replace dn_ee_pos_7=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy2#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy2#mismatch1w_neg]*`a'
	replace b_ue_pos_7=r(estimate)*-100 if _n == `i'
	replace up_ue_pos_7=r(ub)*-100 if _n == `i'
	replace dn_ue_pos_7=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*`b' +_b[unempl#mismatch1w_neg]*x_neg[`i',1]
	replace b_stayers_neg_7=r(estimate)*-100 if _n == `i'
	replace up_stayers_neg_7=r(ub)*-100 if _n == `i'
	replace dn_stayers_neg_7=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy1#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy1#mismatch1w_neg]*x_neg[`i',1]
	replace b_ee_neg_7=r(estimate)*-100 if _n == `i'
	replace up_ee_neg_7=r(ub)*-100 if _n == `i'
	replace dn_ee_neg_7=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy2#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy2#mismatch1w_neg]*x_neg[`i',1]
	replace b_ue_neg_7=r(estimate)*-100 if _n == `i'
	replace up_ue_neg_7=r(ub)*-100 if _n == `i'
	replace dn_ue_neg_7=r(lb)*-100 if _n == `i'	
}

*-----------------------------------------------------------------
*Robustness 6: regional unemployment rate 

sum mismatch1w_neg  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local a=r(mean)

sum mismatch1w_pos  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local b=r(mean)

reghdfe lhrp2 unempl_region c.unempl_region#i.dummy1 c.unempl_region#i.dummy2  c.mismatch1w_pos#c.unempl_region c.unempl_region#i.dummy1#c.mismatch1w_pos c.unempl_region#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl_region c.unempl_region#i.dummy1#c.mismatch1w_neg c.unempl_region#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ  c.time##c.time c.time_trend#i.regionfe i.month if JOB>=100 & JOB~=.  & HOURSM>=75 & age>=20, a(ID i.year#i.occupation_agg i.year#i.industry) cluster(ID)


	foreach var2 in stayers ee ue{
	foreach var in neg pos{
	gen b_`var2'_`var'_8=.
	gen up_`var2'_`var'_8=.
	gen dn_`var2'_`var'_8=.
	}
	}

	forvalues i=1(1)`c_1'{
	lincom _b[unempl_region]+_b[unempl_region#mismatch1w_pos]*x_pos[`i',1]+_b[unempl_region#mismatch1w_neg]*`a'
	replace b_stayers_pos_8=r(estimate)*-100 if _n == `i'
	replace up_stayers_pos_8=r(ub)*-100 if _n == `i'
	replace dn_stayers_pos_8=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl_region]+_b[unempl_region#1.dummy1]+_b[unempl_region#mismatch1w_pos]*x_pos[`i',1]+_b[unempl_region#1.dummy1#mismatch1w_pos]*x_pos[`i',1]+_b[unempl_region#mismatch1w_neg]*`a'+_b[unempl_region#1.dummy1#mismatch1w_neg]*`a'
	replace b_ee_pos_8=r(estimate)*-100 if _n == `i'
	replace up_ee_pos_8=r(ub)*-100 if _n == `i'
	replace dn_ee_pos_8=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl_region]+_b[unempl_region#1.dummy2]+_b[unempl_region#mismatch1w_pos]*x_pos[`i',1]+_b[unempl_region#1.dummy2#mismatch1w_pos]*x_pos[`i',1]+_b[unempl_region#mismatch1w_neg]*`a'+_b[unempl_region#1.dummy2#mismatch1w_neg]*`a'
	replace b_ue_pos_8=r(estimate)*-100 if _n == `i'
	replace up_ue_pos_8=r(ub)*-100 if _n == `i'
	replace dn_ue_pos_8=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl_region]+_b[unempl_region#mismatch1w_pos]*`b' +_b[unempl_region#mismatch1w_neg]*x_neg[`i',1]
	replace b_stayers_neg_8=r(estimate)*-100 if _n == `i'
	replace up_stayers_neg_8=r(ub)*-100 if _n == `i'
	replace dn_stayers_neg_8=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl_region]+_b[unempl_region#1.dummy1]+_b[unempl_region#mismatch1w_pos]*`b'+_b[unempl_region#1.dummy1#mismatch1w_pos]*`b'+_b[unempl_region#mismatch1w_neg]*x_neg[`i',1]+_b[unempl_region#1.dummy1#mismatch1w_neg]*x_neg[`i',1]
	replace b_ee_neg_8=r(estimate)*-100 if _n == `i'
	replace up_ee_neg_8=r(ub)*-100 if _n == `i'
	replace dn_ee_neg_8=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl_region]+_b[unempl_region#1.dummy2]+_b[unempl_region#mismatch1w_pos]*`b'+_b[unempl_region#1.dummy2#mismatch1w_pos]*`b'+_b[unempl_region#mismatch1w_neg]*x_neg[`i',1]+_b[unempl_region#1.dummy2#mismatch1w_neg]*x_neg[`i',1]
	replace b_ue_neg_8=r(estimate)*-100 if _n == `i'
	replace up_ue_neg_8=r(ub)*-100 if _n == `i'
	replace dn_ue_neg_8=r(lb)*-100 if _n == `i'	
}

*-----------------------------------------------------------------
*Robustness 7:  cumulative mismatch

sum mismatch1w_neg  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local a=r(mean)

sum mismatch1w_pos  if age>=20 & mismatch1w~=. & lhrp2~=. & dummy_educ~=. & HOURSM>=75, detail
local b=r(mean)


reghdfe lhrp2 unempl c.unempl#i.dummy1 c.unempl#i.dummy2  c.mismatch1w_pos#c.unempl c.unempl#i.dummy1#c.mismatch1w_pos c.unempl#i.dummy2#c.mismatch1w_pos c.mismatch1w_pos#i.dummy1 c.mismatch1w_pos#i.dummy2 c.mismatch1w_neg#c.unempl c.unempl#i.dummy1#c.mismatch1w_neg c.unempl#i.dummy2#c.mismatch1w_neg c.mismatch1w_neg#i.dummy1 c.mismatch1w_neg#i.dummy2 c.mismatch1w_neg mismatch1w_pos i.dummy1 i.dummy2 age agesq i.dummy_educ c.time##c.time time_trend  i.month cum2 if HOURSM>=75 & age>=20, a(ID i.industry#i.year i.occupation_agg#i.year) cluster(ID)


	foreach var2 in stayers ee ue{
	foreach var in neg pos{
	gen b_`var2'_`var'_9=.
	gen up_`var2'_`var'_9=.
	gen dn_`var2'_`var'_9 =.
	}
	}
	

	forvalues i=1(1)`c_1'{
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'
	replace b_stayers_pos_9=r(estimate)*-100 if _n == `i'
	replace up_stayers_pos_9=r(ub)*-100 if _n == `i'
	replace dn_stayers_pos_9=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy1#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy1#mismatch1w_neg]*`a'
	replace b_ee_pos_9=r(estimate)*-100 if _n == `i'
	replace up_ee_pos_9=r(ub)*-100 if _n == `i'
	replace dn_ee_pos_9=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#1.dummy2#mismatch1w_pos]*x_pos[`i',1]+_b[unempl#mismatch1w_neg]*`a'+_b[unempl#1.dummy2#mismatch1w_neg]*`a'
	replace b_ue_pos_9=r(estimate)*-100 if _n == `i'
	replace up_ue_pos_9=r(ub)*-100 if _n == `i'
	replace dn_ue_pos_9=r(lb)*-100 if _n == `i'
	
	
	
	lincom _b[unempl]+_b[unempl#mismatch1w_pos]*`b' +_b[unempl#mismatch1w_neg]*x_neg[`i',1]
	replace b_stayers_neg_9=r(estimate)*-100 if _n == `i'
	replace up_stayers_neg_9=r(ub)*-100 if _n == `i'
	replace dn_stayers_neg_9=r(lb)*-100 if _n == `i'
 
	lincom _b[unempl]+_b[unempl#1.dummy1]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy1#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy1#mismatch1w_neg]*x_neg[`i',1]
	replace b_ee_neg_9=r(estimate)*-100 if _n == `i'
	replace up_ee_neg_9=r(ub)*-100 if _n == `i'
	replace dn_ee_neg_9=r(lb)*-100 if _n == `i'
	
	lincom _b[unempl]+_b[unempl#1.dummy2]+_b[unempl#mismatch1w_pos]*`b'+_b[unempl#1.dummy2#mismatch1w_pos]*`b'+_b[unempl#mismatch1w_neg]*x_neg[`i',1]+_b[unempl#1.dummy2#mismatch1w_neg]*x_neg[`i',1]
	replace b_ue_neg_9=r(estimate)*-100 if _n == `i'
	replace up_ue_neg_9=r(ub)*-100 if _n == `i'
	replace dn_ue_neg_9=r(lb)*-100 if _n == `i'
	
}

*-----------------------------------------------------------------

twoway  (rcap up_stayers_pos dn_stayers_pos values_quant,  color(blue) lcolor(blue) lcolor(%30)) (scatter b_stayers_pos  values_quant, mcolor(blue))  (scatter b_stayers_pos_3  values_quant, mcolor(blue)) (scatter b_stayers_pos_4  values_quant,mcolor(blue))  (scatter b_stayers_pos_5  values_quant,mcolor(blue)) (scatter b_stayers_pos_6  values_quant,mcolor(blue))  (scatter b_stayers_pos_7  values_quant,mcolor(blue))  (scatter b_stayers_pos_8  values_quant,mcolor(blue))  (scatter b_stayers_pos_9  values_quant,mcolor(blue)), xtitle("Overqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 5 6 7 8 9 10) label(2 "Baseline") label(3 "3 month") label(4 "Recalls") label(5 "Weighted Mis.") label(6 "Skill Req.") label(7 "Occ. Tenure") label(8 "Regional Unempl.")  label(9 "Cum. Mismatch")   col(3) position(6) bmargin(medsmall) col(2)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Stayers") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity") 
graph export "$output/figure3b_left.pdf", as(pdf) replace


twoway  (rcap up_ee_pos dn_ee_pos values_quant,  color(green) lcolor(green) lcolor(%30)) (scatter b_ee_pos  values_quant, mcolor(green))  (scatter b_ee_pos_3  values_quant, mcolor(green)) (scatter b_ee_pos_4  values_quant,mcolor(green))  (scatter b_ee_pos_5  values_quant,mcolor(green)) (scatter b_ee_pos_6  values_quant,mcolor(green))  (scatter b_ee_pos_7  values_quant,mcolor(green))  (scatter b_ee_pos_8  values_quant,mcolor(green)) (scatter b_ee_pos_9  values_quant,mcolor(green)), xtitle("Overqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground))  legend(order(2 3 4 5 6 7 8 9 10) label(2 "Baseline") label(3 "3 month") label(4 "Recalls") label(5 "Weighted Mis.") label(6 "Skill Req.") label(7 "Occ. Tenure") label(8 "Regional Unempl.")  label(9 "Cum. Mismatch")    col(3) position(6) bmargin(medsmall) col(2)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Switchers") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity") 
graph export "$output/figure3b_right.pdf", as(pdf) replace


twoway  (rcap up_ue_pos dn_ue_pos values_quant,  color(red) lcolor(red) lcolor(%30)) (scatter b_ue_pos  values_quant, mcolor(red))  (scatter b_ue_pos_3  values_quant, mcolor(red)) (scatter b_ue_pos_4  values_quant,mcolor(red))  (scatter b_ue_pos_5  values_quant,mcolor(red)) (scatter b_ue_pos_6  values_quant,mcolor(red))  (scatter b_ue_pos_7  values_quant,mcolor(red))  (scatter b_ue_pos_8  values_quant,mcolor(red))  (scatter b_ue_pos_9  values_quant,mcolor(red)), xtitle("Overqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground))  legend(order(2 3 4 5 6 7 8 9 10) label(2 "Baseline") label(3 "3 month") label(4 "Recalls") label(5 "Weighted Mis.") label(6 "Skill Req.") label(7 "Occ. Tenure") label(8 "Regional Unempl.")  label(9 "Cum. Mismatch")    col(3) position(6) bmargin(medsmall) col(2)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("New Hires from Unemployment") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity") 
graph export "$output/figure3b_middle.pdf", as(pdf) replace

*-----------------------------------------------------------------

twoway  (rcap up_stayers_neg dn_stayers_neg values_quant,  color(blue) lcolor(blue) lcolor(%30)) (scatter b_stayers_neg  values_quant, mcolor(blue))  (scatter b_stayers_neg_3  values_quant, mcolor(blue)) (scatter b_stayers_neg_4  values_quant,mcolor(blue))  (scatter b_stayers_neg_5  values_quant,mcolor(blue)) (scatter b_stayers_neg_6  values_quant,mcolor(blue))  (scatter b_stayers_neg_7  values_quant,mcolor(blue))  (scatter b_stayers_neg_8  values_quant,mcolor(blue))  (scatter b_stayers_neg_9  values_quant,mcolor(blue)), xtitle("Underqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 5 6 7 8 9 10) label(2 "Baseline") label(3 "3 month") label(4 "Recalls") label(5 "Weighted Mis.") label(6 "Skill Req.") label(7 "Occ. Tenure") label(8 "Regional Unempl.")  label(9 "Cum. Mismatch")  col(3) position(6) bmargin(medsmall) col(2)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Stayers") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity") 
graph export "$output/figure3c_left.pdf", as(pdf) replace

twoway  (rcap up_ee_neg dn_ee_neg values_quant,  color(green) lcolor(green) lcolor(%30)) (scatter b_ee_neg values_quant, mcolor(green))  (scatter b_ee_neg_3  values_quant, mcolor(green)) (scatter b_ee_neg_4  values_quant,mcolor(green))  (scatter b_ee_neg_5  values_quant,mcolor(green)) (scatter b_ee_neg_6  values_quant,mcolor(green))  (scatter b_ee_neg_7  values_quant,mcolor(green))  (scatter b_ee_neg_8  values_quant,mcolor(green)) (scatter b_ee_neg_9  values_quant,mcolor(green)), xtitle("Underqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 5 6 7 8 9 10) label(2 "Baseline") label(3 "3 month") label(4 "Recalls") label(5 "Weighted Mis.") label(6 "Skill Req.") label(7 "Occ. Tenure") label(8 "Regional Unempl.")  label(9 "Cum. Mismatch")   col(3) position(6) bmargin(medsmall) col(2)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("Job Switchers") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity") 
graph export "$output/figure3c_right.pdf", as(pdf) replace


twoway  (rcap up_ue_neg dn_ue_neg values_quant,  color(red) lcolor(red) lcolor(%30)) (scatter b_ue_neg  values_quant, mcolor(red))  (scatter b_ue_neg_3  values_quant, mcolor(red)) (scatter b_ue_neg_4  values_quant,mcolor(red))  (scatter b_ue_neg_5  values_quant,mcolor(red)) (scatter b_ue_neg_6  values_quant,mcolor(red))  (scatter b_ue_neg_7  values_quant,mcolor(red))  (scatter b_ue_neg_8  values_quant,mcolor(red)) (scatter b_ue_neg_9  values_quant,mcolor(red)), xtitle("Underqualification (percentiles)")  yline(0, lpattern("-") lstyle(foreground)) legend(order(2 3 4 5 6 7 8 9 10) label(2 "Baseline") label(3 "3 month") label(4 "Recalls") label(5 "Weighted Mis.") label(6 "Skill Req.") label(7 "Occ. Tenure") label(8 "Regional Unempl.")  label(9 "Cum. Mismatch")   col(3) position(6) bmargin(medsmall) col(2)) ylabel(-2(2)6)  xlabel(0(25)100)  xsize(5) ysize(5) title("New Hires from Unemployment") yline(0, lcolor(gs8) lpattern("-") lstyle(foreground)) scheme(plotplainblind) ytitle("Wage semi-elasticity") 
graph export "$output/figure3c_middle.pdf", as(pdf) replace


*-----------------------------------------------------------------
*	In-text numbers
*-----------------------------------------------------------------

*--------------------------------------------------------------------
*	 a. intro (pg. 3) & section II - Interpretation (1.24)
*--------------------------------------------------------------------

gen ratio=b_c_high1/b_c_low1
sum ratio if time_hazard>8, detail
local a=round(r(mean),0.001)
display `a'

*---------------------------------------------------------------------------------
*	 b. section IV - Heterogeneity along the mismatch distribution
*---------------------------------------------------------------------------------

*1. wage-semi-elasticity for worst-matched js: 1.7
sum b_stayers if values_quant==90
local b1=round(r(mean),0.1)
display `b1'

*2. diff in wage-semi-elasticity between best & worst js: 1.6
sum b_stayers if values_quant==10
local b2=r(mean)

sum b_stayers if values_quant==90
local b3=r(mean)

display round(`b3'-`b2',0.1)

*3.  wage-semi-elasticity for worst-matched ue: 3.3
sum b_ue if values_quant==90
local b4=round(r(mean),0.1)
display `b4'

*4. diff in wage-semi-elasticity between best & worst js: 1.6
sum b_ee if values_quant==10
local b5=r(mean)

sum b_ee if values_quant==90
local b6=r(mean)

display round(`b6'-`b5',0.1)


*---------------------------------------------------------------------------------
*	 c. section IV - Over vs. Under
*---------------------------------------------------------------------------------

*1. diff in wage-semi-elasticity between best & worst js: 2 
sum b_stayers_pos if values_quant==10
local c1=r(mean)

sum b_stayers_pos if values_quant==90
local c2=r(mean)

display round(`c2'-`c1')

*2. wage-semi-elasticity for worst-matched js: 2
sum b_stayers_pos if values_quant==90
local c3=round(r(mean))
display `c3'

*3. wage-semi-elasticity for worst-matched ue: 3
sum b_ue_pos if values_quant==90
local c4=round(r(mean))
display `c4'

*---------------------------------------------------------------------------------
*.   d. skill-specific weights - section IV - robustness
*---------------------------------------------------------------------------------

foreach var in math verbal social tech {
sum `var'_weight_w
local 	`var'=r(mean)
}

display `math'
display `verbal'
display `social'
display `tech'

*---------------------------------------------------------------------------------
*.   e. occ. switching - section I - wages & employment
*---------------------------------------------------------------------------------

sum dummy_occ if HOURSM>=75 & age>=20 & num_miss==0
local switch=round(r(mean),0.01)
display `switch'*100






